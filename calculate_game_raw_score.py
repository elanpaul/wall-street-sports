import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team, PlayerCurrentStatistics, PlayerHistoricalStatistics
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer, PlayerCurrentStatisticsSerializer, PlayerHistoricalStatisticsSerializer
from bs4 import BeautifulSoup
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint
from datetime import datetime
import decimal
from fractions import Fraction

import get_player_names_and_sports_reference_urls



def get_nba_raw_game_score(minutes_played, fgm, fga, tpfgm, tpfga, ftm, fta, offensive_rebounds, defensive_rebounds, total_rebounds, assists, steals, blocks, turnovers, personal_fouls, points):
    w_fgm = fgm * 85
    w_steals = steals * 53
    w_tpfgm = tpfgm * 51
    w_ftm = ftm * 46
    w_blocks = blocks * 39
    w_off_reb = offensive_rebounds * 39
    w_assists = assists * 34
    w_def_reb = defensive_rebounds * 14
    w_fouls = personal_fouls * 17
    w_ft_missed = (fta - ftm) * 20
    w_fg_missed = (fga - fgm) * 39
    w_turnovers = turnovers * 53
    overall_raw = w_fgm + w_steals + w_tpfgm + w_ftm + w_blocks + w_off_reb + w_assists + w_def_reb - w_fouls - w_ft_missed - w_fg_missed - w_turnovers
    if minutes_played > 0:
        final_raw = overall_raw * (1/minutes_played)
    else:
        final_raw = overall_raw * 0
    print("nba")
    print(overall_raw)
    print(final_raw)
    return final_raw

def get_nfl_offensive_player_raw_score(passes_completed, passes_attempted, passing_yards, passing_touchdowns, interceptions, qb_rating, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, targets, receptions, receiving_yards, receiving_touchdowns):
    w_p_complete = passes_completed * 1.5
    w_p_yards = passing_yards / 20
    w_p_tds = passing_touchdowns * 4
    w_interceptions = interceptions * 3
    w_p_incomplete = (passes_attempted - passes_completed) * 2
    w_completion_score = (w_p_complete - w_p_incomplete) / 10.0
    overall_passing_score = w_p_yards + w_p_tds + w_completion_score - w_interceptions
    final_passing_score = overall_passing_score
    print("final passing score: " + str(final_passing_score))

    w_ru_yards = rushing_yards / 5
    w_ru_tds = rushing_touchdowns * 7
    w_fumbles = fumbles * 8
    w_ru_score = w_ru_yards
    # if rushes_attempted > 0:
    #     w_ru_score = w_ru_yards / rushes_attempted
    # else:
    #     w_ru_score = 0
    overall_rushing_score = w_ru_score + w_ru_tds - w_fumbles

    if overall_rushing_score > 0:
        final_rushing_score = overall_rushing_score
    else:
        final_rushing_score = 0
    

    w_receptions = receptions * 1.5
    w_re_yards = receiving_yards / 8
    w_re_tds = receiving_touchdowns * 7
    # should we include missed catches
    w_misses = (targets - w_receptions)


    overall_receiving_score = w_receptions + w_re_yards + w_re_tds - w_misses
    if overall_receiving_score > 0:
        final_receiving_score = overall_receiving_score
    else:
        final_receiving_score = 0

    overall_raw = final_passing_score + final_rushing_score + final_receiving_score
    if overall_raw > 1:
        final_raw = overall_raw
    else:
        final_raw = 1.0
    print("nfl offense")
    print(overall_raw)
    print(final_raw)
    return final_raw

def get_nfl_defensive_player_raw_score(total_tackles, sacks, forced_fumbles, defensive_interceptions):
    w_t_tackles = total_tackles
    w_sacks = sacks * 4
    w_forced_fumbles = forced_fumbles * 5
    w_def_interceptions = defensive_interceptions * 7
    overall_raw = w_t_tackles + w_sacks + w_forced_fumbles + w_def_interceptions
    final_raw = overall_raw
    print("nfl defense")
    print(overall_raw)
    print(final_raw)
    return final_raw



def get_mlb_pitcher_raw_score(innings_pitched, batters_faced, hits_allowed, earned_runs, strikeouts, decision):
    # also have to be low weights, because the adjuster is multiply instead of divide
    w_strikeouts = strikeouts * 5
    if decision == 2 or decision == 3 or decision == -4:
        w_decision = abs(decision * 10) # by accident a save is negative 4, so abs it
    else:
        w_decision = -30
    w_hits_allowed = hits_allowed * 10
    w_earned_runs = earned_runs * 20

    overall_raw = w_strikeouts + w_decision - w_hits_allowed - w_earned_runs
    if innings_pitched >= 1: # to not include if you pitched for 1 or 2 outs
        final_raw = overall_raw * innings_pitched
    else:
        final_raw = 0
    print("mlb pitcher")
    print(overall_raw)
    print(final_raw)
    return final_raw

def get_mlb_hitter_raw_score(at_bats, runs, hits, rbi, homeruns, on_base_percentage, slugging_percentage):
    # since the divider is at bats, which could be only 1 or 2, so the weights must be smaller
    w_runs = runs * 3
    w_hits = hits * 8
    w_rbi = rbi * 12
    w_hr = homeruns * 15
    # must get doubles, triples, and walks, b/c slugging is cummulative not by game
    # mus get strikeouts stam
    # w_slugging = slugging_percentage * 100

    overall_raw = w_runs + w_hits + w_rbi + w_hr # + w_slugging
    if at_bats > 0:
        final_raw = overall_raw / at_bats
    else:
        final_raw = 0
    print("mlb hitter")
    print(overall_raw)
    print(final_raw)
    return final_raw



def calculate_raw_scores(sport):
    offensive_skill_positions = ["qb", "qb throws", "rb", "fb", "wr", "wr throws", "te"] # obvs throws is a mistake
    offensive_line_positions = ["lt", "rt", "ot", "t", "lg", "rg", "og", "g", "ol", "c"]
    defensive_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl", "lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb", "cb", "ss", "fs", "s"]
    defensive_line_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl"]
    defensive_linebacker_positions = ["lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb"]
    defensive_back_positions = ["cb", "ss", "fs", "s"]
    other_positions = ["p", "k"]
    #nba - 600,000 nfl - 247,201 mlb - 798,898
    all_logs = PlayerHistoricalStatistics.objects.filter(stat_name__contains=sport)#(player=4182)#all()#[:1100]##.all()
    all_logs_dict = {}
    print("length of history rows: " + str(len(all_logs)))

    for game_log_row in all_logs: #str(game_log_row.stat_id)
        if str(game_log_row.stat_datetime_of_game.timestamp()) + str(game_log_row.player.id) in all_logs_dict:
            # print("if")
            all_logs_dict[str(game_log_row.stat_datetime_of_game.timestamp()) + str(game_log_row.player.id)].update({game_log_row.stat_name:game_log_row})
        else:
            # print("else")
            all_logs_dict[str(game_log_row.stat_datetime_of_game.timestamp()) + str(game_log_row.player.id)] = {game_log_row.stat_name:game_log_row}
        # print(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)])
        # print(str(game_log_row.stat_id))
    # print(all_logs_dict)

    game_score_logs = PlayerHistoricalStatistics.objects.filter(stat_name__contains=sport + "_game_score")
    print("made dictionary")
    # print(all_logs_dict)
    for game_score_log_row in game_score_logs:
        # print(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)].items())
        # print(str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id))
        game_score = 1.0
        if game_score_log_row.player.sport.lower() == "nba":
            
            game_score = get_nba_raw_game_score(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_minutes_played"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_field_goals_made"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_field_goals_attempted"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_three_point_field_goals_made"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_three_point_field_goals_attempted"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_free_throws_made"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_free_throws_attempted"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_offensive_rebounds"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_defensive_rebounds"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_total_rebounds"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_assists"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_steals"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_blocks"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_turnovers"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_personal_fouls"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nba_points"].stat_value)
        if game_score_log_row.player.sport.lower() == "mlb":
            if game_score_log_row.player.position.lower() == "p":
                game_score = get_mlb_pitcher_raw_score(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_innings_pitched"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_batters_faced"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_hits_allowed"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_earned_runs"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_strikeouts"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_decision"].stat_value)
            else:
                game_score = get_mlb_hitter_raw_score(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_at_bats"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_runs"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_hits"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_rbi"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_homeruns"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_on_base_percentage"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["mlb_slugging_percentage"].stat_value)
        if game_score_log_row.player.sport.lower() == "nfl":
            print(game_score_log_row.player.name)
            if game_score_log_row.player.position.lower() in offensive_line_positions or game_score_log_row.player.position.lower() in other_positions:
                continue
            elif game_score_log_row.player.position.lower() in offensive_skill_positions:
                game_score = get_nfl_offensive_player_raw_score(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_passes_completed"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_passes_attempted"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_passing_yards"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_passing_touchdowns"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_interceptions"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_qb_rating"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_rushes_attempted"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_rushing_yards"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_rushing_touchdowns"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_fumbles"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_targets"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_receptions"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_receiving_yards"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_receiving_touchdowns"].stat_value)
            else:
                game_score = get_nfl_defensive_player_raw_score(all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_total_tackles"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_sacks"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_forced_fumbles"].stat_value, all_logs_dict[str(game_score_log_row.stat_datetime_of_game.timestamp()) + str(game_score_log_row.player.id)]["nfl_defensive_interceptions"].stat_value)
        game_score_log_row.stat_value = game_score
        game_score_log_row.save()

    # print("values")
    # for g in game_score_logs:
    #     print(str(g.stat_value))
    # print(game_score_logs)


def raw_scores_to_prices(sport):
    players = Player.objects.filter(sport=sport)
    for player in players:
        latest_game_score = player.playerhistoricalstatistics_set.filter(stat_name__contains="game_score").order_by("-stat_datetime_of_game")
        if latest_game_score:
            latest_game_score = latest_game_score[0]
            if latest_game_score.stat_value > 1:
                # print(latest_game_score.stat_datetime_of_game)
                # print(latest_game_score.stat_value)
                decimalized = decimal.Decimal(round(latest_game_score.stat_value, 2))
                player.current_performance_price = decimalized
            else:
                player.current_performance_price = decimal.Decimal(1.0)
        else:
            player.current_performance_price = decimal.Decimal(1.0)
        player.save()


calculate_raw_scores("nfl")
raw_scores_to_prices("nfl")

# ph = PlayerHistoricalStatistics.objects.filter(stat_name__contains="mlb", stat_created_at__lt="2020-05-24")#stat_created_at__lt=)#all()
# print(len(ph))
# for i in ph:
#     if i.stat_name == "mlb_strikouts":
#         i.stat_name = "mlb_strikeouts"
#         i.save()