import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()
# your imports, e.g. Django models
# from models import Location

import requests
from market.models import User, Player, Share, Team, Game
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint
from datetime import datetime, date, timedelta

import csv
import psycopg2

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


url = "http://data.nba.com/data/10s/v2015/json/mobile_teams/nba/2019/league/00_full_schedule.json"
request = http.get(url)
result = request.json()
months = result["lscd"]

right_now = datetime(2020, 3, 11) # really should be datetime.now()

print(right_now)

game_counter = 0
for month in months:
    month_games = month["mscd"]["g"]
    for game in month_games:
        game_to_add = Game()
        game_to_add.id = game["gid"]
        game_to_add.sport = 'NBA'
        # probably stands for ["home"]["team abbreviation"]
        home_team_abbreviation = game["h"]["ta"]
        home_team = Team.objects.filter(abbreviation=home_team_abbreviation)
        if home_team:
            home_team = home_team[0]
            game_to_add.home_team = home_team
        else:
            print("couldn't find abbreviation")
            print(home_team_abbreviation)
            continue
        away_team_abbreviation = game["v"]["ta"]
        away_team = Team.objects.filter(abbreviation=away_team_abbreviation)
        if away_team:
            away_team = away_team[0]
            game_to_add.away_team = away_team
        else:
            print("couldn't find abbreviation")
            print(away_team_abbreviation)
            continue
        game_time = game["etm"]
        if game_time != "TBD":
            game_to_add.game_time = game_time
        # else:
            # game_to_add.game_time = None
        game_to_add.save()
        
        # if right_now.
        # home_team.next_opponent = away_team
        # home_team.save()

        # away_team.next_opponent = home_team
        # away_team.save()

