import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import csv
from datetime import date
import unidecode
import cssutils
import psycopg2

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)

letters_of_the_alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

player_names = []
player_urls = []


def get_baseball_players():
    baseball_players_url_array = []
    base_url = "https://www.baseball-reference.com/players/"
    letter_string = "bcdefghijklmnopqrstuvwxyz"
    for letter in letters_of_the_alphabet:
        # if letter in letter_string:
        #     continue
        url = base_url + letter + '/'
        print(url)
        request = http.get(url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        list_of_players_container = souped_result.find("div", {"id":"div_players_"})
        list_of_players = list_of_players_container.findAll("p")
        for player in list_of_players:
            bold_tag = player.find("b")
            if bold_tag:
                if "2019" in bold_tag.get_text():
                    # print(player.find("a")["href"])
                    baseball_players_url_array.append({"name": player.find("a").get_text(), "url":player.find("a")["href"]})
                    # print(player.find("b").text)
    return baseball_players_url_array

def get_football_players():
    football_players_url_array = []
    base_url = "https://www.pro-football-reference.com/players/"
    letter_string = "abcdefghijklmnopqrstuv"
    for letter in letters_of_the_alphabet:
        if letter in letter_string:
            continue

        url = base_url + letter.upper() + '/'
        print(url)
        request = http.get(url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        list_of_players_container = souped_result.find("div", {"id":"div_players"})
        list_of_players = list_of_players_container.findAll("p")
        for player in list_of_players:
            bold_tag = player.find("b")
            if bold_tag:
                if "2019" in player.get_text():
                    print(player.find("a")["href"])
                    football_players_url_array.append("https://www.pro-football-reference.com" + player.find("a")["href"])
                    print(player.find("b").text)
    return football_players_url_array

def get_basketball_players():
    basketball_players_url_array = []
    base_url = "https://www.basketball-reference.com/players/"
    letter_string = "abcdef"
    for letter in letters_of_the_alphabet:
        if letter in letter_string:
            continue
        url = base_url + letter + '/'
        print(url)
        request = http.get(url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        list_of_players_container = souped_result.find("div", {"id":"all_players"}) # or should be "table", id="players"
        list_of_players = list_of_players_container.findAll("tr")
        for player in list_of_players:
            header_tag = player.find("th")
            strong_tag = header_tag.find("strong")
            if strong_tag:
                max_year = player.find("td", {"data-stat":"year_max"}).get_text()
                if max_year == "2020":
                    # print(header_tag.find("a")["href"])
                    basketball_players_url_array.append("https://www.basketball-reference.com" + player.find("a")["href"])
                    # print(header_tag.text)
    return basketball_players_url_array

# get_baseball_players()
# get_basketball_players()
# get_football_players()