import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import csv
from datetime import date
import unidecode
import cssutils
import psycopg2


# player_id = ''
# t = ''
# base_link = 'https://www.basketball-reference.com/teams/WAS/2020.html'
# base_individual_player_link = base_link + player_id + t
nba_team_abbreviations = ['ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DAL', 'DEN' 'DET', 'GSW', 'HOU', 'IND', 'LAC', 'LAL',
'MEM', 'MIA', 'MIL', 'MIN', 'NOP', 'NYK', 'OKC', 'ORL', 'PHI', 'PHX', 'POR', 'SAC', 'SAS', 'TOR', 'UTA', 'WAS']

nba_team_cities = ['Atlanta', 'Brooklyn', 'Boston', 'Charlotte', 'Chicago', 'Cleveland', 'Dallas', 'Denver', 'Detroit',
'Golden State', 'Houston', 'Indiana', 'LA Clippers', 'LA Lakers', 'Memphis', 'Miami', 'Milwaukee', 'Minnesota',
'New Orleans', 'New York', 'Oklahoma City', 'Orlando', 'Philadelphia', 'Phoenix', 'Portland', 'Sacramento',
'San Antonio', 'Toronto', 'Utah', 'Washington']

nba_teams_url_names = ['atlanta-hawks', 'boston-celtics', 'brooklyn-nets', 'charlotte-hornets', 'chicago-bulls',
'cleveland-cavaliers', 'dallas-mavericks', 'denver-nuggets', 'detroit-pistons', 'golden-state-warriors',
'houston-rockets', 'indiana-pacers', 'la-clippers', 'los-angeles-lakers', 'memphis-grizzlies',
'miami-heat', 'milwaukee-bucks', 'minnesota-timberwolves', 'new-orleans-pelicans', 'new-york-knicks',
'oklahoma-city-thunder', 'orlando-magic', 'philadelphia-76ers', 'phoenix-suns', 'portland-trail-blazers',
'sacramento-kings', 'san-antonio-spurs', 'toronto-raptors', 'utah-jazz', 'washington-wizards']

team_colors_array = [['#E03A3E', '#C1D32F'], ['#007A33', '#BA9653'], ['#000000', '#FFFFFF'], ['#1D1160', '#00788C'], ['#CE1141', '#000000'], ['#860038', '#041E42'],
['#00538C', '#002B5E'], ['#0E2240', '#FEC524'], ['#C8102E', '#1D42BA'], ['#1D428A', '#FFC72C'], ['#CE1141', '#000000'], ['#002D62', '#FDBB30'],
['#C8102E', '#1D428A'], ['#552583', '#FDB927'], ['#5D76A9', '#12173F'], ['#98002E', '#F9A01B'], ['#00471B', '#EEE1C6'], ['#0C2340', '#236192'],
['#0C2340', '#C8102E'], ['#006BB6', '#F58426'], ['#007AC1', '#EF3B24'], ['#0077C0', '#C4CED4'], ['#006BB6', '#ED174C'], ['#1D1160', '#E56020'],
['#E03A3E', '#000000'], ['#5A2D81', '#63727A'], ['#C4CED4', '#000000'], ['#CE1141', '#000000'], ['#002B5C', '#00471B'], ['#002B5C', '#E31837']]
# i = 0
full_teams_results_array = []

list_of_names = []
with open("nba_individual_player_list.csv", encoding="utf8") as csvfile:
    reader = csv.reader(csvfile)#, quoting=csv.QUOTE_NONE) # , quoting=csv.QUOTE_NONNUMERICchange contents to floats
    for row in reader: # each row is a list
        list_of_names.append(row[0])

# for x in list_of_names:
#     x = x.decode('utf8')


retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)
# print(list_of_names)
# print(list_of_names[0])
# print(type(list_of_names[0]))

bad_name_array = ['charlie brown', 'devon hall', 'dusty hannahs', 'amile jefferson', 'daryl macon',
'sviatoslav mykhailiuk', 'shamorie ponds', 'jarrod uthoff']
counter = 0
for name in list_of_names:
    counter += 1
    if counter < 230:
        continue

    # url = 'https://www.nba.com/players/bam/adebayo'
    if name in bad_name_array:
        continue

    url_first_letter = name[0]
    first_and_last = name.split(' ')
    url_name = first_and_last[1][:5] + first_and_last[0][:2] + '01'
    if url_name == 'augusd.01':
        url_name = 'augusdj01'
    if url_name == 'bareaj.01':
        url_name = 'bareajo01'

    # unidecode.unidecode(name.split(' ')[0]) +'/' + unidecode.unidecode(name.split(' ')[1])
    url = 'https://www.basketball-reference.com/players/' + url_first_letter + '/' + url_name

    print(url)
    request = http.get(url)
    result_html = request.text
    souped_result = BeautifulSoup(result_html)
    # print(souped_result)

#     player_image_section = souped_result.findAll('section', {'class':'nba-player-header__item nba-player-header__headshot'})[0]
# # how to get src of image and then how to serialize it
#     player_image = player_image_section.findAll('img')[0]['src']
#     print(player_image)
#     playing_details = souped_result.findAll('p', {'class':'nba-player-header__details-top'})[0]
    # logo you can get here

    #TODO team name, team color
    # number = playing_details.findAll('span')[0].text.strip()[1:]
    # print(number)
    # position = playing_details.findAll('span')[2].text.strip()
    # print(position)


    # height_details = souped_result.findAll('section', {'class':'nba-player-vitals__top-left'})[0]
    # height = height_details.findAll('p', {'class':'nba-player-vitals__top-info-imperial'})[0].text.strip()
    # weight_details = souped_result.findAll('section', {'class':'nba-player-vitals__top-right'})[0]

    # weight = weight_details.findAll('p', {'class':'nba-player-vitals__top-info-imperial'})[0].text.split(' ')[0]
    # print(weight)

    # print(len(current_team_results_array))
    # full_teams_results_array.append(current_team_results_array)
print(player_array)
# with open("nba_individual_player_stats_and_price.csv","w+", newline="") as my_csv:
#     csvWriter = csv.writer(my_csv,delimiter=',')
#     csvWriter.writerows(player_array)

