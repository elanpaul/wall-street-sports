import requests
from bs4 import BeautifulSoup
import csv
from datetime import date
import psycopg2

player_id = ''
t = ''
base_link = 'https://www.basketball-reference.com/teams/WAS/2020.html'
base_individual_player_link = base_link + player_id + t
nba_teams = ['ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DAL', 'DEN' 'DET', 'GSW', 'HOU', 'IND', 'LAC', 'LAL',
'MEM', 'MIA', 'MIL', 'MIN', 'NOP', 'NYK', 'OKC', 'ORL', 'PHI', 'PHX', 'POR', 'SAC', 'SAS', 'TOR', 'UTA', 'WAS']

nba_team_names = ['Atlanta', 'Brooklyn', 'Boston', 'Charlotte', 'Chicago', 'Cleveland', 'Dallas', 'Denver', 'Detroit',
'Golden State', 'Houston', 'Indiana', 'LA Clippers', 'LA Lakers', 'Memphis', 'Miami', 'Milwaukee', 'Minnesota',
'New Orleans', 'New York', 'Oklahoma City', 'Orlando', 'Philadelphia', 'Phoenix', 'Portland', 'Sacramento',
'San Antonio', 'Toronto', 'Utah', 'Washington']

nba_teams_url_names = ['atlanta-hawks', 'boston-celtics', 'brooklyn-nets', 'charlotte-hornets', 'chicago-bulls',
'cleveland-cavaliers', 'dallas-mavericks', 'denver-nuggets', 'detroit-pistons', 'golden-state-warriors',
'houston-rockets', 'indiana-pacers', 'la-clippers', 'los-angeles-lakers', 'memphis-grizzlies',
'miami-heat', 'milwaukee-bucks', 'minnesota-timberwolves', 'new-orleans-pelicans', 'new-york-knicks',
'oklahoma-city-thunder', 'orlando-magic', 'philadelphia-76ers', 'phoenix-suns', 'portland-trail-blazers',
'sacramento-kings', 'san-antonio-spurs', 'toronto-raptors', 'utah-jazz', 'washington-wizards']
# i = 0
full_teams_results_array = []

url = 'https://www.basketball-reference.com/leagues/NBA_2020_per_game.html'
request = requests.get(url)
result_html = request.text
souped_result = BeautifulSoup(result_html)
# print(souped_result)
main_table = souped_result.findAll('table')[0]
# print(main_table[0])
# print(len(main_table))
main_body = main_table.find('tbody')
# print(main_body)
all_data_rows = main_body.findAll('tr', {'class':'full_table'})
counter = 0
    # print(team)

player_array = []
for row in all_data_rows:
    counter+=1
    # print(counter)
    if counter > 15:
        break
    current_player_array = []
    current_tds = row.findAll('td')
    # print(current_tds)
    # print(main_table)
    player_name = current_tds[0].text.strip().lower()
    games_played = int(row.find('td', {'data-stat':'g'}).text.strip())
    minutes_played = float(row.find('td', {'data-stat':'mp_per_g'}).text.strip())
    field_goals_made = float(row.find('td', {'data-stat':'fg_per_g'}).text.strip())
    field_goals_attempted = float(row.find('td', {'data-stat':'fga_per_g'}).text.strip())
    field_goals_percentage = (row.find('td', {'data-stat':'fg_pct'}).text.strip())
    three_point_field_goals_made = float(row.find('td', {'data-stat':'fg3_per_g'}).text.strip())
    three_point_field_goals_attempted = float(row.find('td', {'data-stat':'fg3a_per_g'}).text.strip())
    three_point_field_goals_percentage = (row.find('td', {'data-stat':'fg3_pct'}).text.strip())
    free_throws_made = float(row.find('td', {'data-stat':'ft_per_g'}).text.strip())
    free_throws_attempted = float(row.find('td', {'data-stat':'fta_per_g'}).text.strip())
    free_throws_percentage = (row.find('td', {'data-stat':'ft_pct'}).text.strip())
    offensive_rebounds = float(row.find('td', {'data-stat':'orb_per_g'}).text.strip())
    defensive_rebounds = float(row.find('td', {'data-stat':'drb_per_g'}).text.strip())
    total_rebounds = float(row.find('td', {'data-stat':'trb_per_g'}).text.strip())
    assists = float(row.find('td', {'data-stat':'ast_per_g'}).text.strip())
    steals = float(row.find('td', {'data-stat':'stl_per_g'}).text.strip())
    blocks = float(row.find('td', {'data-stat':'blk_per_g'}).text.strip())
    turnovers = float(row.find('td', {'data-stat':'tov_per_g'}).text.strip())
    personal_fouls = float(row.find('td', {'data-stat':'pf_per_g'}).text.strip())
    points = float(row.find('td', {'data-stat':'pts_per_g'}).text.strip())
    current_player_array.append(counter)
    # current_player_array.append(player_name)
    current_player_array.append(minutes_played)
    current_player_array.append(field_goals_made)
    current_player_array.append(field_goals_attempted)
    # current_player_array.append(field_goals_percentage)
    current_player_array.append(three_point_field_goals_made)
    current_player_array.append(three_point_field_goals_attempted)
    # current_player_array.append(three_point_field_goals_percentage)
    current_player_array.append(free_throws_made)
    current_player_array.append(free_throws_attempted)
    # current_player_array.append(free_throws_percentage)
    current_player_array.append(offensive_rebounds)
    current_player_array.append(defensive_rebounds)
    current_player_array.append(total_rebounds)
    current_player_array.append(assists)
    current_player_array.append(steals)
    current_player_array.append(blocks)
    current_player_array.append(turnovers)
    current_player_array.append(points)

    current_player_array.append(counter)
    current_player_array.append(personal_fouls)
    current_player_array.append(games_played)


    # print(counter)
    player_array.append(current_player_array)
# print(len(current_team_results_array))
# full_teams_results_array.append(current_team_results_array)
print(player_array)



conn = psycopg2.connect(host='127.0.0.1', port='5432',user='postgres',password='PostgresElanPaul3526!',database='sportsstockmarketdb') # To remove slash
# PostgresElanPaul3526
# sportsstockmarketdb
cursor = conn.cursor()

cursor.executemany("INSERT INTO market_playercurrentstatistics VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", player_array)
conn.commit() # <- We MUST commit to reflect the inserted data
cursor.close()
conn.close()
# with open("nba_individual_player_stats_new.csv","w+", newline="", encoding='utf8') as my_csv: #   
#     csvWriter = csv.writer(my_csv) #, delimiter=',', quoting=csv.QUOTE_NONE)
#     csvWriter.writerows(player_array)

