import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team, PlayerCurrentStatistics
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer, PlayerCurrentStatisticsSerializer
from bs4 import BeautifulSoup
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint
from datetime import datetime

import get_player_names_and_sports_reference_urls


player_colors_array = [['#E03A3E', '#C1D32F'], ['#007A33', '#BA9653'], ['#000000', '#FFFFFF'], ['#1D1160', '#00788C'], ['#CE1141', '#000000'], ['#860038', '#041E42'],
['#00538C', '#002B5E'], ['#0E2240', '#FEC524'], ['#C8102E', '#1D42BA'], ['#1D428A', '#FFC72C'], ['#CE1141', '#000000'], ['#002D62', '#FDBB30'],
['#C8102E', '#1D428A'], ['#552583', '#FDB927'], ['#5D76A9', '#12173F'], ['#98002E', '#F9A01B'], ['#00471B', '#EEE1C6'], ['#0C2340', '#236192'],
['#0C2340', '#C8102E'], ['#006BB6', '#F58426'], ['#007AC1', '#EF3B24'], ['#0077C0', '#C4CED4'], ['#006BB6', '#ED174C'], ['#1D1160', '#E56020'],
['#E03A3E', '#000000'], ['#5A2D81', '#63727A'], ['#C4CED4', '#000000'], ['#CE1141', '#000000'], ['#002B5C', '#00471B'], ['#002B5C', '#E31837']]


retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)




def get_value_or_default(data, default):
    if data:
        if data.get_text().strip() != "":
            if isinstance(default, int):
                data = int(data.get_text().strip())
            elif isinstance(default, float):
                data = float(data.get_text().strip())
            else:
                data = str(data.get_text().strip())
        else:
            data = default
    else:
        data = default
    return data

def get_value_or_default_for_nba_json(data, default):
    if data == "":
        return 0
    else:
        return float(data)

def create_player_current_statistics_row(id, stat_name, stat_value, stat_type, player):
    player_current_stats_set = player.player_current_stats
    if player_current_stats_set:
        player_current_stats_set.filter(stat_name=stat_name).delete()
    player_statistics_row = PlayerCurrentStatistics()
    # player_statistics_row.id = id
    player_statistics_row.stat_name = stat_name
    player_statistics_row.stat_value = stat_value
    player_statistics_row.stat_type = stat_type
    player_statistics_row.stat_created_at = datetime.now()
    player_statistics_row.player = player
    player_statistics_row.save()

def getNBAPlayerCurrentStatistics():
    base_url = "https://data.nba.net/10s/prod/v1/2019/players/"

    players = Player.objects.filter(sport="nba")
    counter = 1
    for player in players:
        player_id = player.id
        url = base_url + str(player_id) + "_profile.json"
        print(url)
        request = http.get(url)
        result = request.json()
        stats = result["league"]["standard"]["stats"]["latest"]

        games_played = stats["gamesPlayed"]
        if games_played == '':
            games_played = 0
        else:
            games_played = int(stats["gamesPlayed"])

        games_played = get_value_or_default_for_nba_json(stats["gamesPlayed"], 0)
        minutes_played = get_value_or_default_for_nba_json(stats["min"], 0.0)
        field_goals_made = get_value_or_default_for_nba_json(stats["fgm"], 0)
        field_goals_attempted = get_value_or_default_for_nba_json(stats["fga"], 0)
        three_point_field_goals_made = get_value_or_default_for_nba_json(stats["tpm"], 0)
        three_point_field_goals_attempted = get_value_or_default_for_nba_json(stats["tpa"], 0)
        free_throws_made = get_value_or_default_for_nba_json(stats["ftm"], 0)
        free_throws_attempted = get_value_or_default_for_nba_json(stats["fta"], 0)
        offensive_rebounds = get_value_or_default_for_nba_json(stats["offReb"], 0)
        defensive_rebounds = get_value_or_default_for_nba_json(stats["defReb"], 0)
        total_rebounds = get_value_or_default_for_nba_json(stats["rpg"], 0)
        assists = get_value_or_default_for_nba_json(stats["apg"], 0)
        steals = get_value_or_default_for_nba_json(stats["steals"], 0)
        blocks = get_value_or_default_for_nba_json(stats["blocks"], 0)
        turnovers = get_value_or_default_for_nba_json(stats["turnovers"], 0)
        player_fouls = get_value_or_default_for_nba_json(stats["pFouls"], 0)
        total_points = get_value_or_default_for_nba_json(stats["ppg"], 0.0)


        # create new PlayerCurrentStatistics Objects
        # should i add games_started?  
        create_player_current_statistics_row(counter, "nba_games_played", games_played, "float", player)
        create_player_current_statistics_row(counter+1, "nba_minutes_played", minutes_played, "float", player)
        create_player_current_statistics_row(counter+2, "nba_field_goals_made", field_goals_made, "float", player)
        create_player_current_statistics_row(counter+3, "nba_field_goals_attempted", field_goals_attempted, "float", player)
        create_player_current_statistics_row(counter+4, "nba_three_point_field_goals_made", three_point_field_goals_made, "float", player)
        create_player_current_statistics_row(counter+5, "nba_three_point_field_goals_attempted", three_point_field_goals_made, "float", player)
        create_player_current_statistics_row(counter+6, "nba_free_throws_made", free_throws_made, "float", player)
        create_player_current_statistics_row(counter+7, "nba_free_throws_attempted", free_throws_attempted, "float", player)
        create_player_current_statistics_row(counter+8, "nba_offensive_rebounds", offensive_rebounds, "float", player)
        create_player_current_statistics_row(counter+9, "nba_defensive_rebounds", defensive_rebounds, "float", player)
        create_player_current_statistics_row(counter+10, "nba_total_rebounds", total_rebounds, "float", player)
        create_player_current_statistics_row(counter+11, "nba_assists", assists, "float", player)
        create_player_current_statistics_row(counter+12, "nba_steals", steals, "float", player)
        create_player_current_statistics_row(counter+13, "nba_blocks", blocks, "float", player)
        create_player_current_statistics_row(counter+14, "nba_turnovers", turnovers, "float", player)
        create_player_current_statistics_row(counter+15, "nba_personal_fouls", player_fouls, "float", player)
        # should it be points
        create_player_current_statistics_row(counter+16, "nba_total_points", total_points, "float", player)


        counter+=17


def prepare_nfl_qb_player(counter, player, current_salary, games_played, passes_completed, passes_attempted, passing_yards, passing_touchdowns, interceptions, qb_rating, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, targets, receptions, receiving_yards, receiving_touchdowns):
    create_player_current_statistics_row(counter, "nfl_current_salary", current_salary, "integer", player)
    create_player_current_statistics_row(counter+1, "nfl_games_played", games_played, "integer", player)
    create_player_current_statistics_row(counter+2, "nfl_passes_completed", passes_completed, "integer", player)
    create_player_current_statistics_row(counter+3, "nfl_passes_attempted", passes_attempted, "integer", player)
    create_player_current_statistics_row(counter+4, "nfl_passing_yards", passing_yards, "integer", player)
    create_player_current_statistics_row(counter+5, "nfl_passing_touchdowns", passing_touchdowns, "integer", player)
    create_player_current_statistics_row(counter+6, "nfl_interceptions", interceptions, "integer", player)
    create_player_current_statistics_row(counter+7, "nfl_qb_rating", qb_rating, "float", player)
    create_player_current_statistics_row(counter+8, "nfl_rushes_attempted", rushes_attempted, "integer", player)
    create_player_current_statistics_row(counter+9, "nfl_rushing_yards", rushing_yards, "integer", player)
    create_player_current_statistics_row(counter+10, "nfl_rushing_touchdowns", rushing_touchdowns, "integer", player)
    create_player_current_statistics_row(counter+11, "nfl_fumbles", fumbles, "integer", player)
    create_player_current_statistics_row(counter+12, "nfl_targets", targets, "integer", player)
    create_player_current_statistics_row(counter+13, "nfl_receptions", receptions, "integer", player)
    create_player_current_statistics_row(counter+14, "nfl_receiving_yards", receiving_yards, "integer", player)
    create_player_current_statistics_row(counter+15, "nfl_receiving_touchdowns", receiving_touchdowns, "integer", player)

def prepare_nfl_defensive_player(counter, player, games_played, total_tackles, sacks, forced_fumbles, defensive_interceptions):
    create_player_current_statistics_row(counter, "nfl_current_salary", current_salary, "integer", player)
    create_player_current_statistics_row(counter+1, "nfl_games_played", games_played, "integer", player)
    create_player_current_statistics_row(counter+2, "nfl_total_tackles", total_tackles, "integer", player)
    create_player_current_statistics_row(counter+3, "nfl_sacks", sacks, "integer", player)
    create_player_current_statistics_row(counter+4, "nfl_forced_fumbles", forced_fumbles, "integer", player)
    create_player_current_statistics_row(counter+5, "nfl_defensive_interceptions", defensive_interceptions, "integer", player)

def get_nfl_player_current_statistics(football_players_url_array):
    not_found_players = []
    player_counter = 1
    id_counter = 1
    player_array = []
    position_array = []
    offensive_skill_positions = ["qb", "qb throws", "rb", "fb", "wr", "wr throws", "te"] # obvs throws is a mistake
    offensive_line_positions = ["lt", "rt", "ot", "t", "lg", "rg", "og", "g", "ol", "c"]
    defensive_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl", "lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb", "cb", "ss", "fs", "s"]
    defensive_line_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl"]
    defensive_linebacker_positions = ["lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb"]
    defensive_back_positions = ["cb", "ss", "fs", "s"]
    other_positions = ["p", "k"]
    # players = Player.objects.filter(sport="NBA")

    for url in football_players_url_array:
        full_url = url
        print(full_url)
        request = http.get(full_url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        # print(souped_result.encode("utf-8"))
        meta_data = souped_result.find("div", {"id":"meta"})
        # print("meta_data")
        # print(meta_data)
        header = souped_result.find("h1", {"itemprop":"name"})
        # print("header")
        # print(header)
        lookup_name = header.get_text().lower()#[:-17]
        player = Player.objects.filter(sport="nfl", name=lookup_name)
        # testp = Player.objects.filter(sport="nfl").order_by("name")[5:20] #, name__trigram_similar=lookup_name
        # print(loo)
        print(lookup_name.encode("utf-8"))
        # for x in testp:
        #     print(x.name)
        if player:
            # print("found")
            player = player[0]
            if player.position not in position_array:
                position_array.append(player.position)
        else:
            not_found_players.append(lookup_name)
            print("couldnt find player " + str(lookup_name.encode("utf-8")))
            continue
        # main_div = souped_result.find('div', {"class":"stats_pullout"})
        # if main_div:
        #     main_div = main_div[0]
        # else:
        #     print("couldnt find main div")

        main_table = souped_result.find('table', {"class":"stats_table"})
        # if main_table:
        #     main_table = main_table[0]
        # else:
        #     print("couldnt find main div")
        main_body = main_table.find('tbody')
        all_data_rows = main_body.findAll('tr') #, {'class':'full_table'}
        print(len(all_data_rows))
        latest_year_data_row = all_data_rows[len(all_data_rows) - 1]
        row = latest_year_data_row
        game_counter = 1
        player_counter += 1
        current_player_array = []

        games_played = get_value_or_default(row.find('td', {'data-stat':'g'}), 0)
        # games_started = get_value_or_default(row.find('td', {'data-stat':'gs'}), 0)

        all_header_p_tags = meta_data.findAll('p')
        current_salary = 0
        for tag in all_header_p_tags:
            if 'Current salary' in tag.get_text():
                current_salary = int(tag.get_text().split(' ')[2])
                print(current_salary)

        # after this, the stats depend on the position

        # if "throws" in player.position.lower():

        if player.position.lower() in offensive_skill_positions:
            passes_completed = get_value_or_default(row.find('td', {'data-stat':'pass_cmp'}), 0)
            passes_attempted = get_value_or_default(row.find('td', {'data-stat':'pass_att'}), 0)
            passing_yards = get_value_or_default(row.find('td', {'data-stat':'pass_yds'}), 0)
            passing_touchdowns = get_value_or_default(row.find('td', {'data-stat':'pass_td'}), 0)
            interceptions = get_value_or_default(row.find('td', {'data-stat':'pass_int'}), 0)
            qb_rating = get_value_or_default(row.find('td', {'data-stat':'pass_rating'}), 0.0)

            rushes_attempted = get_value_or_default(row.find('td', {'data-stat':'rush_att'}), 0)
            rushing_yards = get_value_or_default(row.find('td', {'data-stat':'rush_yds'}), 0)
            rushing_touchdowns = get_value_or_default(row.find('td', {'data-stat':'rush_td'}), 0)
            fumbles = get_value_or_default(row.find('td', {'data-stat':'fumbles'}), 0)

            targets = get_value_or_default(row.find('td', {'data-stat':'targets'}), 0)
            receptions = get_value_or_default(row.find('td', {'data-stat':'rec'}), 0)
            receiving_yards = get_value_or_default(row.find('td', {'data-stat':'rec_yds'}), 0)
            receiving_touchdowns = get_value_or_default(row.find('td', {'data-stat':'rec_td'}), 0)
            prepare_nfl_qb_player(id_counter, player, current_salary, games_played, passes_completed, passes_attempted, passing_yards, passing_touchdowns, interceptions, qb_rating, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, targets, receptions, receiving_yards, receiving_touchdowns)
            id_counter += 16
        elif player.position.lower() in offensive_line_positions:
            continue
        elif player.position.lower() in other_positions:
            continue
        elif player.position.lower() in defensive_positions:
            defensive_interceptions = get_value_or_default(row.find('td', {'data-stat':'def_int'}), 0)
            forced_fumbles = get_value_or_default(row.find('td', {'data-stat':'fumbles_forced'}), 0)
            sacks = get_value_or_default(row.find('td', {'data-stat':'sacks'}), 0.0)
            total_tackles = get_value_or_default(row.find('td', {'data-stat':'tackles_combined'}), 0)
            # total_tackles = int(row.find('td', {'data-stat':'tackles_solo'}).text.strip())
            # total_tackles = int(row.find('td', {'data-stat':'tackles_assists'}).text.strip())
            # total_tackles = int(row.find('td', {'data-stat':'tackles_loss'}).text.strip())
            prepare_nfl_defensive_player(id_counter, player, current_salary, games_played, total_tackles, sacks, forced_fumbles, defensive_interceptions)
            id_counter += 6

        else:
            print("not in a category: " + player.position)
            continue
    print("players that weren't found:")
    for x in not_found_players:
        print(x.encode("utf-8"))

def prepare_mlb_pitcher_player(id_counter, player, games_played, innings_pitched, batters_faced, hits_allowed, earned_runs, strikeouts, wins, losses, saves):
    create_player_current_statistics_row(id_counter, "mlb_games_played", games_played, "integer", player)
    create_player_current_statistics_row(id_counter+1, "mlb_innings_pitched", innings_pitched, "integer", player)
    create_player_current_statistics_row(id_counter+2, "mlb_batters_faced", batters_faced, "integer", player)
    create_player_current_statistics_row(id_counter+3, "mlb_hits_allowed", hits_allowed, "integer", player)
    create_player_current_statistics_row(id_counter+4, "mlb_earned_runs", earned_runs, "integer", player)
    create_player_current_statistics_row(id_counter+5, "mlb_strikouts", strikeouts, "integer", player)
    create_player_current_statistics_row(id_counter+6, "mlb_wins", wins, "integer", player)
    create_player_current_statistics_row(id_counter+7, "mlb_losses", losses, "integer", player)
    create_player_current_statistics_row(id_counter+8, "mlb_saves", saves, "integer", player)

def prepare_mlb_hitter_player(id_counter, player, games_played, at_bats, runs, hits, rbi, homeruns, on_base_percentage, slugging_percentage):
    create_player_current_statistics_row(id_counter, "mlb_games_played", games_played, "integer", player)
    create_player_current_statistics_row(id_counter+1, "mlb_at_bats", at_bats, "integer", player)
    create_player_current_statistics_row(id_counter+2, "mlb_runs", runs, "integer", player)
    create_player_current_statistics_row(id_counter+3, "mlb_hits", hits, "integer", player)
    create_player_current_statistics_row(id_counter+4, "mlb_rbi", rbi, "integer", player)
    create_player_current_statistics_row(id_counter+5, "mlb_homeruns", homeruns, "integer", player)
    create_player_current_statistics_row(id_counter+6, "mlb_on_base_percentage", on_base_percentage, "float", player)
    create_player_current_statistics_row(id_counter+7, "mlb_slugging_percentage", slugging_percentage, "float", player)

def get_mlb_player_current_statistics(baseball_players_url_array):
    not_found_players = []
    player_counter = 1
    id_counter = 1
    player_array = []
    position_array = []

    found_will = False
    first_will = False
    for player_dict in baseball_players_url_array:
        lookup_name = player_dict["name"].lower()#[:-17]
        player = Player.objects.filter(sport="mlb", name=lookup_name)

        
        # testp = Player.objects.filter(sport="mlb").order_by("name")[5:20] #, name__trigram_similar=lookup_name
        # print(loo)
        print(lookup_name.encode("utf-8"))
        # for x in testp:
        #     print(x.name)
        if player:
            # print("found")
            player = player[0]
            if player.position not in position_array:
                position_array.append(player.position)
        else:
            not_found_players.append(lookup_name)
            print("couldnt find player " + str(lookup_name.encode("utf-8")))
            continue


        if lookup_name == "will smith":
            if found_will == False:
                player = Player.objects.filter(sport="mlb", name=lookup_name)[0]
                found_will = True
            else:
                player = Player.objects.filter(sport="mlb", name=lookup_name)[1]

                
        # https://www.basketball-reference.com/leagues/NBA_2020_per_game.html
        #TODO right now it's just the current year, but for the historical table, it must be every year

        # print(player_dict["url"])
        url = player_dict["url"]
        base_url = "https://www.baseball-reference.com"
        full_url = base_url + url
        print(full_url)
        request = http.get(full_url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        # print(souped_result.encode("utf-8"))
        meta_data = souped_result.find("div", {"id":"meta"})
        # print("meta_data")
        # print(meta_data)
        header = souped_result.find("h1", {"itemprop":"name"})
        # print("header")
        # print(header)
        main_table = souped_result.find('table', {"class":"stats_table"})
        # if main_table:
        #     main_table = main_table[0]
        # else:
        #     print("couldnt find main div")
        main_body = main_table.find('tbody')
        all_data_rows = main_body.findAll('tr') #, {'class':'full_table'}
        print(len(all_data_rows))
        latest_year_data_row = all_data_rows[len(all_data_rows) - 1]
        row = latest_year_data_row
        game_counter = 1
        player_counter += 1
        current_player_array = []

        games_played = get_value_or_default(row.find('td', {'data-stat':'G'}), 0)

        if player.position.lower() == "p":
            innings_pitched = get_value_or_default(row.find('td', {'data-stat':'IP'}), 0.0)
            batters_faced = get_value_or_default(row.find('td', {'data-stat':'R'}), 0)
            hits_allowed = get_value_or_default(row.find('td', {'data-stat':'H'}), 0)
            earned_runs = get_value_or_default(row.find('td', {'data-stat':'ER'}), 0)
            strikeouts = get_value_or_default(row.find('td', {'data-stat':'SO'}), 0)
            wins = get_value_or_default(row.find('td', {'data-stat':'W'}), 0)
            losses = get_value_or_default(row.find('td', {'data-stat':'L'}), 0)
            # holds = get_value_or_default(row.find('td', {'data-stat':'player_game_result'}), 0)
            saves = get_value_or_default(row.find('td', {'data-stat':'SV'}), 0)

            prepare_mlb_pitcher_player(id_counter, player, games_played, innings_pitched, batters_faced, hits_allowed, earned_runs, strikeouts, wins, losses, saves)
            id_counter += 8
        else:
            at_bats = get_value_or_default(row.find('td', {'data-stat':'AB'}), 0)
            runs = get_value_or_default(row.find('td', {'data-stat':'R'}), 0)
            hits = get_value_or_default(row.find('td', {'data-stat':'H'}), 0)
            rbi = get_value_or_default(row.find('td', {'data-stat':'RBI'}), 0)
            homeruns = get_value_or_default(row.find('td', {'data-stat':'HR'}), 0)
            on_base_percentage = get_value_or_default(row.find('td', {'data-stat':'onbase_perc'}), 0.0)
            slugging_percentage = get_value_or_default(row.find('td', {'data-stat':'slugging_perc'}), 0.0)

            prepare_mlb_hitter_player(id_counter, player, games_played, at_bats, runs, hits, rbi, homeruns, on_base_percentage, slugging_percentage)
            id_counter += 7

    print("players that weren't found:")
    for x in not_found_players:
        print(x.encode("utf-8"))
    print("all positions:")
    for y in position_array:
        print(y)



# getNBAPlayerCurrentStatistics()

    
# basketball_players_url_array = get_player_names_and_sports_reference_urls.get_basketball_players()
# get_nba_player_current_statistics(basketball_players_url_array)

football_players_url_array = get_player_names_and_sports_reference_urls.get_football_players()
get_nfl_player_current_statistics(football_players_url_array)

# baseball_players_url_array = get_player_names_and_sports_reference_urls.get_baseball_players()
# get_mlb_player_current_statistics(baseball_players_url_array)

# PlayerCurrentStatistics.objects.all().delete()



