import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team, PlayerCurrentStatistics
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer, PlayerCurrentStatisticsSerializer
from bs4 import BeautifulSoup
import csv
from datetime import date
from datetime import datetime
import psycopg2
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


def add_basketball_images_from_heroku_app():
    base_url = "https://nba-players.herokuapp.com/players/"
    players = Player.objects.filter(sport="nba")
    counter = 0
    for player in players:
        counter += 1
        # if counter > 5:
        #     break
        name = player.name.lower()
        player_first_name = name.split(" ")[0]
        player_last_name = name.split(" ")[1]
        url = base_url + player_last_name + "/" + player_first_name
        response = http.get(url)
        if response:
            image_name = "C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket\\market\\static\\market\\images\\players\\nba_old_alt\\" + player_first_name + "_" + player_last_name + ".png"
            file = open(image_name, "wb")
            file.write(response.content)
            file.close()

def add_basketball_images_from_official_site():
    base_url = "https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/"
    players = Player.objects.filter(sport="nba")
    counter = 0
    for player in players:
        print(str(counter))
        counter += 1
        # if counter > 5:
        #     break
        name = player.name.lower().replace(" ", "_")
        url = base_url + str(player.id) + ".png"
        response = http.get(url)
        if response:
            image_name = "C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket\\market\\static\\market\\images\\players\\nba\\" + name + ".png"
            file = open(image_name, "wb")
            file.write(response.content)
            file.close()

def add_football_images():
    letters_of_the_alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    base_url = "https://www.nfl.com"
    for letter in letters_of_the_alphabet:
        # if letter != "a":
        #     continue
        full_url = base_url + "/players/active/" + letter
        request = http.get(full_url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        print(full_url)
        next_url = soupify_and_download(souped_result)
        while(next_url != ""):
            full_url = base_url + next_url
            request = http.get(full_url)
            result_html = request.text
            souped_result = BeautifulSoup(result_html)
            print(full_url)
            next_url = soupify_and_download(souped_result)

        
    

def soupify_and_download(souped_result):
    players_not_found_images = []

    # request = http.get(full_url)
    # result_html = request.text
    # souped_result = BeautifulSoup(result_html)
    # print(souped_result)
    main_div = souped_result.find("div", {"class":"nfl-c-player-directory"})
    if not main_div:
        print("no main div")
    table_body = main_div.find("tbody")
    rows = table_body.findAll("tr")
    for row in rows:
        player_container = row.find("div", {"class":"d3-o-media-object"})
        
        player_name = player_container.find("a")
        if player_name:
            player_name = player_name.get_text().strip().lower().replace(" ", "_")
            if player_name == None:
                player_name = player_container.find("a")["href"].split("/")[-2].replace("-", "_")

            
            print(player_name)
        player_image_link_container = player_container.find("picture")
        if not player_image_link_container:
            players_not_found_images.append(player_name)
            continue
        player_image = player_image_link_container.find("source")#img
        print(player_image["srcset"].split(" ")[0].replace("t_lazy/", ""))#src
        
        if player_image:
            # continue
            image_response = http.get(player_image["srcset"].split(" ")[0].replace("t_lazy/", ""))
            image_name = "C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket\\market\\static\\market\\images\\players\\nfl\\" + player_name + ".png"
            file = open(image_name, "wb")
            file.write(image_response.content)
            file.close()
    next_link = souped_result.find("a", {"class":"nfl-o-table-pagination__next"})
    print("not found players: ")
    for p in players_not_found_images:
        print(p)
    if next_link:
        return next_link["href"]
    else:
        return ""

# add_football_images()
add_basketball_images_from_official_site()