import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team, PlayerHistoricalStatistics
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint


players = Player.objects.all()

counter = 0
for player in players:
    history = PlayerHistoricalStatistics.objects.filter(player=player).order_by("game_number").reverse()[:2]
    if len(history) == 0:
        print("didn't get enough games")
        counter += 1
        continue
    else:
        player.current_price = history[0].game_score
        player.save()
        if len(history) == 1:
            print("got some games")
            counter += 1
            continue
        else:
            player.delta = history[0].game_score - history[1].game_score
            player.save()
    # print(history)
print(counter)