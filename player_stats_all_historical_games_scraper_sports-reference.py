import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team, PlayerCurrentStatistics, PlayerHistoricalStatistics
from bs4 import BeautifulSoup
import csv
from datetime import date
from datetime import datetime
import psycopg2

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

import get_player_names_and_sports_reference_urls

from django.conf import settings
from django.utils.timezone import make_aware



retry_strategy = Retry(
    total=10,
    status_forcelist=[403, 404, 429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


def add_inactive_game_to_list(row, game_id_string, player, id_counter):
    game_number_of_season = int(row.find('th', {'data-stat':'ranker'}).text.strip())
    game_date = row.find('td', {'data-stat':'date_game'}).text.strip()
    game_date = make_aware(datetime.strptime(game_date, "%Y-%m-%d"))
    # must chnge this to be dynamic to sports
    team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'team_id'}).text.strip().lower(), "nba") # this should be a foreign key

    home_or_away = row.find('td', {'data-stat':'game_location'}).text.strip()
    if home_or_away == "":
        home_or_away = 1
    else:
        home_or_away = 0
    opponent_team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'opp_id'}).text.strip().lower, "nba")
    game_id = int(game_id_string)
    prepare_nba_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0)
    return 1

def get_value_or_default(data, default):
    if data:
        if data.get_text().strip() != "":
            if isinstance(default, int):
                data = int(data.get_text().strip())
            elif isinstance(default, float):
                data = float(data.get_text().strip())
            else:
                data = str(data.get_text().strip())
        else:
            data = default
    else:
        data = default
    return data

def team_abbreviation_to_object_id(team_abbreviation, team_sport):
    if team_sport == "nba":
        if team_abbreviation == "brk":
            team_abbreviation = "bkn"
        if team_abbreviation == "cho":
            team_abbreviation = "ch"
        if team_abbreviation == "pho":
            team_abbreviation = "phx"
    if team_sport == "nfl":
        if team_abbreviation == "gnb":
            team_abbreviation = "gb"
        if team_abbreviation == "kan":
            team_abbreviation = "kc"
        if team_abbreviation == "nor":
            team_abbreviation = "no"
        if team_abbreviation == "nwe":
            team_abbreviation = "ne"
        if team_abbreviation == "sfo":
            team_abbreviation = "sf"
        if team_abbreviation == "tam":
            team_abbreviation = "tb"
    if team_sport == "mlb":
        if team_abbreviation == "chw":
            team_abbreviation = "cws"
        if team_abbreviation == "kcr":
            team_abbreviation = "kc"
        if team_abbreviation == "sdp":
            team_abbreviation = "sd"
        if team_abbreviation == "sfg":
            team_abbreviation = "sf"
        if team_abbreviation == "tbr":
            team_abbreviation = "tb"
        if team_abbreviation == "wsn":
            team_abbreviation = "wsh"
    team = Team.objects.filter(sport=team_sport, abbreviation=team_abbreviation)
    # ex = Team.objects.filter(sport="mlb")
    # for e in ex:
    #     print(e.abbreviation)
    if team:
        team = team[0]
        return team.id
    else:
        print("couldnt find team")
        print(team_abbreviation)
        return -1

def prepare_nba_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, minutes_played, fgm, fga, tpfgm, tpfga, ftm, fta, offensive_rebounds, defensive_rebounds, total_rebounds, assists, steals, blocks, turnovers, personal_fouls, points, game_score):
    create_player_historical_statistics_row(id_counter, "nba_game_id", game_id, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+1, "nba_game_number", game_number_of_season, "integer", player, game_date)
    # is implemented as string right now in models, but should be foreign key
    create_player_historical_statistics_row(id_counter+2, "nba_team", team, "foreign_key", player, game_date)
    create_player_historical_statistics_row(id_counter+3, "nba_home_or_away", home_or_away, "string", player, game_date)
    # implemented as string, should this also be a foreign key or just keep it as string
    create_player_historical_statistics_row(id_counter+4, "nba_opponent", opponent_team, "string", player, game_date)
    # idk how to get if started
    create_player_historical_statistics_row(id_counter+5, "nba_started", started, "integer", player, game_date)
    # should this be string
    create_player_historical_statistics_row(id_counter+6, "nba_minutes_played", minutes_played, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+7, "nba_field_goals_made", fgm, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+8, "nba_field_goals_attempted", fga, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+9, "nba_three_point_field_goals_made", tpfgm, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+10, "nba_three_point_field_goals_attempted", tpfga, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+11, "nba_free_throws_made", ftm, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+12, "nba_free_throws_attempted", fta, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+13, "nba_offensive_rebounds", offensive_rebounds, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+14, "nba_defensive_rebounds", defensive_rebounds, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+15, "nba_total_rebounds", total_rebounds, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+16, "nba_assists", assists, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+17, "nba_steals", steals, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+18, "nba_blocks", blocks, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+19, "nba_turnovers", turnovers, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+20, "nba_personal_fouls", personal_fouls, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+21, "nba_points", points, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+22, "nba_game_score", game_score, "float", player, game_date)

def create_player_historical_statistics_row(id, stat_name, stat_value, stat_type, player, game_date):
    print("stat_id: " + str(id))
    print("stat_name: " + stat_name)
    print("stat_value: " + str(stat_value))
    print("stat_type: " + stat_type)
    print("player_id: " + str(player.id))
    print("player: " + player.name)
    print("game_date: " + game_date.strftime("%m/%d/%Y, %H:%M:%S"))
    player_historical_stats_set = player.playerhistoricalstatistics_set
    if player_historical_stats_set:
        player_historical_stats_set.filter(stat_datetime_of_game=game_date, stat_name=stat_name).delete()#all().delete() #
    player_statistics_row = PlayerHistoricalStatistics()
    # idk how to setup this field, so for now its just autoincrement
    # player_statistics_row.stat_id = id
    player_statistics_row.stat_name = stat_name
    player_statistics_row.stat_value = stat_value
    player_statistics_row.stat_type = stat_type
    # this is wrong
    player_statistics_row.stat_datetime_of_game = game_date #datetime.now()
    player_statistics_row.stat_created_at = datetime.now()
    player_statistics_row.player = player
    player_statistics_row.save()
    print("real stat id: " + str(player_statistics_row.stat_id))

def get_nba_player_historical_statistics(basketball_players_url_array):
    not_found_players = []
    player_counter = 1
    id_counter = 1
    player_array = []
    # players = Player.objects.filter(sport="NBA")
    for url in basketball_players_url_array:
        # possibly a better option to just get this years stats directly from here (or maybe as a test to see if im accurate)
        # https://www.basketball-reference.com/leagues/NBA_2020_per_game.html
        # TODO right now it's just the current year, but for the historical table, it must be every year

        # gets rid of the .html
        full_url = url[:-5] + '/gamelog/2020'
        print(full_url)
        request = http.get(full_url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        meta_data = souped_result.find("div", {"id":"meta"})
        header = meta_data.find("h1", {"itemprop":"name"})
        lookup_name = header.get_text()[:-17]
        player = Player.objects.filter(sport="nba", name=lookup_name)
        # testp = Player.objects.filter(sport="NBA").order_by("name")[5:20] #, name__trigram_similar=lookup_name

        print(lookup_name.encode("utf-8"))
        # for x in testp:
        #     print(x.name)
        if player:
            # print("found")
            player = player[0]
        else:
            not_found_players.append(lookup_name)
            print("couldnt find player " + str(lookup_name.encode("utf-8")))
            continue

        main_table = souped_result.find('table', {"id":"pgl_basic"})

        main_body = main_table.find('tbody')
        # print(main_body)
        all_data_rows = main_body.findAll('tr', {'class':''})
        game_counter = 1
        player_counter += 1
        # print(team)

        for row in all_data_rows:
            
            current_player_array = []
            current_tds = row.findAll('td')
            # print(current_tds)
            # print(main_table)
            current_game_string = '2020'
            if game_counter < 10:
                current_game_string += '0'
            current_game_string += str(game_counter)
            if player_counter < 10:
                current_game_string += '0'
            current_game_string += str(player_counter)
            game_id = int(current_game_string)
            potentially_inactive = row.find('td', {'data-stat':'reason'}) # either 'Inactive' or 'Did Not Play'
            if potentially_inactive:
                add_inactive_game_to_list(row, current_game_string, player, id_counter)
                id_counter+=23
                continue
            game_number_of_season = int(row.find('th', {'data-stat':'ranker'}).text.strip())
            game_date = row.find('td', {'data-stat':'date_game'}).text.strip()
            game_date = make_aware(datetime.strptime(game_date, "%Y-%m-%d"))
            # print(game_date)
            team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'team_id'}).text.strip().lower(), "nba") # this should be a foreign key

            home_or_away = row.find('td', {'data-stat':'game_location'}).text.strip()
            if home_or_away == "":
                home_or_away = 1
            else:
                home_or_away = 0

            opponent_team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'opp_id'}).text.strip().lower(), "nba")

            result = row.find('td', {'data-stat':'game_result'}).text.strip()
            started = get_value_or_default(row.find('td', {'data-stat':'gs'}), 0) # either 1 or 0
            minutes_played_raw = row.find('td', {'data-stat':'mp'}).text.strip()
            # if minutes_played_raw:
            minutes_played_array = minutes_played_raw.split(":")
            seconds_played_float = int(minutes_played_array[1]) / 60
            minutes_played = int(minutes_played_array[0]) + seconds_played_float
            field_goals_made = get_value_or_default(row.find('td', {'data-stat':'fg'}), 0)
            field_goals_attempted = get_value_or_default(row.find('td', {'data-stat':'fga'}), 0)
            # field_goals_percentage = (row.find('td', {'data-stat':'fg_pct'}).text.strip())
            three_point_field_goals_made = get_value_or_default(row.find('td', {'data-stat':'fg3'}), 0)
            three_point_field_goals_attempted = get_value_or_default(row.find('td', {'data-stat':'fg3a'}), 0)
            # three_point_field_goals_percentage = (row.find('td', {'data-stat':'fg3_pct'}).text.strip())
            free_throws_made = get_value_or_default(row.find('td', {'data-stat':'ft'}), 0)
            free_throws_attempted = get_value_or_default(row.find('td', {'data-stat':'fta'}), 0)
            # free_throws_percentage = (row.find('td', {'data-stat':'ft_pct'}).text.strip())
            offensive_rebounds = get_value_or_default(row.find('td', {'data-stat':'orb'}), 0)
            defensive_rebounds = get_value_or_default(row.find('td', {'data-stat':'drb'}), 0)
            total_rebounds = get_value_or_default(row.find('td', {'data-stat':'trb'}), 0)
            assists = get_value_or_default(row.find('td', {'data-stat':'ast'}), 0)
            steals = get_value_or_default(row.find('td', {'data-stat':'stl'}), 0)
            blocks = get_value_or_default(row.find('td', {'data-stat':'blk'}), 0)
            turnovers = get_value_or_default(row.find('td', {'data-stat':'tov'}), 0)
            personal_fouls = get_value_or_default(row.find('td', {'data-stat':'pf'}), 0)
            points = get_value_or_default(row.find('td', {'data-stat':'pts'}), 0)
            # make sure theres no .80000000001 things going on, see kent bazemore, from before the round function
            # game_score = round(float(row.find('td', {'data-stat':'game_score'}).text.strip()), 2)
            game_score = get_value_or_default(row.find('td', {'data-stat':'game_score'}), 0.0)

            prepare_nba_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, 0, minutes_played, field_goals_made, field_goals_attempted, three_point_field_goals_made, three_point_field_goals_attempted, free_throws_made, free_throws_attempted, offensive_rebounds, defensive_rebounds, total_rebounds, assists, steals, blocks, turnovers, personal_fouls, points, game_score)
            id_counter+=23

    print("players that weren't found:")
    for x in not_found_players:
        print(x.encode("utf-8"))

def prepare_nfl_qb_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, passes_completed, passes_attempted, passing_yards, passing_touchdowns, interceptions, qb_rating, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, targets, receptions, receiving_yards, receiving_touchdowns, game_score):
    create_player_historical_statistics_row(id_counter, "nfl_game_id", game_id, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+1, "nfl_game_number", game_number_of_season, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+2, "nfl_team", team, "foreign_key", player, game_date)
    create_player_historical_statistics_row(id_counter+3, "nfl_home_or_away", home_or_away, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+4, "nfl_opponent", opponent_team, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+5, "nfl_started", started, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+6, "nfl_passes_completed", passes_completed, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+7, "nfl_passes_attempted", passes_attempted, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+8, "nfl_passing_yards", passing_yards, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+9, "nfl_passing_touchdowns", passing_touchdowns, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+10, "nfl_interceptions", interceptions, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+11, "nfl_qb_rating", qb_rating, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+12, "nfl_rushes_attempted", rushes_attempted, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+13, "nfl_rushing_yards", rushing_yards, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+14, "nfl_rushing_touchdowns", rushing_touchdowns, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+15, "nfl_fumbles", fumbles, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+16, "nfl_targets", targets, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+17, "nfl_receptions", receptions, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+18, "nfl_receiving_yards", receiving_yards, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+19, "nfl_receiving_touchdowns", receiving_touchdowns, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+20, "nfl_game_score", game_score, "float", player, game_date)

def prepare_nfl_defensive_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, total_tackles, sacks, forced_fumbles, defensive_interceptions, game_score):
    create_player_historical_statistics_row(id_counter, "nfl_game_id", game_id, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+1, "nfl_game_number", game_number_of_season, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+2, "nfl_team", team, "foreign_key", player, game_date)
    create_player_historical_statistics_row(id_counter+3, "nfl_home_or_away", home_or_away, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+4, "nfl_opponent", opponent_team, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+5, "nfl_started", started, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+6, "nfl_total_tackles", total_tackles, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+7, "nfl_sacks", sacks, "float", player, game_date)
    create_player_historical_statistics_row(id_counter+8, "nfl_forced_fumbles", forced_fumbles, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+9, "nfl_defensive_interceptions", defensive_interceptions, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+10, "nfl_game_score", game_score, "float", player, game_date)


def get_nfl_player_historical_statistics(football_players_url_array):
    not_found_players = []
    player_counter = 1
    id_counter = 1
    player_array = []
    position_array = []
    offensive_skill_positions = ["qb", "qb throws", "rb", "fb", "wr", "wr throws", "te"] # obvs throws is a mistake
    offensive_line_positions = ["lt", "rt", "ot", "t", "lg", "rg", "og", "g", "ol", "c"]
    defensive_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl", "lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb", "cb", "ss", "fs", "s"]
    defensive_line_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl"]
    defensive_linebacker_positions = ["lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb"]
    defensive_back_positions = ["cb", "ss", "fs", "s"]
    other_positions = ["p", "k"]
    # players = Player.objects.filter(sport="NBA")

    for url in football_players_url_array:

        # https://www.basketball-reference.com/leagues/NBA_2020_per_game.html
        #TODO right now it's just the current year, but for the historical table, it must be every year

        # gets rid of the .htm instead of .html
        full_url = url[:-4] + '/gamelog/2019' #2020 season didn't happen yet
        # if full_url != "https://www.pro-football-reference.com/players/B/BoarCh00/gamelog/2019":
        #     continue
        print(full_url)
        request = http.get(full_url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        # print(souped_result.encode("utf-8"))
        meta_data = souped_result.find("div", {"id":"meta"})
        # print("meta_data")
        # print(meta_data)
        header = souped_result.find("h1", {"itemprop":"name"})
        # print("header")
        # print(header)
        lookup_name = header.get_text().lower()#[:-17]
        player = Player.objects.filter(sport="nfl", name=lookup_name)
        # testp = Player.objects.filter(sport="nfl").order_by("name")[5:20] #, name__trigram_similar=lookup_name
        # print(loo)
        print(lookup_name.encode("utf-8"))
        # for x in testp:
        #     print(x.name)
        if player:
            # print("found")
            player = player[0]
            if player.position not in position_array:
                position_array.append(player.position)
        else:
            not_found_players.append(lookup_name)
            print("couldnt find player " + str(lookup_name.encode("utf-8")))
            continue
        main_table = souped_result.find('table', {"id":"stats"})
        main_body = main_table.find('tbody')
        all_data_rows = main_body.findAll('tr', {'class':''})
        game_counter = 1
        player_counter += 1
        for row in all_data_rows:
            current_player_array = []
            current_tds = row.findAll('td')
            current_game_string = '2020'
            if game_counter < 10:
                current_game_string += '0'
            current_game_string += str(game_counter)
            if player_counter < 10:
                current_game_string += '0'
            current_game_string += str(player_counter)
            game_id = int(current_game_string)
            potentially_inactive = row.find('td', {'data-stat':'reason'}) # either 'Inactive' or 'Did Not Play'
            if potentially_inactive:
                print("inactive in football")
                add_inactive_game_to_list(row, current_game_string, player, id_counter)
                id_counter+=23
                continue
            game_number_of_season = int(row.find('th', {'data-stat':'ranker'}).text.strip())
            game_date = row.find('td', {'data-stat':'game_date'}).text.strip() #as opposed to date_game
            game_date = make_aware(datetime.strptime(game_date, "%Y-%m-%d"))
            team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'team'}).text.strip().lower(), "nfl") # this should be a foreign key
            home_or_away = row.find('td', {'data-stat':'game_location'}).text.strip()
            if home_or_away == "":
                home_or_away = 1
            else:
                home_or_away = 0
            opponent_team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'opp'}).text.strip().lower(), "nfl")
            result = row.find('td', {'data-stat':'game_result'}).text.strip()
            started = row.find('td', {'data-stat':'gs'}).text.strip() # either 1 or 0
            if started == "":
                started = 0
            else: # should be equaling a "*" string
                started = 1
            # after this, the stats depend on the position

            # if "throws" in player.position.lower():

            if player.position.lower() in offensive_skill_positions:
                passes_completed = get_value_or_default(row.find('td', {'data-stat':'pass_cmp'}), 0)
                passes_attempted = get_value_or_default(row.find('td', {'data-stat':'pass_att'}), 0)
                passing_yards = get_value_or_default(row.find('td', {'data-stat':'pass_yds'}), 0)
                passing_touchdowns = get_value_or_default(row.find('td', {'data-stat':'pass_td'}), 0)
                interceptions = get_value_or_default(row.find('td', {'data-stat':'pass_int'}), 0)
                qb_rating = get_value_or_default(row.find('td', {'data-stat':'pass_rating'}), 0.0)

                rushes_attempted = get_value_or_default(row.find('td', {'data-stat':'rush_att'}), 0)
                rushing_yards = get_value_or_default(row.find('td', {'data-stat':'rush_yds'}), 0)
                rushing_touchdowns = get_value_or_default(row.find('td', {'data-stat':'rush_td'}), 0)
                fumbles = get_value_or_default(row.find('td', {'data-stat':'fumbles'}), 0)

                targets = get_value_or_default(row.find('td', {'data-stat':'targets'}), 0)
                receptions = get_value_or_default(row.find('td', {'data-stat':'rec'}), 0)
                receiving_yards = get_value_or_default(row.find('td', {'data-stat':'rec_yds'}), 0)
                receiving_touchdowns = get_value_or_default(row.find('td', {'data-stat':'rec_td'}), 0)

                #     # kicking stats, don't know if should be included
                #     field_goals_attempted = models.FloatField(default=0.0)
                #     field_goals_made = models.FloatField(default=0.0)
                #     # punting stats, don't know if should be included
                #     punts = models.FloatField(default=0.0)
                #     punt_yards = models.FloatField(default=0.0)

                game_score = 0.0
                prepare_nfl_qb_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, passes_completed, passes_attempted, passing_yards, passing_touchdowns, interceptions, qb_rating, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, targets, receptions, receiving_yards, receiving_touchdowns, game_score)
                id_counter += 21
            # elif player.position.lower() == "rb":
            #     rushes_attempted = int(row.find('td', {'data-stat':'rush_att'}).text.strip())
            #     rushing_yards = int(row.find('td', {'data-stat':'rush_yds'}).text.strip())
            #     rushing_touchdowns = int(row.find('td', {'data-stat':'rush_td'}).text.strip())
            #     fumbles = int(row.find('td', {'data-stat':'fumbles'}).text.strip())
            #     game_score = 0.0
            #     prepare_nfl_rb_player(id_counter, game_id, game_date, game_number_of_season, team, home_or_away, opponent_team, started, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, game_score)
            #     id_counter += 11
            # elif player.position.lower() == "wr" or player.position.lower() == "te":
            #     targets = int(row.find('td', {'data-stat':'targets'}).text.strip())
            #     receptions = int(row.find('td', {'data-stat':'rec'}).text.strip())
            #     receiving_yards = int(row.find('td', {'data-stat':'rec_yds'}).text.strip())
            #     receiving_touchdowns = int(row.find('td', {'data-stat':'rec_td'}).text.strip())
            #     fumbles = int(row.find('td', {'data-stat':'fumbles'}).text.strip())
            #     game_score = 0.0
            #     prepare_nfl_qb_player(id_counter, game_id, game_date, game_number_of_season, team, home_or_away, opponent_team, started, rushes_attempted, rushing_yards, rushing_touchdowns, fumbles, game_score)
            #     id_counter += 11
            elif player.position.lower() in offensive_line_positions:
                continue
            elif player.position.lower() in other_positions:
                continue
            elif player.position.lower() in defensive_positions:
                defensive_interceptions = get_value_or_default(row.find('td', {'data-stat':'def_int'}), 0)
                forced_fumbles = get_value_or_default(row.find('td', {'data-stat':'fumbles_forced'}), 0)
                sacks = get_value_or_default(row.find('td', {'data-stat':'sacks'}), 0.0)
                total_tackles = get_value_or_default(row.find('td', {'data-stat':'tackles_combined'}), 0)
                # total_tackles = int(row.find('td', {'data-stat':'tackles_solo'}).text.strip())
                # total_tackles = int(row.find('td', {'data-stat':'tackles_assists'}).text.strip())
                # total_tackles = int(row.find('td', {'data-stat':'tackles_loss'}).text.strip())
                game_score = 0.0
                prepare_nfl_defensive_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, total_tackles, sacks, forced_fumbles, defensive_interceptions, game_score)
                id_counter += 11

            else:
                print("not in a category: " + player.position)
                continue
    print("players that weren't found:")
    for x in not_found_players:
        print(x.encode("utf-8"))
  
def prepare_mlb_pitcher_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, innings_pitched, batters_faced, hits_allowed, earned_runs, strikeouts, decision, game_score):
    create_player_historical_statistics_row(id_counter, "mlb_game_id", game_id, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+1, "mlb_game_number", game_number_of_season, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+2, "mlb_team", team, "foreign_key", player, game_date)
    create_player_historical_statistics_row(id_counter+3, "mlb_home_or_away", home_or_away, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+4, "mlb_opponent", opponent_team, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+5, "mlb_started", started, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+6, "mlb_innings_pitched", innings_pitched, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+7, "mlb_batters_faced", batters_faced, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+8, "mlb_hits_allowed", hits_allowed, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+9, "mlb_earned_runs", earned_runs, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+10, "mlb_strikeouts", strikeouts, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+11, "mlb_decision", decision, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+12, "mlb_game_score", game_score, "float", player, game_date)

def prepare_mlb_hitter_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, at_bats, runs, hits, rbi, homeruns, on_base_percentage, slugging_percentage, game_score):
    create_player_historical_statistics_row(id_counter, "mlb_game_id", game_id, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+1, "mlb_game_number", game_number_of_season, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+2, "mlb_team", team, "foreign_key", player, game_date)
    create_player_historical_statistics_row(id_counter+3, "mlb_home_or_away", home_or_away, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+4, "mlb_opponent", opponent_team, "string", player, game_date)
    create_player_historical_statistics_row(id_counter+5, "mlb_started", started, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+6, "mlb_at_bats", at_bats, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+7, "mlb_runs", runs, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+8, "mlb_hits", hits, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+9, "mlb_rbi", rbi, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+10, "mlb_homeruns", homeruns, "integer", player, game_date)
    create_player_historical_statistics_row(id_counter+11, "mlb_on_base_percentage", on_base_percentage, "float", player, game_date)
    create_player_historical_statistics_row(id_counter+12, "mlb_slugging_percentage", slugging_percentage, "float", player, game_date)
    create_player_historical_statistics_row(id_counter+13, "mlb_game_score", game_score, "float", player, game_date)

def get_mlb_player_historical_statistics(baseball_players_url_array):
    not_found_players = []
    player_counter = 1
    id_counter = 1
    player_array = []
    position_array = []

    found_will = False
    first_will = False
    for player_dict in baseball_players_url_array:
        lookup_name = player_dict["name"].lower()#[:-17]
        player = Player.objects.filter(sport="mlb", name=lookup_name)

        
        # testp = Player.objects.filter(sport="mlb").order_by("name")[5:20] #, name__trigram_similar=lookup_name
        # print(loo)

        print(lookup_name.encode("utf-8"))
        # for x in testp:
        #     print(x.name)
        if player:
            # print("found")
            player = player[0]
            print(player.id)
            if player.position not in position_array:
                position_array.append(player.position)
        else:
            not_found_players.append(lookup_name)
            print("couldnt find player " + str(lookup_name.encode("utf-8")))
            continue

        if player.id > 4160 or player.id < 3650:
            continue

        if lookup_name != "will smith":
            print("will not found yet")
            # if found_will == False:
            #     continue
        else:  
            # print("this is will")
            found_will = True
            if first_will == False:
                first_will = True
                continue
            else:
                # print("will player")
                player = Player.objects.filter(sport="mlb", name=lookup_name)[1]
        # else:
            # print("made it...")
                
        # https://www.basketball-reference.com/leagues/NBA_2020_per_game.html
        #TODO right now it's just the current year, but for the historical table, it must be every year

        # print(player_dict["url"])
        url_id = player_dict["url"][:-6].split("/")[3]
        # print(url_id)
        base_url = "https://www.baseball-reference.com/players/gl.fcgi?id=" + url_id + "&t="
        suffix_url = "&year=2019"
        if player.position.lower() == "pitcher" or player.position.lower() == "p":
            if player.name.lower() == "shohei ohtani":
                full_url = base_url + "h" + suffix_url
            else:
                full_url = base_url + "p" + suffix_url
        else:
            full_url = base_url + "h" + suffix_url
        # gets rid of the .htm instead of .html
        # full_url = player_dict["url"][:-4] + '/gamelog/2019' #2020 season didn't happen yet
        # if full_url != "https://www.pro-football-reference.com/players/B/BoarCh00/gamelog/2019":
        #     continue
        print(full_url)
        request = http.get(full_url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        # print(souped_result.encode("utf-8"))
        meta_data = souped_result.find("div", {"id":"meta"})
        # print("meta_data")
        # print(meta_data)
        header = souped_result.find("h1", {"itemprop":"name"})
        # print("header")
        # print(header)

        if player.position.lower() == "pitcher" or player.position.lower() == "p":
            if player.name.lower() == "shohei ohtani":
                main_table = souped_result.find('table', {"id":"batting_gamelogs"})
            else:
                main_table = souped_result.find('table', {"id":"pitching_gamelogs"})
        else:
            main_table = souped_result.find('table', {"id":"batting_gamelogs"})

        main_body = main_table.find('tbody')
        all_data_rows = main_body.findAll('tr', {'class':''}) # empty class to ignore the middle header rows
        game_counter = 1
        player_counter += 1
        for row in all_data_rows:
            current_player_array = []
            current_tds = row.findAll('td')
            current_game_string = '2020'
            if game_counter < 10:
                current_game_string += '0'
            current_game_string += str(game_counter)
            if player_counter < 10:
                current_game_string += '0'
            current_game_string += str(player_counter)
            game_id = int(current_game_string)
            potentially_inactive = row.find('td', {'data-stat':'reason'}) # either 'Inactive' or 'Did Not Play'
            if potentially_inactive:
                print("inactive in baseball")
                add_inactive_game_to_list(row, current_game_string, player, id_counter)
                id_counter+=23
                continue
            game_number_of_season = int(row.find('th', {'data-stat':'ranker'}).text.strip())
            game_date = row.find('td', {'data-stat':'date_game'}).text.strip()
            if "(1)" in game_date or "(2)" in game_date:
                game_date = game_date[:-3].strip()
            game_date = game_date + " 2019"#as opposed to date_game

            # print(game_date)
            game_date = datetime.strptime(game_date, "%b %d %Y")
            team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'team_ID'}).text.strip().lower(), "mlb") # this should be a foreign key
            home_or_away = row.find('td', {'data-stat':'team_homeORaway'}).text.strip()
            if home_or_away == "":
                home_or_away = 1
            else:
                home_or_away = 0
            opponent_team = team_abbreviation_to_object_id(row.find('td', {'data-stat':'opp_ID'}).text.strip().lower(), "mlb")
            result = row.find('td', {'data-stat':'game_result'}).text.strip()
            # how to implement this
            started = 1
            # row.find('td', {'data-stat':'gs'}).text.strip() # either 1 or 0
            # if started == "":
            #     started = 0
            # else: # should be equaling a "*" string
            #     started = 1
            # after this, the stats depend on the position

            # if "throws" in player.position.lower():

            if player.position.lower() == "p" or player.position.lower() == "pitcher":
                innings_pitched = get_value_or_default(row.find('td', {'data-stat':'IP'}), 0.0)
                batters_faced = get_value_or_default(row.find('td', {'data-stat':'R'}), 0)
                hits_allowed = get_value_or_default(row.find('td', {'data-stat':'H'}), 0)
                earned_runs = get_value_or_default(row.find('td', {'data-stat':'ER'}), 0)
                strikeouts = get_value_or_default(row.find('td', {'data-stat':'SO'}), 0)
                decision = get_value_or_default(row.find('td', {'data-stat':'player_game_result'}), "")
                if decision == "":
                    decision = 0
                else:
                    if decision[:1] == "W":
                        decision = 3
                    elif decision[:1] == "L":
                        decision = -3
                    elif decision[:1] == "H":
                        decision = 2
                    elif decision[:1] == "S":
                        decision = -4
                    else:
                        decision = 0

                game_score = 0.0
                prepare_mlb_pitcher_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, innings_pitched, batters_faced, hits_allowed, earned_runs, strikeouts, decision, game_score)
                id_counter += 13
            else:
                at_bats = get_value_or_default(row.find('td', {'data-stat':'AB'}), 0)
                runs = get_value_or_default(row.find('td', {'data-stat':'R'}), 0)
                hits = get_value_or_default(row.find('td', {'data-stat':'H'}), 0)
                rbi = get_value_or_default(row.find('td', {'data-stat':'RBI'}), 0)
                homeruns = get_value_or_default(row.find('td', {'data-stat':'HR'}), 0)
                on_base_percentage = get_value_or_default(row.find('td', {'data-stat':'onbase_perc'}), 0.0)
                slugging_percentage = get_value_or_default(row.find('td', {'data-stat':'slugging_perc'}), 0.0)

                # do i want to get another url just for errors or leave it how it is

                game_score = 0.0
                prepare_mlb_hitter_player(id_counter, game_id, player, game_date, game_number_of_season, team, home_or_away, opponent_team, started, at_bats, runs, hits, rbi, homeruns, on_base_percentage, slugging_percentage, game_score)
                id_counter += 14

    print("players that weren't found:")
    for x in not_found_players:
        print(x.encode("utf-8"))
    print("all positions:")
    for y in position_array:
        print(y)

# basketball_players_url_array = get_player_names_and_sports_reference_urls.get_basketball_players()
# get_nba_player_historical_statistics(basketball_players_url_array)

football_players_url_array = get_player_names_and_sports_reference_urls.get_football_players()
get_nfl_player_historical_statistics(football_players_url_array)

# baseball_players_url_array = get_player_names_and_sports_reference_urls.get_baseball_players()
# get_mlb_player_historical_statistics(baseball_players_url_array)

# teams = PlayerHistoricalStatistics.objects.all()
# PlayerHistoricalStatistics.objects.filter(stat_name__contains="mlb").delete()

# for t in teams:
    # print(t.id)
    # t.sport = t.sport.lower()
    # t.abbreviation = t.abbreviation.lower()
    # t.save()

