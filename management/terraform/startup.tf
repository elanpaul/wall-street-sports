provider "aws" {
  profile = "default"
  region  = "us-east-1"
  version = "~> 2.60"
}

resource "aws_instance" "sports_market_instance" {
  ami           = "ami-2757f631"
  instance_type = "t2.micro"
}

