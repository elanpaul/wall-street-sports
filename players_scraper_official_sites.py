import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import csv
from datetime import date
import unidecode
import cssutils
import psycopg2


player_id = ''
t = ''
base_link = 'https://www.basketball-reference.com/teams/WAS/2020.html'
base_individual_player_link = base_link + player_id + t
nba_teams = ['ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DAL', 'DEN' 'DET', 'GSW', 'HOU', 'IND', 'LAC', 'LAL',
'MEM', 'MIA', 'MIL', 'MIN', 'NOP', 'NYK', 'OKC', 'ORL', 'PHI', 'PHX', 'POR', 'SAC', 'SAS', 'TOR', 'UTA', 'WAS']

nba_team_names = ['Atlanta', 'Brooklyn', 'Boston', 'Charlotte', 'Chicago', 'Cleveland', 'Dallas', 'Denver', 'Detroit',
'Golden State', 'Houston', 'Indiana', 'LA Clippers', 'LA Lakers', 'Memphis', 'Miami', 'Milwaukee', 'Minnesota',
'New Orleans', 'New York', 'Oklahoma City', 'Orlando', 'Philadelphia', 'Phoenix', 'Portland', 'Sacramento',
'San Antonio', 'Toronto', 'Utah', 'Washington']

nba_teams_url_names = ['atlanta-hawks', 'boston-celtics', 'brooklyn-nets', 'charlotte-hornets', 'chicago-bulls',
'cleveland-cavaliers', 'dallas-mavericks', 'denver-nuggets', 'detroit-pistons', 'golden-state-warriors',
'houston-rockets', 'indiana-pacers', 'la-clippers', 'los-angeles-lakers', 'memphis-grizzlies',
'miami-heat', 'milwaukee-bucks', 'minnesota-timberwolves', 'new-orleans-pelicans', 'new-york-knicks',
'oklahoma-city-thunder', 'orlando-magic', 'philadelphia-76ers', 'phoenix-suns', 'portland-trail-blazers',
'sacramento-kings', 'san-antonio-spurs', 'toronto-raptors', 'utah-jazz', 'washington-wizards']

# team_colors_array = []

# team_colors_base = 'https://teamcolorcodes.com/atlanta-hawks-color-codes/'
# request = requests.get(team_colors_base)
# result_html = request.text
# souped_result = BeautifulSoup(result_html)
# # print(souped_result)

# team_colors_a = souped_result.findAll('a', {'class':'team-button'})
# for t in team_colors_a:
#     style_tags_array = t["style"].split(' ')
#     primary_color = style_tags_array[1][:-1]
#     if ' color' in style_tags_array:
#         secondary_color = style_tags_array[7][:-1]
#     else:
#         secondary_color = style_tags_array[5][:-1]
#     tmp_clrs = [primary_color, secondary_color]
#     team_colors_array.append(tmp_clrs)


# print(len(player_image_section))
team_colors_array = [['#E03A3E', '#C1D32F'], ['#007A33', '#BA9653'], ['#000000', '#FFFFFF'], ['#1D1160', '#00788C'], ['#CE1141', '#000000'], ['#860038', '#041E42'],
['#00538C', '#002B5E'], ['#0E2240', '#FEC524'], ['#C8102E', '#1D42BA'], ['#1D428A', '#FFC72C'], ['#CE1141', '#000000'], ['#002D62', '#FDBB30'],
['#C8102E', '#1D428A'], ['#552583', '#FDB927'], ['#5D76A9', '#12173F'], ['#98002E', '#F9A01B'], ['#00471B', '#EEE1C6'], ['#0C2340', '#236192'],
['#0C2340', '#C8102E'], ['#006BB6', '#F58426'], ['#007AC1', '#EF3B24'], ['#0077C0', '#C4CED4'], ['#006BB6', '#ED174C'], ['#1D1160', '#E56020'],
['#E03A3E', '#000000'], ['#5A2D81', '#63727A'], ['#C4CED4', '#000000'], ['#CE1141', '#000000'], ['#002B5C', '#00471B'], ['#002B5C', '#E31837']]
# i = 0
full_teams_results_array = []

list_of_names = []
with open("nba_individual_player_list.csv", encoding="utf8") as csvfile:
    reader = csv.reader(csvfile)#, quoting=csv.QUOTE_NONE) # , quoting=csv.QUOTE_NONNUMERICchange contents to floats
    for row in reader: # each row is a list
        list_of_names.append(row[0])

# for x in list_of_names:
#     x = x.decode('utf8')

# print(list_of_names)
# print(list_of_names[0])
# print(type(list_of_names[0]))




css_parser = cssutils.CSSParser()




retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)




player_characteristics_array = []
bad_name_array = ['charlie brown', 'devon hall', 'dusty hannahs', 'amile jefferson', 'daryl macon',
'sviatoslav mykhailiuk', 'shamorie ponds', 'jarrod uthoff']
counter = 0
for name in list_of_names:
    # break
    counter += 1
    if counter > 25:
        break
    current_player = []

    url = 'https://www.nba.com/players/' + unidecode.unidecode(name.split(' ')[0]) +'/' + unidecode.unidecode(name.split(' ')[1])
    # url = 'https://www.nba.com/players/bam/adebayo'
    if name in bad_name_array:
        continue
    print(url)
    # stylesheet = css_parser.parseUrl('https://www.nba.com/themes/custom/league/assets/css/style.css?q7cdrq')
    # for each_rule in stylesheet: #.CSSRules:
    #     if each_rule.type == each_rule.STYLE_RULE:
    #         if 'nba-player-header__item' in each_rule.selectorText:
    #             print(each_rule.style)
    #             break
    # break
    request = http.get(url)
    result_html = request.text
    souped_result = BeautifulSoup(result_html)
    # print(souped_result)

    player_image_section = souped_result.findAll('section', {'class':'nba-player-header__item nba-player-header__headshot'})[0]
# how to get src of image and then how to serialize it
    player_image = player_image_section.findAll('img')[0]['src']
    print(player_image)
    playing_details = souped_result.findAll('p', {'class':'nba-player-header__details-top'})[0]
    # logo you can get here

    #TODO team name, team color
    number = playing_details.findAll('span')[0].text.strip()[1:]
    print(number)
    position = playing_details.findAll('span')[2].text.strip()
    print(position)


    height_details = souped_result.findAll('section', {'class':'nba-player-vitals__top-left'})[0]
    height = height_details.findAll('p', {'class':'nba-player-vitals__top-info-imperial'})[0].text.strip()
    # height_feet = height.findAll('span')[0].text.strip()
    # height_inches = height.findAll('span')[1].text.strip()
    # print(height_feet)
    # print(height_inches)

    # spl_index = height.index('ft')
    tmp_arr = height.split('ft')
    height = 'ft '.join(tmp_arr)
    print(height)
    weight_details = souped_result.findAll('section', {'class':'nba-player-vitals__top-right'})[0]

    weight = weight_details.findAll('p', {'class':'nba-player-vitals__top-info-imperial'})[0].text.split(' ')[0]
    print(weight)

    potential_teams = souped_result.findAll('span', {'class':'nba-player-vitals__bottom-info'})[5].text.split(' ')
    print(potential_teams)
    team = ''
    primary_color = ''
    secondary_color = ''
    for t in range(len(potential_teams)-1, -1, -1):
        print(potential_teams[t])
        if(len(potential_teams[t]) == 3):
            team = potential_teams[t].lower()
            break
    print(team)
    if team != '':
        temp_team_index = nba_teams.index(team.upper())
        primary_color = team_colors_array[temp_team_index + 1][0]
        secondary_color = team_colors_array[temp_team_index + 1][1]



    current_player.append(counter)
    current_player.append(name)
    current_player.append(0.0)
    current_player.append('nba')
    current_player.append(team)
    current_player.append(primary_color)
    current_player.append(secondary_color)
    current_player.append('')
    current_player.append('')
    current_player.append(position)
    current_player.append(number)    
    current_player.append(height)
    current_player.append(weight)
    current_player.append(0.0)
    current_player.append(1)
    player_characteristics_array.append(current_player)
    # print(len(current_team_results_array))
    # full_teams_results_array.append(current_team_results_array)
print(player_characteristics_array)

conn = psycopg2.connect(host='127.0.0.1', port='5432',user='postgres',password='PostgresElanPaul3526!',database='sportsstockmarketdb') # To remove slash
# PostgresElanPaul3526
# sportsstockmarketdb
cursor = conn.cursor()

# cursor.execute("INSERT INTO market_player (player_id, name) VALUES(%s, %s)", player_characteristics_array[0])
# cursor.executemany("INSERT INTO market_player (id, name, current_price, sport, team, team_color, team_secondary_color, player_image, team_image, position, number, height, weight, delta, owner_id) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", player_characteristics_array)
# cursor.execute("INSERT INTO market_player (player_id, name, current_price, height, number, player_image, position, sport, team, team_color, team_image, weight, team_secondary_color) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", player_characteristics_array[0])
# cursor.executemany("INSERT INTO market_player VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", player_characteristics_array)
# for z in player_characteristics_array:
#     sql_update_query = """Update market_player set team_color = %s, team_secondary_color= %s where name = %s"""
#     cursor.execute(sql_update_query, (z[9], z[12], z[1]))
conn.commit() # <- We MUST commit to reflect the inserted data
cursor.close()
conn.close()

# with open("nba_individual_player_characteristics_from_nba.csv","w+", newline="") as my_csv:
#     csvWriter = csv.writer(my_csv,delimiter=',')
#     csvWriter.writerows(player_characteristics_array)

