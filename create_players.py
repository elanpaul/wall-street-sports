import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()
# your imports, e.g. Django models
# from models import Location

import requests
from market.models import User, Player, Team, Share, PlayerCurrentStatistics, PlayerHistoricalStatistics, Market
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
import decimal
from fractions import Fraction
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import csv
from datetime import date
import unidecode
import cssutils
import psycopg2

import get_player_names_and_sports_reference_urls

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)

# nfl goes from 1-2024 inclusive, nba starts at 2199, then has a few random ones, then jumps to many digit numbers


def create_baseball_players(baseball_players_array):
    bad_name_array = ["zack cozart"]
    counter = 3000

    for url in baseball_players_array:
        counter+=1
        print(url)
        request = http.get(url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        all_info_div = souped_result.find("div", {"id":"info"})
        all_meta_div = all_info_div.find("div", {"id":"meta"})
        person_div = all_meta_div.find("div", {"itemtype":"https://schema.org/Person"})
        uniform_numbers_div = all_info_div.find_all("div", {"class":"uni_holder"})
        # nesseccary?
        
        player = Player()
        player.id = counter
        name = person_div.find("h1", {"itemprop":"name"}).text.lower().encode("utf-8")
        # encoded_name = name.encode("utf-8")
        # print(name)
        if name:
            player.name = name.decode()
        else:
            print("no player name found")
        player.sport = "mlb"


        attributes = person_div.find_all("p")
        print(attributes)
        position_line = attributes[0]
        print(position_line)
        if position_line:
            position = position_line.get_text().split(":")[1].strip()
            player.position = position
        else:
            print("no player name found")

        if uniform_numbers_div:
            uniform_numbers_div = uniform_numbers_div[0]
            all_uniforms = uniform_numbers_div.find_all("a")
            current_uniform = all_uniforms[len(all_uniforms) - 1]
            number = current_uniform["href"].split("=")[-1]
            print(number)
            player.number = number
            if name.decode() in bad_name_array:
                print("bad name")
                team = " ".join(current_uniform["data-tip"].split(" ")[1:]).lower()
                print(team)
                # print(Team.objects.all())
                team_object = Team.objects.filter(full_name=team)
                if team_object:
                    player.team = team_object[0]
                else:
                    print("bad no team")
        if name.decode() == "zack cozart":
            team = "los angeles angels"
        elif name.decode() == "rajai davis":
            team = "new york mets"
        elif name.decode() == "jared hughes":
            team = "philadelphia phillies"
        elif name.decode() == "ian kinsler":
            team = "san diego padres"
        elif name.decode() == "matt ramsey":
            team = "los angeles angels"
        elif name.decode() == "hunter strickland":
            team = "washington nationals"
        elif name.decode() == "carlos torres":
            team = "detroit tigers"
        elif name.decode() == "sam tuivailala":
            team = "seattle mariners"
        elif name.decode() == "pablo sandoval":
            team = attributes[4].find("a").get_text().lower()
        elif name.decode() == "pat venditte":
            team = attributes[4].find("a").get_text().lower()
        else:
            team = attributes[3].find("a").get_text().lower()
        print(team)
        # print(Team.objects.all())
        team_object = Team.objects.filter(full_name=team)
        if team_object:
            player.team = team_object[0]
        else:
            ("good no team")
        if name.decode() == "pablo sandoval" or name.decode() == "pat venditte":
            height_weight_line = attributes[3]
        else:
            height_weight_line = attributes[2]

        if height_weight_line:
            height_and_weight = height_weight_line.get_text().split("(")[0].strip()
        height = height_and_weight.split(",")[0]
        player.height = height
        weight = height_and_weight.split(",")[1]
        player.weight = weight
        player.owner_id = 1
        player.save()


def create_football_players(football_players_url_array):
    bad_name_array = ["zack cozart"]
    counter = 0

    for url in football_players_url_array:
        # if url != "https://www.pro-football-reference.com/players/H/HowaTy00.htm":
        #     continue
        counter+=1
        print(url)
        request = http.get(url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        all_info_div = souped_result.find("div", {"id":"info"})
        all_meta_div = all_info_div.find("div", {"id":"meta"})
        person_div = all_meta_div.find("div", {"itemtype":"https://schema.org/Person"})
        uniform_numbers_div = all_info_div.find_all("div", {"class":"uni_holder"})
        # nesseccary?
        
        player = Player()
        player.id = counter
        name = person_div.find("h1", {"itemprop":"name"}).text.lower().encode("utf-8")
        # encoded_name = name.encode("utf-8")
        # print(name)
        if name:
            player.name = name.decode()
        else:
            print("no player name found")
        player.sport = "nfl"


        attributes = person_div.find_all("p")
        print(attributes)
        position_line = attributes[1]
        # print("isnt empty")
        # if position_line.get_text().strip() == "":
        #     print("is empty")
        #     position_line = attributes[2]
        # if name.decode().strip() == "michael davis":
            # print("they equal")
        print(position_line)
        if position_line:
            # print("position line")
            if name.decode().strip() == "sam beal":
                position = "cb"
            elif name.decode().strip() == "michael davis":
                position = "cb"
            elif name.decode().strip() == "virgil green":
                position = "te"
            elif name.decode().strip() == "tytus howard":
                position = "t"
            elif name.decode().strip() == "ty johnson":
                position = "wr"
            elif name.decode() == "devaroe lawrence":
                position = "dl"
            elif name.decode() == "tremon smith":
                position = "cb"
            elif name.decode() == "benny snell jr.":
                position = "rb"
            elif name.decode() == "jalen thompson":
                position = "s"
            elif name.decode() == "quincy williams":
                position = "lb"
            else:
                position = position_line.get_text().split(":")[1].strip()
            player.position = position
        else:
            print("no player name found")

        if uniform_numbers_div:
            uniform_numbers_div = uniform_numbers_div[0]
            all_uniforms = uniform_numbers_div.find_all("a")
            current_uniform = all_uniforms[len(all_uniforms) - 1]
            number = current_uniform["href"].split("=")[-1]
            print(number)
            if name.decode() == "wil lutz":
                player.number = 3
            else:
                player.number = number
            # if name.decode() in bad_name_array:
            #     print("bad name")
            team = " ".join(current_uniform["data-tip"].split(" ")[:-1]).lower()
            # team = current_uniform["href"].split("&")[0].split("=")[-1]
            print(team)
            # print(Team.objects.all())
            if team == "san diego chargers":
                team = "los angeles chargers"
            if team == "st. louis rams":
                team = "los angeles rams"
            if team == "las vegas raiders":
                team = "oakland raiders"
            team_object = Team.objects.filter(full_name=team)
            if team_object:
                player.team = team_object[0]
            else:
                print("bad no team")
        # if name.decode().strip() == "sam beal":
        #     position = "new york giants"
        # elif name.decode().strip() == "johnathan abram":
        #     team = "oakland raiders"
        # elif name.decode().strip() == "sam acho":
        #     team = "tampa bay buccaneers"
        # elif name.decode().strip() == "jerell adams":
        #     team = "houston texans"
        # elif name.decode().strip() == "matthew adams":
        #     team = "houston texans"
        # elif name.decode().strip() == "sam acho":
        #     team = "tampa bay buccaneers"
        # elif name.decode().strip() == "sam acho":
        #     team = "tampa bay buccaneers"
        # elif name.decode().strip() == "michael davis":
        #     team = "los angeles chargers"
        # elif name.decode().strip() == "virgil green":
        #     team = "los angeles chargers"
        # elif name.decode().strip() == "tytus howard":
        #     team = "houston texans"
        # # idk if i should have/need this
        # elif name.decode() == "trumaine johnson":
        #     team = "new york jets"
        # elif name.decode() == "ty johnson":
        #     team = "detroit lions"
        # elif name.decode() == "devaroe lawrence":
        #     team = "kansas city chiefs"
        # elif name.decode() == "tremon smith":
        #     team = "philadelphia eagles"
        # elif name.decode() == "benny snell jr.":
        #     team = "pittsburgh steelers"
        # elif name.decode() == "jalen thompson":
        #     team = "arizona cardinals"
        # elif name.decode() == "quincy williams":
        #     team = "jacksonville jaguars"
        # elif name.decode() == "carlos torres":
        #     team = "detroit tigers"
        # elif name.decode() == "sam tuivailala":
        #     team = "seattle mariners"
        # elif name.decode() == "pablo sandoval":
        #     team = attributes[4].find("a").get_text().lower()
        # elif name.decode() == "pat venditte":
        #     team = attributes[4].find("a").get_text().lower()
        # else:
        #     team = attributes[3].find("a").get_text().lower()
        print(team)
        # print(Team.objects.all())
        # team_object = Team.objects.filter(full_name=team)
        # if team_object:
        #     player.team = team_object[0]
        # else:
        #     ("good no team")
        if name.decode().strip() == "sam beal":
            height_weight_line = attributes[2]
        elif name.decode().strip() == "michael davis":
            height_weight_line = attributes[2]
        elif name.decode().strip() == "virgil green":
            height_weight_line = attributes[2]
        elif name.decode().strip() == "tytus howard":
            height_weight_line = attributes[1]
        elif name.decode() == "ty johnson":
            height_weight_line = attributes[1]
        elif name.decode() == "devaroe lawrence":
            height_weight_line = attributes[1]
        elif name.decode() == "tremon smith":
            height_weight_line = attributes[1]
        elif name.decode() == "benny snell jr.":
            height_weight_line = attributes[1]
        elif name.decode() == "jalen thompson":
            height_weight_line = attributes[2]
        elif name.decode() == "quincy williams":
            height_weight_line = attributes[1]
        else:
            height_weight_line = attributes[2]

        if height_weight_line:
            height_and_weight = height_weight_line.get_text().split("(")[0].strip()
        height = height_and_weight.split(",")[0]
        player.height = height
        weight = height_and_weight.split(",")[1]
        player.weight = weight
        player.owner_id = 1
        player.save()

def create_basketball_players(basketball_players_url_array):
    bad_name_array = ["zack cozart"]
    counter = 0

    for url in basketball_players_url_array:
        # if url != "https://www.pro-football-reference.com/players/H/HowaTy00.htm":
        #     continue
        counter+=1
        print(url)
        request = http.get(url)
        result_html = request.text
        souped_result = BeautifulSoup(result_html)
        all_info_div = souped_result.find("div", {"id":"info"})
        all_meta_div = all_info_div.find("div", {"id":"meta"})
        person_div = all_meta_div.find("div", {"itemtype":"https://schema.org/Person"})
        uniform_numbers_div = all_info_div.find_all("div", {"class":"uni_holder"})
        # nesseccary?
        
        player = Player()
        player.id = counter
        name = person_div.find("h1", {"itemprop":"name"}).text.lower().encode("utf-8")
        # encoded_name = name.encode("utf-8")
        # print(name)
        if name:
            player.name = name.decode()
        else:
            print("no player name found")
        player.sport = "nfl"


        attributes = person_div.find_all("p")
        print(attributes)
        position_line = attributes[1]
        # print("isnt empty")
        # if position_line.get_text().strip() == "":
        #     print("is empty")
        #     position_line = attributes[2]
        # if name.decode().strip() == "michael davis":
            # print("they equal")
        print(position_line)
        if position_line:
            # print("position line")
            if name.decode().strip() == "sam beal":
                position = "cb"
            elif name.decode().strip() == "michael davis":
                position = "cb"
            elif name.decode().strip() == "virgil green":
                position = "te"
            elif name.decode().strip() == "tytus howard":
                position = "t"
            elif name.decode().strip() == "ty johnson":
                position = "wr"
            elif name.decode() == "devaroe lawrence":
                position = "dl"
            elif name.decode() == "tremon smith":
                position = "cb"
            elif name.decode() == "benny snell jr.":
                position = "rb"
            elif name.decode() == "jalen thompson":
                position = "s"
            elif name.decode() == "quincy williams":
                position = "lb"
            else:
                position = position_line.get_text().split(":")[1].strip()
            player.position = position
        else:
            print("no player name found")

        if uniform_numbers_div:
            uniform_numbers_div = uniform_numbers_div[0]
            all_uniforms = uniform_numbers_div.find_all("a")
            current_uniform = all_uniforms[len(all_uniforms) - 1]
            number = current_uniform["href"].split("=")[-1]
            print(number)
            if name.decode() == "wil lutz":
                player.number = 3
            else:
                player.number = number
            # if name.decode() in bad_name_array:
            #     print("bad name")
            team = " ".join(current_uniform["data-tip"].split(" ")[:-1]).lower()
            # team = current_uniform["href"].split("&")[0].split("=")[-1]
            print(team)
            # print(Team.objects.all())
            if team == "san diego chargers":
                team = "los angeles chargers"
            if team == "st. louis rams":
                team = "los angeles rams"
            if team == "las vegas raiders":
                team = "oakland raiders"
            team_object = Team.objects.filter(full_name=team)
            if team_object:
                player.team = team_object[0]
            else:
                print("bad no team")
        # if name.decode().strip() == "sam beal":
        #     position = "new york giants"
        # elif name.decode().strip() == "johnathan abram":
        #     team = "oakland raiders"
        # elif name.decode().strip() == "sam acho":
        #     team = "tampa bay buccaneers"
        # elif name.decode().strip() == "jerell adams":
        #     team = "houston texans"
        # elif name.decode().strip() == "matthew adams":
        #     team = "houston texans"
        # elif name.decode().strip() == "sam acho":
        #     team = "tampa bay buccaneers"
        # elif name.decode().strip() == "sam acho":
        #     team = "tampa bay buccaneers"
        # elif name.decode().strip() == "michael davis":
        #     team = "los angeles chargers"
        # elif name.decode().strip() == "virgil green":
        #     team = "los angeles chargers"
        # elif name.decode().strip() == "tytus howard":
        #     team = "houston texans"
        # # idk if i should have/need this
        # elif name.decode() == "trumaine johnson":
        #     team = "new york jets"
        # elif name.decode() == "ty johnson":
        #     team = "detroit lions"
        # elif name.decode() == "devaroe lawrence":
        #     team = "kansas city chiefs"
        # elif name.decode() == "tremon smith":
        #     team = "philadelphia eagles"
        # elif name.decode() == "benny snell jr.":
        #     team = "pittsburgh steelers"
        # elif name.decode() == "jalen thompson":
        #     team = "arizona cardinals"
        # elif name.decode() == "quincy williams":
        #     team = "jacksonville jaguars"
        # elif name.decode() == "carlos torres":
        #     team = "detroit tigers"
        # elif name.decode() == "sam tuivailala":
        #     team = "seattle mariners"
        # elif name.decode() == "pablo sandoval":
        #     team = attributes[4].find("a").get_text().lower()
        # elif name.decode() == "pat venditte":
        #     team = attributes[4].find("a").get_text().lower()
        # else:
        #     team = attributes[3].find("a").get_text().lower()
        print(team)
        # print(Team.objects.all())
        # team_object = Team.objects.filter(full_name=team)
        # if team_object:
        #     player.team = team_object[0]
        # else:
        #     ("good no team")
        if name.decode().strip() == "sam beal":
            height_weight_line = attributes[2]
        elif name.decode().strip() == "michael davis":
            height_weight_line = attributes[2]
        elif name.decode().strip() == "virgil green":
            height_weight_line = attributes[2]
        elif name.decode().strip() == "tytus howard":
            height_weight_line = attributes[1]
        elif name.decode() == "ty johnson":
            height_weight_line = attributes[1]
        elif name.decode() == "devaroe lawrence":
            height_weight_line = attributes[1]
        elif name.decode() == "tremon smith":
            height_weight_line = attributes[1]
        elif name.decode() == "benny snell jr.":
            height_weight_line = attributes[1]
        elif name.decode() == "jalen thompson":
            height_weight_line = attributes[2]
        elif name.decode() == "quincy williams":
            height_weight_line = attributes[1]
        else:
            height_weight_line = attributes[2]

        if height_weight_line:
            height_and_weight = height_weight_line.get_text().split("(")[0].strip()
        height = height_and_weight.split(",")[0]
        player.height = height
        weight = height_and_weight.split(",")[1]
        player.weight = weight
        player.owner_id = 1


def format_player_positions():
    psts = []
    pls = Player.objects.all()
    # +1577
    # for p in pls:
    #     p.position = p.position.lower().strip()
    #     p.name = p.name.lower().strip()
    #     p.save()
    # return
    offensive_skill_positions = ["qb", "qb throws", "rb", "fb", "wr", "wr throws", "te"] # obvs throws is a mistake
    offensive_line_positions = ["lt", "rt", "ot", "t", "lg", "rg", "og", "g", "ol", "c"]
    defensive_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl", "lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb", "cb", "ss", "fs", "s"]
    defensive_line_positions = ["lde", "rde", "de", "ldt", "rdt", "dt", "edge", "dt/lb", "dt", "dl"]
    defensive_linebacker_positions = ["lolb", "rolb", "olb", "lilb", "rilb", "ilb", "rlb", "llb", "lb", "mlb"]
    defensive_back_positions = ["cb", "ss", "fs", "s"]
    other_positions = ["p", "k"]

    infield_position = ["2b", "ss", "3b"]


    for i in pls:
        pst = i.position.lower()
        
        if i.position.lower() not in psts:
            psts.append(i.position.lower())
        if i.sport.lower() == "nfl":
            if "throws" in i.position.lower():
                i.position = i.position.lower()[:-6].strip()
                # print("thing")
                # print(len(i.position.lower().split(" ")))
                i.save()
            if i.position.lower() in offensive_line_positions:
                i.position = "ol"
            if i.position.lower() in defensive_line_positions:
                i.position = "dl"
            if i.position.lower() in defensive_linebacker_positions:
                i.position = "lb"
            if i.position.lower() in defensive_back_positions:
                i.position = "db"
        if i.sport.lower() == "mlb":
            without_useless = ' '.join(i.position.lower().replace("pinch hitter", "").replace("pinch runner", "").replace("designated hitter", "").replace(",", "").replace("and", "").strip().split())
            abbreviated = without_useless.replace("pitcher", "p").replace("catcher", "c").replace("first baseman", "1b").replace("second baseman", "2b").replace("third baseman", "3b").replace("shortstop", "ss").replace("outfielder", "of").replace("rightfielder", "of").replace("leftfielder", "of").replace("centerfielder", "of")
            sorted_positions_array = sorted(abbreviated.split())
            sorted_positions = ' '.join(sorted_positions_array)
            i.position = sorted_positions
            if len(i.position.lower().split(" ")) == 1:
                continue
            if len(i.position.lower().split(" ")) >= 2:
                if "2b" in i.position.lower() or "ss" in i.position.lower():
                    i.position = "if"
                elif "of" in i.position.lower():
                    i.position = "u"
                else:
                    i.position = sorted_positions.replace(" ", "/")

            i.save()
            # if i.position.lower() == "pitcher":
            #     i.position = "p"
            # if i.position.lower() == "first baseman" or i.position.lower() == "first baseman and pinch hitter":
            #     i.position = "1b"
            # if i.position.lower() == "outfielder" or i.position.lower() == "outfielder and pinch hitter" or i.position.lower() == "designated hitter and leftfielder" or i.position.lower() == "outfielder and pinch hitter" or i.position.lower() == "centerfielder" or i.position.lower() == "designated hitter and leftfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder" or i.position.lower() == "rightfielder":
            #     i.position = "of"
            # if i.position.lower() == "shortstop and second baseman" or i.position.lower() == "pinch hitter, shortstop and second baseman" or i.position.lower() == "third baseman, second baseman and shortstop" or i.position.lower() == "shortstop, second baseman and third baseman" or i.position.lower() == "second baseman, third baseman and shortstop":
            #     i.position = "if"
            # if i.position.lower() == "first baseman, pinch hitter and leftfielder" or i.position.lower() == "outfielder and first baseman":
            #     i.position = "1b/of"
            # if i.position.lower() == "shortstop":
            #     i.position = "ss"
            # if i.position.lower() == "second baseman":
            #     i.position = "2b"
            # if i.position.lower() == "third baseman":
            #     i.position = "3b"
            # if i.position.lower() == "catcher, pinch hitter and second baseman" or i.position.lower() == "pinch hitter and catcher" or i.position.lower() == "catcher":
            #     i.position = "c"
            # if i.position.lower() == "outfielder and pinch hitter":
            #     i.position = "of"
            # if i.position.lower() == "catcher, pinch hitter and first baseman" or i.position.lower() == "catcher and first baseman" or i.position.lower() == "catcher, third baseman and first baseman":
            #     i.position = "c/1b"
            # if i.position.lower() == "centerfielder":
            #     i.position = "of"
            # if i.position.lower() == "second baseman, leftfielder and third baseman":
            #     i.position = "u"
            # if i.position.lower() == "pitcher":
            #     i.position = "p"
        # if i.sport.lower == "nba":



    for ps in psts:
        print(ps)

# format_player_positions()
# baseball_players_array = get_player_names_and_sports_reference_urls.get_baseball_players()
# create_baseball_players(baseball_players_array)
# football_players_url_array =get_player_names_and_sports_reference_urls.get_football_players()
# create_football_players(football_players_url_array)

# PlayerHistoricalStatistics.objects.all().delete()
# PlayerCurrentStatistics.objects.all().delete()
# playersPlayer.objects.all()

