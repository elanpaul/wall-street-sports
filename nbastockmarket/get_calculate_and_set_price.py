import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()
# your imports, e.g. Django models
# from models import Location

import requests
from market.models import User, Player, Share, Market
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
import decimal
from fractions import Fraction


import csv
from datetime import date
import psycopg2

decimal.getcontext().prec = 1000



# Process
# Step 1: get all raw scores from players(RAW)
# Step 2: get total raw scores(TRAW)
# Step 3: get scaled raw score by doing (RAW/TRAW)*100 (PRAW)

# Step 4: get all number of shares owned of single player (SHARE)
# Step 5: get total number of shares owned of that single player (TSHARE)
# Step 6: get percent of market single player has by doing (SHARE/TSHARE)*100 (PSHARE)

# Step 7: get individual calculated score value by doing PRAW*PSHARE (VALUE)
# Step 8: weight the scores to increase/decrease volatility 



def getCalculateAndSetPrice():

    player_calculation_dict = {"individuals":{}, "totals":{}}

    # conn = psycopg2.connect(host='127.0.0.1', port='5432',user='postgres',password='PostgresElanPaul3526!',database='sportsstockmarketdb')
    # cursor = conn.cursor()

    # not sure if theres a better place to keep this, but this is where the raw is stored for now in market_playerhistoricalstatistics
    # see here https://stackoverflow.com/questions/121387/fetch-the-row-which-has-the-max-value-for-a-column
    # and here https://stackoverflow.com/questions/1379565/how-to-fetch-the-first-and-last-record-of-a-grouped-record-in-a-mysql-query-with for solutions

    # solution a: select game_id, game_score, game_number, game_date from(select game_id, game_score, game_number, game_date, max(game_date) over (partition by player_id_id) max_my_date from market_playerhistoricalstatistics) as mf
    # solution b: SELECT m1.*
    # FROM market_playerhistoricalstatistics m1 LEFT JOIN market_playerhistoricalstatistics m2
    # ON (m1.player_id_id = m2.player_id_id AND m1.game_number < m2.game_number)
    # WHERE m2.game_number IS NULL;
    # cursor.execute("SELECT m1.* FROM market_playerhistoricalstatistics m1 LEFT JOIN market_playerhistoricalstatistics m2 ON (m1.player_id_id = m2.player_id_id AND m1.game_number < m2.game_number) WHERE m2.game_number IS NULL;")
    # raw_scores = cursor.fetchall()

    # cursor.execute("SELECT player_id, current_price FROM market_player;")
    # raw_scores = cursor.fetchall()
    # dont need section
    # players = Player.objects.all()
    # total_raw_scores = decimal.Decimal(0.0)
    # for player in players:
    #     total_raw_scores += decimal.Decimal(player.current_performance_price) #rw[23]
    #     player_calculation_dict["totals"].update({"TRAW":total_raw_scores})

    #     tmp_dict = {"RAW":decimal.Decimal(player.current_performance_price)} #rw[24], rw[23], "game_id": rw[0],
    #     if str(player.id) in player_calculation_dict["individuals"]:
    #         player_calculation_dict["individuals"][str(player.id)].update(tmp_dict)
    #     else:
    #         player_calculation_dict["individuals"][str(player.id)] = tmp_dict

    # end of dont need section

    # start of dont need section
    # cursor.execute("SELECT player_id, number_of_shares FROM market_share")
    # shares = cursor.fetchall()
    # shares = Share.objects.all()
    # total_shares = decimal.Decimal(0)
    # player_calculation_dict["totals"].update({"TSHARE":total_shares})
    # owned_shares_array = {}
    # for share in shares:
    #     total_shares += decimal.Decimal(share.number_of_shares)
    #     player_calculation_dict["totals"].update({"TSHARE":total_shares})
    #     if str(share.player.id) in owned_shares_array:
    #         owned_shares_array[str(share.player.id)] += decimal.Decimal(share.number_of_shares)
    #     else:
    #         owned_shares_array.update({str(share.player.id):decimal.Decimal(share.number_of_shares)})


    #     # print(share)
    #     # print(player_id)
    #     if "SHARE" in player_calculation_dict["individuals"][str(share.player.id)]:
    #         player_calculation_dict["individuals"][str(share.player.id)]["SHARE"] += decimal.Decimal(share.number_of_shares)
    #     else:
    #         player_calculation_dict["individuals"][str(share.player.id)].update({"SHARE": decimal.Decimal(share.number_of_shares)})

    # end of dont need section

    # this is to simulate better what the total raw scores are gonna be(15 player play, 15 games/night, 15 score per player: 15*15*15)
    # player_calculation_dict["totals"].update({"TRAW":3375})




    # have to do the following calculations AFTER everything is already retrieve and know otherwise, you can't calculate percentages during the loop, must be after the total is found
    # individual_data = player_calculation_dict["individuals"]
    # total_data = player_calculation_dict["totals"]
    players = Player.objects.all()
    market = Market.objects.get(id=1)
    for player in players:
        old_price = player.current_price
        # current_performance_price = (decimal.Decimal(player.current_performance_price) / decimal.Decimal(market.total_raw_prices)) * decimal.Decimal(market.total_raw_prices)
        
        current_owned_percentage_price = (decimal.Decimal(player.current_player_total_shares) / decimal.Decimal(market.total_number_of_shares)) * decimal.Decimal(100)
        current_price = decimal.Decimal(player.current_performance_price) + decimal.Decimal(current_owned_percentage_price)
        # player.current_performance_price = current_performance_price
        player.current_owned_percentage_price = current_owned_percentage_price
        player.current_price = current_price
        player.save()
        if old_price < player.current_price:
            print("hello")
            print("old price of " + str(player.name) + " was " + str(old_price))
            print("performance price: " + str(player.current_performance_price))
            print(str(player.current_player_total_shares))
            print(str(market.total_number_of_shares))
            print("percentage price: " + str(player.current_owned_percentage_price))
            print("new price of " + str(player.name) + " was " + str(player.current_price))


        # if "RAW" in individual_data[player]:
        #     percentage_raw = decimal.Decimal(individual_data[player]["RAW"]) / decimal.Decimal(total_data["TRAW"])
        #     individual_data[player]["PRAW"] = decimal.Decimal(percentage_raw) * decimal.Decimal(100)
        # else:
        #     individual_data[player]["PRAW"] = decimal.Decimal(1.0)
        # if "SHARE" in individual_data[player]:
        #     share_percentage = decimal.Decimal(individual_data[player]["SHARE"]) / decimal.Decimal(total_data["TSHARE"])
        #     individual_data[player]["PSHARE"] = decimal.Decimal(share_percentage) * decimal.Decimal(100)
        # else:
        #     individual_data[player]["PSHARE"] = decimal.Decimal(0.0)
        # if "VALUE" in individual_data[player]:
        # better if it's plus, because then it "guarantees" that it's still a closed system
        # individual_data[player].update({"VALUE": (decimal.Decimal(individual_data[player]["PRAW"]) + decimal.Decimal(individual_data[player]["PSHARE"])) * decimal.Decimal(100)})
        # api_player = Player.objects.get(id=int(player))
        # display = False
        # if decimal.Decimal(200.0) < decimal.Decimal(individual_data[player]["VALUE"]):
        #     print("hello")
        #     print("PRAW: " + str(individual_data[player]["PRAW"]))
        #     print("TRAW: " + str(total_data["TRAW"]))
        #     print("PSHARE: " + str(individual_data[player]["PSHARE"]))
        #     print("TSHARE: " + str(total_data["TSHARE"]))
        #     print("old price of " + str(api_player.name) + " was " + str(api_player.current_price))
        #     print("VALUE was " + str(individual_data[player]["VALUE"]))
        #     display = True
        # api_player.current_owned_percentage_price = decimal.Decimal(individual_data[player]["PSHARE"])
        # api_player.current_performance_price = decimal.Decimal(individual_data[player]["PRAW"])
        # api_player.current_price = decimal.Decimal(individual_data[player]["VALUE"])
        # api_player.save()
        # if display == True:
        #     print("new price of " + str(api_player.name) + " is " + str(api_player.current_price))





    # print(owned_shares_array)
    # print(player_calculation_dict)
    # for player in individual_data:
    #     print("RAW: " + str(player["RAW"]) + ", PRAW: " + str(player["PRAW"]) + ", PSHARE: " + str(player["PSHARE"]) + ", VALUE: " + str(player["VALUE"]))
    # conn.commit() # <- We MUST commit to reflect the inserted data
    # cursor.close()
    # conn.close()
    # print(player_calculation_dict)

    return player_calculation_dict

# update player prices
    # player_list = Player.objects.all()
    # for p in range(len(player_list)):
    #     player = player_list[p]
    #     # if player_calculation_dict["individuals"][p]["VALUE"] == 0.0:
    #     #     random_integer = random.randint(0, 5000)
    #     #     random_float = random_integer / 100
    #     #     player.current_price = random_float
    #     #     player.save()
    #     # else:
    #     player.current_price = player_calculation_dict["individuals"][str(player.player_id)]["VALUE"]
    #     player.save()

# getCalculateAndSetPrice()