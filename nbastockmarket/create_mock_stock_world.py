import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()
# your imports, e.g. Django models
# from models import Location

import requests
from market.models import User, Player, Share, PlayerHistoricalStatistics, Market
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
import decimal
from fractions import Fraction

import get_calculate_and_set_price

decimal.getcontext().prec = 1000
# x = ((1.1) - (1.0))
# print(x)


# for p in player_list:
#     d = (p.current_price)
#     if d.as_tuple().exponent < -2:
#         print(p.id)
#         print(p.current_price)
    # if p.current_price < 0:
    #     p.current_price = 1.0
    # print("f")
    # print(str(p.current_price))
    # p.current_price = round(p.current_price, 2)
    # p.save()
    # print(str(p.current_price))

share_list = Share.objects.all()

company_bank_account = 0

# requests.post("http://127.0.0.1:8000/market/api/users/")



def createUsers(number_of_users_to_create):
    user_list = User.objects.all()
    for u in range(number_of_users_to_create):
        test_name = 'ApiTestUser' + str(random.randint(0, 1000000))
        user = User(username=test_name)
        user.save()
    
    print("created " + str(number_of_users_to_create) + " users")

def deleteAllUsers():
    user_list = User.objects.all()
    for user in user_list:
        if user.id != 1:
            user.delete()
    ("deleted all users besides elanpaul")

def setPlayerPrices():
    player_list = Player.objects.all()

    total = decimal.Decimal(0.0)
    for player in player_list:
        # print(str(player.id))
        # PlayerHistoricalStatistics.game_score must be a decimal.Decimal otherwise it comes out that
        # 5.0 is 5.000...4462394869...00000 as opposed to a smooth decimal 5.000...000
        most_current_game_log = PlayerHistoricalStatistics.objects.filter(player=player.id).order_by('game_date')
        if most_current_game_log:
            most_current_game_log = most_current_game_log.reverse()[0] # [0:2]
            if decimal.Decimal(most_current_game_log.game_score) <= decimal.Decimal(1):
                number = decimal.Decimal(1.0)
            else:
                number = decimal.Decimal(most_current_game_log.game_score)
            player.current_price = number
            player.current_performance_price = number
            player.current_player_total_shares = decimal.Decimal(0.0)
            player.current_owned_percentage_price = decimal.Decimal(0.0)
            player.save()
        else:
            player.current_price = decimal.Decimal(1.0)
            player.current_performance_price = decimal.Decimal(1.0)
            player.current_player_total_shares = decimal.Decimal(0.0)
            player.current_owned_percentage_price = decimal.Decimal(0.0)
            player.save()
            # continue
        # if player.current_price > decimal.Decimal(0.0) and player.current_price < decimal.Decimal(1.0):
        #     print("got zero point zero")
        #     print(player.id)
        #     print(player.name)
        #     print(player.current_price)
        # total += player.current_price
        # print(most_current_game_log)
        # number = random.randint(0, 50) #round(random.uniform(1, 100), 2)

        # print(str(total))
    print("set all of the player prices")

def setMarket():
    market = Market.objects.get(id=1)
    market.total_raw_prices = decimal.Decimal(0.0)
    market.total_number_of_shares = decimal.Decimal(0.0)
    market.save()
    players = Player.objects.all()
    for player in players:
        # print(str(player.current_performance_price))
        market.total_raw_prices += decimal.Decimal(player.current_performance_price)
        market.save()

def buyShares(number_of_share_objects_to_buy):
    player_list = Player.objects.all()
    market = Market.objects.get(id=1)
    global company_bank_account
    global user_list
    # number_of_share_objects_to_create = 10
    for so in range(number_of_share_objects_to_buy):
        player_to_buy_id = random.randint(0, len(player_list) - 1)
        # print(player_to_buy_id)
        # player_to_buy = Player.objects.get(pk=player_to_buy_id)
        user_to_buy_id = random.randint(0, len(user_list) - 1)
        # print(user_to_buy_id)
        # user_to_buy = User.objects.get(pk=user_to_buy_id)
        # try:
        #     print("trying for " + str(player_list[player_to_buy_id].name))
        #     user = user_list[user_to_buy_id]
        #     share = player_list[player_to_buy_id].share_set.get(user=user)
        #     # what to do about the bought price, does it update now or stay as the old one
        #     # share.bought_price = 
        #     shares_to_buy = random.randint(1, 400)
        #     share.number_of_shares += shares_to_buy
        #     share.save()
        #     company_bank_account += ((player_list[player_to_buy_id].current_price) * shares_to_buy)

        # except:
        print("excepting")
        bought_player = Player.objects.get(id=player_list[player_to_buy_id].id)
        shares_to_buy = random.randint(1, 400)

        
        
        bought_player.current_player_total_shares += decimal.Decimal(shares_to_buy)
        # market.total_raw_prices += decimal.Decimal(decimal.Decimal(updated_player.current_price))
        # market.total_share_prices += decimal.Decimal(updated_player * shares_to_buy))
        bought_player.save()
        market.total_number_of_shares += decimal.Decimal(shares_to_buy)
        market.save()

        # don't need this anymore, can find the price with just this player, don't need to loop through all the players
        # get_calculate_and_set_price.getCalculateAndSetPrice()

        # very important, must refresh player otherwise it uses the old version before all the adjustment
        bought_player = Player.objects.get(id=player_list[player_to_buy_id].id)

        old_price = bought_player.current_price
        current_owned_percentage_price = (decimal.Decimal(bought_player.current_player_total_shares) / decimal.Decimal(market.total_number_of_shares)) * decimal.Decimal(100)
        current_price = decimal.Decimal(bought_player.current_performance_price) + decimal.Decimal(current_owned_percentage_price)
        # player.current_performance_price = current_performance_price
        bought_player.current_owned_percentage_price = current_owned_percentage_price
        bought_player.current_price = current_price
        bought_player.save()
        if old_price < bought_player.current_price:
            print("old price of " + str(bought_player.name) + " was " + str(old_price))
            print("performance price: " + str(bought_player.current_performance_price))
            print(str(bought_player.current_player_total_shares))
            print(str(market.total_number_of_shares))
            print("percentage price: " + str(bought_player.current_owned_percentage_price))
            print("new price of " + str(bought_player.name) + " is " + str(bought_player.current_price))

        # very important, must refresh player otherwise it uses the old version before all the adjustment
        bought_player = Player.objects.get(id=player_list[player_to_buy_id].id)

        share = Share()
        share.player = bought_player
        share.user = user_list[user_to_buy_id]
        share.number_of_shares = shares_to_buy
        share.bought_price = decimal.Decimal(bought_player.current_price)
        # in real life, first check if the purchase can be done, aka if he has enough money, etc.
        share.save()

        # market.total_price += decimal.Decimal(Player.objects.get(id=player_list[player_to_buy_id].id).current_price) * shares_to_buy))
        # company_bank_account += ((Player.objects.get(id=player_list[player_to_buy_id].id).current_price) * shares_to_buy)

        print(str(share.player.current_price))
        print(str(share.player.id))
        print("in mock stock world")
        # put the calculator here for a realer experience, but then we get screwed over by the chinese
        company_bank_account += ((bought_player.current_price) * shares_to_buy)

        print("bought " + str(shares_to_buy) + " shares of " + str(share.player.name) + " at " + str(share.player.current_price) + " and now the money went to: " + str(company_bank_account) + " dollars")

    print("bought " + str(number_of_share_objects_to_buy) + " share objects")

def buyAGazillionShares():
    global company_bank_account
    global user_list
    # number_of_share_objects_to_create = 10
    # for so in range(number_of_share_objects_to_buy):
    player_to_buy_id = 0 # james harden, random.randint(0, len(player_list) - 1)
    # print(player_to_buy_id)
    # player_to_buy = Player.objects.get(pk=player_to_buy_id)
    user_to_buy_id = 0 #random.randint(0, len(user_list) - 1)
    # print(user_to_buy_id)
    # user_to_buy = User.objects.get(pk=user_to_buy_id)
    try:
        user = user_list[user_to_buy_id]
        share = player_list[player_to_buy_id].share_set.get(user=user)
        # what to do about the bought price, does it update now or stay as the old one
        share.number_of_shares += 10000000
        company_bank_account += (player_list[player_to_buy_id].current_price * share.number_of_shares)

        share.save()
    except:
        share = Share()
        share.player = player_list[player_to_buy_id]
        share.user = user_list[user_to_buy_id]
        share.number_of_shares = 10000000
        # share.bought_price = player_list[player_to_buy_id].current_price
        # in real life, first check if the purchase can be done, aka if he has enough money, etc.
        company_bank_account += (player_list[player_to_buy_id].current_price * share.number_of_shares)

        share.save()
    print(str(share.player.current_price))
    print(str(share.player.player_id))
    print("bought a gazillion shares")

def sellShares(number_of_share_objects_to_sell):
    global company_bank_account
    global user_list
    number_of_shares_to_sell = random.randint(1, 400)
    for so in range(number_of_share_objects_to_sell):
        player_to_sell = random.randint(0, len(player_list) - 1)
        # print(player_to_sell_id)
        # player_to_buy = Player.objects.get(pk=player_to_buy_id)
        user_to_sell_id = random.randint(0, len(user_list) - 1)
        # print(user_to_sell_id)
        user = user_list[user_to_sell_id]
        share = user.share_set.get(player=player_list[player_to_sell])
        if share:
            if share.number_of_shares > number_of_shares_to_sell:
                share.number_of_shares -= number_of_shares_to_sell
                company_bank_account -= ((player_list[player_to_sell].current_price) * share.number_of_shares)

                share.save()
            else:
                company_bank_account -= ((player_list[player_to_sell].current_price) * share.number_of_shares)
                share.delete()
        else:
            print("no share found in sellShares")
    print("sold " + str(number_of_share_objects_to_sell) + " share objects")


def deleteAllShares():
    share_list = Share.objects.all()

    print("this function is just to have a clean restart, use the sellAllShares function for realistic action")
    global company_bank_account
    for share in share_list: # if share.user.id != 1:
        # share.player.current_player_total_shares = decimal.Decimal(0.0)
        # share.player.current_owned_percentage_price = decimal.Decimal(0.0)
        share.delete()
    print("deleted all the share objects")

def sellAllShares():
    share_list = Share.objects.all()
    market = Market.objects.get(id=1)
    global company_bank_account
    for share in share_list: # if share.user.id != 1:
        bought_player = Player.objects.get(id=share.player.id)
        bought_player.current_player_total_shares -= decimal.Decimal(share.number_of_shares)
        bought_player.save()
        market.total_number_of_shares -= decimal.Decimal(share.number_of_shares)
        market.save()

        bought_player = Player.objects.get(id=share.player.id)
        
        old_price = bought_player.current_price
        if market.total_number_of_shares > 0:
            current_owned_percentage_price = (decimal.Decimal(bought_player.current_player_total_shares) / decimal.Decimal(market.total_number_of_shares)) * decimal.Decimal(100)
        else:
            current_owned_percentage_price = decimal.Decimal(0)
        current_price = decimal.Decimal(bought_player.current_performance_price) + decimal.Decimal(current_owned_percentage_price)
        bought_player.current_owned_percentage_price = current_owned_percentage_price
        bought_player.current_price = current_price
        bought_player.save()
        # if old_price < bought_player.current_price:
        # print("sold " + str(bought_player.name))
        print("old price of " + str(bought_player.name) + " was " + str(old_price))
        print("performance price: " + str(bought_player.current_performance_price))
        print(str(bought_player.current_player_total_shares))
        print(str(market.total_number_of_shares))
        print("percentage price: " + str(bought_player.current_owned_percentage_price))
        print("new price of " + str(bought_player.name) + " is " + str(bought_player.current_price))

        # very important, must refresh player otherwise it uses the old version before all the adjustment
        bought_player = Player.objects.get(id=share.player.id)
        company_bank_account -= ((bought_player.current_price * share.number_of_shares))
        print(str(bought_player.current_price))
        print(str(bought_player.id))
        print("sold " + str(share.number_of_shares) + " shares at " + str(bought_player.current_price) + " and now the money went to: " + str(company_bank_account) + " dollars")

        share.delete()
        # get_calculate_and_set_price.getCalculateAndSetPrice()

    print("sold all the share objects")


def displayAllPlayerPrices():
    player_list = Player.objects.all()

    total = decimal.Decimal(0.0)
    for player in player_list:
        total += player.current_price
        # print("Player " + str(player.player_id) + " Price: " + str(player.current_price))
    print("\n")
    print("\n")
    print("the current total is: " + str(total))

def displayAllShares():
    for share in share_list:
        print("Player " + str(share.player.player_id) + " Shares: " + str(share.number_of_shares))
    print("\n")
    print("\n")


def isMarketSystemClosed():
    target_total_price = 200.0
    test_total_price = 0.0
    for player in player_list:
        # print(player.current_price)
        test_total_price += float(player.current_price)
    if test_total_price == target_total_price:
        print("Success!")
    else:
        print("Fail! " + str(test_total_price) + " didn't equal " + str(target_total_price))

# def canWeLoseMoney():
    # for player in player_list:

# pre-work
deleteAllUsers()
createUsers(2)
user_list = User.objects.all()
deleteAllShares()
# setPlayerPrices()
# setMarket()
company_bank_account = 0
# m = Market.objects.get(id=1)
# print(str(m.total_raw_prices))
# calculated_dict = get_calculate_and_set_price.getCalculateAndSetPrice()
# print(calculated_dict)
# displayAllPlayerPrices()


print("finished setup now onto the buying")

# buyShares(20)
# peak_money = company_bank_account
print("money in bank account: " + str(company_bank_account))
print("\n\n\n\n\n")
# sellAllShares()
print("money in bank account: " + str(company_bank_account))

# calculated_dict = get_calculate_and_set_price.getCalculateAndSetPrice()
# displayAllPlayerPrices()
# players_calculated = calculated_dict["individuals"]
# counter = decimal.Decimal(0.0)
# for k in players_calculated:
#     player_c = players_calculated[k]
#     counter += decimal.Decimal(player_c["VALUE"])
#     print(str(player_c))
#     print("Player: " + str(k) + " Value: " + str(player_c["VALUE"]))

# print(str(round(counter, 2)))
# print(calculated_dict["totals"])

# calculated_dict = get_calculate_and_set_price.getCalculateAndSetPrice()
# displayAllPlayerPrices()
# players_calculated = calculated_dict["individuals"]
# counter = 0.0
# for k in players_calculated:
#     player_c = players_calculated[k]
#     counter += player_c["VALUE"]
#     print("Player: " + str(k) + " Value: " + str(player_c["VALUE"]))
# isMarketSystemClosed()
