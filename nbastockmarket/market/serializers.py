from django.contrib.auth.models import User, Group
from rest_framework import serializers
from market.models import Player, VSUser, PlayerHistoricalStatistics, PlayerCurrentStatistics, Share, WatchlistDetail, UserMarketDetails, Team, Game, Market


# from https://www.django-rest-framework.org/tutorial/quickstart/
# why HyperlinkedModelSerializer? "You can also use primary key and various other relationships, but hyperlinking is good RESTful design." (quote from quickstart tutorial)
class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="user-detail",)
    # players = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='market:player-detail',)
    # shares = serializers.HyperlinkedIdentityField(Share.objects.filter(user=this),)
    # not sure what this line is doing see here https://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/
    # players = serializers.HyperlinkedRelatedField(many=True, view_name='player-detail', queryset=Player.objects.all())
    class Meta:
        model = User
        fields = ['id', 'url', 'username', 'email']#, 'players']


class GroupSerializer(serializers.ModelSerializer):
    # url = serializers.HyperlinkedIdentityField(view_name="market:group-detail",)
    # users = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='market:player-detail',)

    class Meta:
        model = Group
        fields = ['url', 'name']

class PlayerSerializer(serializers.ModelSerializer):
    # bug/messup in django with the DefaultRouter() shtick(when you don't just do everything yourself, you have to define the HyperlinkedIdentity Field, as well as the HyperlinkedRelatedField)
    # see more about this here https://stackoverflow.com/questions/27728989/django-rest-how-to-use-router-in-application-level-urls-py/27735254#27735254 and here
    # https://stackoverflow.com/questions/20550598/django-rest-framework-could-not-resolve-url-for-hyperlinked-relationship-using, and it seems like its mainly a problem when you have an app
    # name or namespace, that it screws up all the defaults
    # url = serializers.HyperlinkedIdentityField(view_name="market:player-detail",)
    id = serializers.HyperlinkedIdentityField(view_name="market:player-detail",)
    # name = serializers.HyperlinkedIdentityField(view_name="market:player-detail",)
    # team = serializers.HyperlinkedIdentityField(view_name="market:team-detail",)
    # user = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name='market:user-detail',)
    team = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name='market:team-detail',)

    # not sure why this had to be explicit
    # owner = serializers.ReadOnlyField(source='owner.username')
    # owner = serializers.ReadOnlyField(source='player.name')
    # team = serializers.HyperlinkedIdentityField(view_name='market:player-team', format='html')
    
    class Meta:
        model = Player
        fields = ['id', 'name', 'current_price', 'current_performance_price', 'current_player_total_shares', 'current_owned_percentage_price', 'sport', 'team', 'image', 'position', 'number', 'height', 'weight', 'owner']

class VSUserSerializer(serializers.ModelSerializer):
    # url = serializers.HyperlinkedIdentity(view_name="market:vsuser-detail")
    class Meta:
        model = VSUser
        fields = ['id', 'user', 'birth_date', 'address', 'city', 'state', 'zipcode', 'level', 'money_in_the_bank']

class PlayerHistoricalStatisticsSerializer(serializers.ModelSerializer):
     # player = serializers.HyperlinkedRelatedField(view_name='market:player-detail', read_only=True)
    player = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name="market:player-detail",)


    class Meta:
        model = PlayerHistoricalStatistics
        # fields = ['game_id', 'player', 'game_number', 'game_date', 'team', 'home_or_away', 'opponent', 'started', 'minutes_played', 'field_goals_made', 'field_goals_attempted',
        # 'three_point_field_goals_made', 'three_point_field_goals_attempted', 'free_throws_made', 'free_throws_attempted', 'offensive_rebounds', 'defensive_rebounds',
        # 'total_rebounds', 'assists', 'steals', 'blocks', 'turnovers', 'personal_fouls', 'total_points', 'game_score']
        fields = ['stat_id', 'player', 'stat_name', 'stat_value', 'stat_type', 'stat_datetime_of_game', 'stat_created_at']

class PlayerCurrentStatisticsSerializer(serializers.ModelSerializer):
     # player = serializers.HyperlinkedRelatedField(view_name='market:player-detail', read_only=True)
    player = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name="market:player-detail",)

    class Meta:
        model = PlayerCurrentStatistics
        # fields = ['id', 'player', 'games_played', 'minutes_played', 'field_goals_made', 'field_goals_attempted',
        # 'three_point_field_goals_made', 'three_point_field_goals_attempted', 'free_throws_made', 'free_throws_attempted', 'offensive_rebounds', 'defensive_rebounds',
        # 'total_rebounds', 'assists', 'steals', 'blocks', 'turnovers', 'player_fouls', 'total_points']
        fields = ['id', 'player', 'stat_name', 'stat_value', 'stat_type', 'stat_created_at']


# I guess it doesn't have to be serializers.HyperlinkedModelSerializer
class ShareSerializer(serializers.HyperlinkedModelSerializer):
    share_detail = serializers.HyperlinkedIdentityField(view_name="share-detail",)
    user_detail = serializers.HyperlinkedRelatedField(view_name='user-detail', read_only=True, many=False)
    player_detail = serializers.HyperlinkedRelatedField(view_name='player-detail', read_only=True, many=False)
    class Meta:
        model = Share
        fields = ['id', 'share_detail', 'user', 'user_detail', 'player', 'player_detail', 'number_of_shares', 'bought_price']

class WatchlistDetailSerializer(serializers.ModelSerializer):
    # player = serializers.HyperlinkedRelatedField(view_name='market:player-detail', read_only=True)
    class Meta:
        model = WatchlistDetail
        fields = ['user', 'player']

class UserMarketDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMarketDetails
        fields = ['user', 'money_in_account']

class TeamSerializer(serializers.ModelSerializer):
    id = serializers.HyperlinkedIdentityField(view_name="market:team-detail",)
    # full_name = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name='market:team-detail',)
    next_opponent = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name='market:team-detail',)
    class Meta:
        model = Team
        fields = ['id', 'sport', 'full_name', 'abbreviation', 'primary_color', 'secondary_color', 'defensive_rank', 'image', 'next_opponent']

class GameSerializer(serializers.ModelSerializer):
    # player = serializers.HyperlinkedRelatedField(view_name='market:player-detail', read_only=True)
    class Meta:
        model = Game
        fields = ['id', 'sport', 'home_team', 'away_team', 'game_time']

class MarketSerializer(serializers.ModelSerializer):
    # player = serializers.HyperlinkedRelatedField(view_name='market:player-detail', read_only=True)
    class Meta:
        model = Market
        fields = ['id', 'total_raw_prices', 'total_number_of_shares']