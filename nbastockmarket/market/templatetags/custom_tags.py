from django import template

from django.db.models import Q


register = template.Library()

@register.filter
def filter_player_stats(value):
    stats = value.playercurrentstatistics_set.filter(Q(stat_name="nba_total_points") | Q(stat_name="nba_total_rebounds") | Q(stat_name="nba_assists"))
    return stats

@register.filter
def delta_percentage(value, arg):
    old_price = float(arg) - value
    if old_price == 0:
        old_price = 1
    change = float(value/old_price)
    percentage = float(change*100)
    return round(percentage, 2)


@register.filter
def percentify(value):
    answer = float(value/100)
    return round(answer, 2)

@register.filter
def todollarsandcents(value):
    stringed = str(value)
    # return stringed
    # dollars = stringed.split(".")[0]
    # '{:,}'.format(dollars)

    if "." in stringed:
        dollars = stringed.split(".")[0]
        comma_added = '{:,}'.format(int(dollars))

        cents = stringed.split(".")[1]
        if len(cents) == 1:
            cents += "0"
        if len(cents) > 2:
            cents = cents[:2]
        new_string = comma_added + "." + cents
        # dont do float otherwise it redeletes the extra zero
        return (new_string)
    else:
        comma_added = '{:,}'.format(int(stringed))
        new_string = comma_added + ".00"
        # dont do float otherwise it redeletes the extra zero
        return new_string


@register.filter
def tostockname(value):
    one_word = " ".join(value)
    return one_word.upper()

@register.filter
def fitword(value, arg):
    original_name = value
    too_much = arg
    if len(value) <= too_much:
        return value
    else:
        first_name = value.split(" ")[0]
        last_name = value.split(" ")[1]
        # if len(first_name) > 7:
        #     first_name = first_name[:6] + "."
        # if len(last_name) > 7:
        #     last_name = last_name[:6] + "."
        if len(first_name) > len(last_name):
            first_name = first_name[:1] + "."
        else:
            last_name = last_name[:1] + "."
        fit_word = first_name + " " + last_name
        # if len(fit_word) <= too_much:

        #     pass

        return fit_word

@register.filter
def tovariablename(value):
    # should accept everything lowercase anyways, but just in case...
    # these are for special cases of names(primarily what this function will be used for)
    if "." in value:
        return value.lower()
    # if "-" in value:
    #     return value.lower()

    variable_version = value.replace(" ", "_")
    return variable_version.lower()

@register.filter
def keyvalue(dict, key):    
    return dict[key]

@register.filter
def subtract(value, arg):
    answer = value - arg
    return round(answer, 2)

@register.filter
def divide(value, arg):
    try:
        return float(value) / float(arg)
    except (ValueError, ZeroDivisionError):
        return None

@register.filter
def multiply(value, arg):
    return float(value) * float(arg)

@register.filter
def absolute_value(value):
    return abs(value)

@register.simple_tag
def get_total_cost_difference(cost_a, multiplier, cost_b):
    total_cost_a = cost_a * multiplier
    total_cost_b = cost_b * multiplier
    difference = total_cost_a - total_cost_b
    return difference