from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

from datetime import datetime
from django.db.models import Q

# change all pk_id fields to just be the default Django id, autoincrement field, must erase the table and recreate to do that, so you can wait until a checkpoint

class Team(models.Model):
    id = models.IntegerField(primary_key=True)
    sport = models.CharField(default='', max_length=200)
    full_name = models.CharField(max_length=200)
    abbreviation = models.CharField(max_length=200)
    primary_color = models.CharField(default='', max_length=200)
    secondary_color = models.CharField(default='', max_length=200)
    defensive_rank = models.IntegerField(default=0)
    image = models.CharField(default='', max_length=200)
    # should not be models.CASCADE
    next_opponent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)

    # is this needed
    # previous_opponent = models.CharField(default='', max_length=200)

class Player(models.Model):
    # change this to just be the default Django id, autoincrement field, must erase the table and recreate to do that, so you can wait until a checkpoint
    # player_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    current_price = models.FloatField(default=1.0)#models.DecimalField(default=0, max_digits=105, decimal_places=100)
    current_performance_price = models.DecimalField(default=0, max_digits=105, decimal_places=100)
    current_player_total_shares = models.DecimalField(default=0, max_digits=105, decimal_places=100)
    current_owned_percentage_price = models.DecimalField(default=0, max_digits=105, decimal_places=100)
    sport = models.CharField(default='', max_length=200)
    image = models.CharField(default='', max_length=200) # how to represent
    # team_image = models.CharField(default='', max_length=200) # how to represent
    position = models.CharField(default='', max_length=200)
    number = models.IntegerField(default=0)
    height = models.CharField(default='', max_length=200)
    weight = models.CharField(default='', max_length=200)
    delta = models.FloatField(default=0.0)

    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    career_start_year = models.IntegerField(default=2019)
    career_end_year = models.IntegerField(default=2019)
    is_active = models.BooleanField(default=True)

    owner = models.ForeignKey(User, related_name='players', on_delete=models.CASCADE)
    # next_opponent = models.ForeignKey(Team, max_length=200, on_delete=models.CASCADE)
    # to_add to another table or this table?
    # ppg, apg, rpg, per, etc., for season? for career?
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Player._meta.fields]
    # def __str__(self):
    #     return self.name
    def get_main_stats(self):
        return 'hello'#self.name#.all()#.filter(stat_name="nba_total_points")

class VSUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True) # idk what null=True and blank=True means
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    zipcode = models.CharField(max_length=200)
    level = models.CharField(max_length=200)
    money_in_the_bank = models.FloatField(default=0.0)
    # def __str__(self):
    #     return self.username

class PlayerHistoricalStatistics(models.Model):
    stat_id = models.AutoField(primary_key=True) #// some interesting choices here
    stat_name = models.CharField(default="", max_length=200)
    stat_value = models.FloatField(default=0.0) #, max_digits=125, decimal_places=100)
    stat_type = models.CharField(default="", max_length=200)  #//maybe
    stat_datetime_of_game = models.DateTimeField(default=datetime.now, blank=True)
    stat_created_at = models.DateTimeField(default=datetime.now, blank=True)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)


class PlayerCurrentStatistics(models.Model):
    # stat_id = models.IntegerField(primary_key=True) #// some interesting choices here
    stat_name = models.CharField(default="", max_length=200)
    stat_value = models.FloatField(default=0.0) #, max_digits=125, decimal_places=100)
    stat_type = models.CharField(default="", max_length=200)  #//maybe
    stat_created_at = models.DateTimeField(default=datetime.now, blank=True)
    player = models.ForeignKey(Player, on_delete=models.CASCADE) #, related_name='player_current_stats'

class Share(models.Model):
    # should player_id, really be OneToOne, because even though theres many per player, but theres only one within each User per player
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    number_of_shares = models.IntegerField(default=0)
    # maybe should be integer field that is x100 for performance($23.45 would just be stored as 2345)
    bought_price = models.FloatField(default=0.0)#models.DecimalField(default=0, max_digits=105, decimal_places=100) # IntegerField
    # def __str__(self):
    #     return self.player_symbol
    # def player_sport(self):
    #     return self.sport

class WatchlistDetail(models.Model):
    #see here which one you really want for on_delete, https://docs.djangoproject.com/en/3.0/ref/models/fields/#django.db.models.ForeignKey.on_delete
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)

class UserMarketDetails(models.Model):
    # user_market_id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    money_in_account = models.IntegerField(default=0)
    # def __str__(self):
    #     return 'no string value in models'

class OrderBook(models.Model):
    # order_id = models.IntegerField(primary_key=True)
    user_id = models.ForeignKey(User, default=-1, on_delete=models.CASCADE)
    datetime_stamp = models.DateTimeField('datetime ordered')
    player_symbol = models.CharField(max_length=200)
    bid_or_ask_side = models.CharField(max_length=200)
    price = models.IntegerField(default=0.0)
    quantity = models.IntegerField(default=0)

        
class Game(models.Model):
    # game_id = models.IntegerField(primary_key=True)
    sport = models.CharField(max_length=200)
    home_team = models.ForeignKey(Team, null=True, blank=True, related_name='home_team', on_delete=models.CASCADE)
    away_team = models.ForeignKey(Team, null=True, blank=True, related_name='away_team', on_delete=models.CASCADE)
    game_time = models.DateTimeField('game time', null=True, blank=True)

class Market(models.Model):
    total_raw_prices = models.DecimalField(default=0, max_digits=105, decimal_places=100)
    # total_share_prices = models.DecimalField(default=0, max_digits=105, decimal_places=100)
    total_number_of_shares = models.DecimalField(default=0, max_digits=205, decimal_places=100)
    # total_price = models.DecimalField(default=0, max_digits=105, decimal_places=100)
