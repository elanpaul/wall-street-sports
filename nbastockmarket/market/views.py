from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

from django.shortcuts import get_object_or_404, render, redirect

# Create your views here.
from django.urls import reverse
from django.template import loader, RequestContext
from django.views import generic
from .models import Player, VSUser, User, UserMarketDetails, PlayerHistoricalStatistics, PlayerCurrentStatistics, Team, Game, Share, WatchlistDetail, UserMarketDetails, Market
from datetime import datetime, date, timedelta
from django.core import serializers
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User # idk if this is redundant

from django.db.models import Func, F


# need these apparently for emailing (according to https://medium.com/@frfahim/django-registration-with-confirmation-email-bb5da011e4ef)
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage


# need for rest frameworkd api
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from market.serializers import UserSerializer, GroupSerializer # even though in tutorial it says it should be project.application.serializers
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from market.serializers import VSUserSerializer, PlayerSerializer, PlayerHistoricalStatisticsSerializer, PlayerCurrentStatisticsSerializer, ShareSerializer, WatchlistDetailSerializer, UserMarketDetailsSerializer, TeamSerializer, GameSerializer, MarketSerializer
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.response import Response
from rest_framework import permissions
from django.http import Http404
from rest_framework.views import APIView
from rest_framework import mixins
from rest_framework import generics
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework import viewsets
import json

import logging


logger = logging.getLogger(__name__)










def index(request, previousContext={}):
    logger.error("hello from index")
    template_name = 'market/index.html'
    context_object_name = 'latest_player_list'
    # user_id = request.COOKIES.get("user_id", None)
    # user_id = 1
    user = request.user
    if user.is_authenticated:
        try:
            user_market_details = UserMarketDetails.objects.get(user=user)
        except UserMarketDetails.DoesNotExist:
            user_market_details = None
        

        # get user's owned stocks
        user_shares = Share.objects.filter(user=user.id)#.prefetch_related('playercurrentstatistics_set')#'player_set', 'playerhistoricalstatistics_set')
    
        

        # get user's watchlist
        # why does this work, but the same exact thing for Share 25 lines before doesn't work, be me'ayin
        watchlist_shares = WatchlistDetail.objects.filter(user_id=user.id)#.prefetch_related('playerhistoricalstatistics_set')

        current_dt = date(2020, 3, 10)
        yesterday_dt = current_dt - timedelta(days=2)# should be days=1
        start_of_current_nba_season = datetime(2019, 8, 1)
        share_season_stats_array = []
        share_and_watchlist_last_night_array = []
        watchlist_season_stats_array = []


        for share in user_shares:
            player_historical_stats_season = PlayerHistoricalStatistics.objects.filter(player_id=share.player.id, stat_datetime_of_game__gte=start_of_current_nba_season, stat_name='game_score').order_by('stat_datetime_of_game')
            player_historical_stats_season_json = serializers.serialize('json', player_historical_stats_season, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))
            share_season_stats_array.append(player_historical_stats_season_json)
            player_historical_stats_last_night = PlayerHistoricalStatistics.objects.filter(player_id=share.player.id, stat_datetime_of_game__date=yesterday_dt, stat_name='game_score').order_by('stat_datetime_of_game')
            share_and_watchlist_last_night_array.append(player_historical_stats_last_night)

        for watchlist_stock in watchlist_shares:
            player_historical_stats_season = PlayerHistoricalStatistics.objects.filter(player_id=watchlist_stock.player.id, stat_datetime_of_game__gte=start_of_current_nba_season, stat_name='game_score').order_by('stat_datetime_of_game')
            player_historical_stats_season_json = serializers.serialize('json', player_historical_stats_season, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))
            watchlist_season_stats_array.append(player_historical_stats_season_json)
            player_historical_stats_last_night = PlayerHistoricalStatistics.objects.filter(player_id=watchlist_stock.player.id, stat_datetime_of_game__date=yesterday_dt, stat_name='game_score').order_by('stat_datetime_of_game')
            share_and_watchlist_last_night_array.append(player_historical_stats_last_night)
    else:
        user_shares = None
        user_market_details = None
        watchlist_shares = None
        share_season_stats_array = None
        share_and_watchlist_last_night_array = None
        watchlist_season_stats_array = None
    # get today's top five moving stocks
    top_movers = Player.objects.annotate(abs_balance=Func(F('delta'), function='ABS')).order_by('-abs_balance')[0:8]
    # top_five_movers_scores = [0, 0, 0, 0, 0, 0, 0, 0]
    # top_five_movers_objs = [0, 0, 0, 0, 0, 0, 0, 0]
    # found_players_dict = {}
    # yesterday_and_today_player_stats = PlayerHistoricalStatistics.objects.filter(stat_datetime_of_game__range=["2020-03-01", "2020-03-11"], stat_name='game_score').order_by('-stat_datetime_of_game')

    # for player in yesterday_and_today_player_stats:
    #     if str(player.player.name) in found_players_dict:
    #         if found_players_dict["entries"] == 2:
    #             continue
    #         found_players_dict["entries"] += 1
    #         player_game_score_difference = abs(found_players_dict[str(player.player.name)] - player.stat_value)
    #         lowest_living_score_index = top_five_movers_scores.index(min(top_five_movers_scores))
    #         if player_game_score_difference > top_five_movers_scores[lowest_living_score_index]: # >= as opposed to ">", is so that 
    #             top_five_movers_scores[lowest_living_score_index] = player_game_score_difference
    #             top_five_movers_objs[lowest_living_score_index] = player.player
    #     else:
    #         found_players_dict[str(player.player.name)] = player.stat_value
    #         found_players_dict["entries"] = 1
    # top_five_array = []
    # for p in top_five_movers_objs:
    #     # top_player = get_object_or_404(Player, player_id=p.player_id.player_id)
    #     top_player = PlayerHistoricalStatistics.objects.filter(stat_datetime_of_game__range=["2020-03-01", "2020-03-11"], player_id=1628386, stat_name='nba_game_score').order_by('stat_datetime_of_game').reverse()[0:2]
    #     print("length is: " + str(len(top_player)))
    #     top_five_array.append(top_player)

    suggested_players_array = []
    lebron = Player.objects.get(id=2544)
    harden = Player.objects.get(id=201935)
    luka = Player.objects.get(id=1629029)
    kawhi = Player.objects.get(id=202695)
    lillard = Player.objects.get(id=1629029)
    suggested_players_array.append(lebron)
    suggested_players_array.append(harden)
    suggested_players_array.append(luka)
    suggested_players_array.append(kawhi)

    context = {'user':user, 'user_shares':user_shares, 'user_market_detail':user_market_details, 'top_five_players':top_movers,
    'user_watchlist_shares':watchlist_shares, 'share_stats_array':share_season_stats_array, 'last_night_stats_array': share_and_watchlist_last_night_array,
    'watchlist_stats_array':watchlist_season_stats_array, 'suggested_players_array':suggested_players_array}
    context.update(previousContext)
    context_to_add = getHeaderVariables()
    context.update(context_to_add)
    return render(request, 'market/index.html', context)

    # else:
    #     context = {'user':user}
    #     context.update(previousContext)
    #     context_to_add = getHeaderVariables()
    #     context.update(context_to_add)
    #     return render(request, 'market/index.html', context)

def allPlayerStats(request):
    user = request.user
    sport = "nba"
    position = "g"
    if "sport" in request.GET:
        if request.GET["sport"]:
            sport = request.GET["sport"]
    if "position" in request.GET:
        if request.GET["position"]:
            position = request.GET["position"]
    # print(sport)
    # print(position)
    all_players = Player.objects.filter(sport=sport).prefetch_related('playercurrentstatistics_set').only('id', 'name','current_performance_price','team', 'delta', 'sport', 'position').select_related('team')##.exclude(current_price__lte=2)
    # all_players_values = all_players
    # player_current_stats = PlayerCurrentStatistics.objects.all()
    player_current_stats_dict = {}

    # for player in all_players:
    # #     print(row.id)
    #     player_current_stats = player.player_current_stats.all()
    #     player_current_stats_dict[player.id] = {}
    #     # player_current_stats_dict[player.id]["data"] = player
    #     for row in player_current_stats:
    #         player_current_stats_dict[player.id][row.stat_name] = row
    # player_historical_stats_season_json = serializers.serialize('json', player_historical_stats_season, fields=('game_date', 'game_score'))
    all_players_json = serializers.serialize('json', all_players, fields=('id', 'name','current_performance_price','team', 'delta', 'sport', 'position'))
    context = {'user':user, 'players': all_players, 'players_json': all_players_json, 'sport':sport, 'position':position}
    context_to_add = getHeaderVariables()
    context.update(context_to_add)
    return render(request, 'market/all_player_stats.html', context)


def detail(request, player_id):
    # user_id = request.COOKIES.get("user_id", None)
    # user_id = 1
    user = request.user
    player = Player.objects.get(pk=player_id) #203943)
    player_current_stats = player.playercurrentstatistics_set.all()
    player_current_stats_dict = {}
    # for row in player_current_stats:
    #     print(row.id)
    #     player_current_stats_dict[row.stat_name] = row

    # css = player.player_current_stats.all()
    try:
        # doesn't work for player Quincy Adams at /market/2/buy/
        selected_player_share = Share.objects.get(user=user.id, player=player_id)
        is_owned_share = True
    # selected_player = user.share_set.get(player_id=player_id)
    except (KeyError, Share.DoesNotExist):
        selected_player_share = None
        is_owned_share = False
        # return render(request, 'market/player_detail.html', {
        #     'player': player,
        # })
    # else:
    # user_player_shares = Share.objects.get(player_id=player_id)
    try:
        watchlist_share = WatchlistDetail.objects.filter(user_id=user.id, player_id=player_id)
        if watchlist_share:
            is_watchlist_share = True
        else:
            is_watchlist_share = False
    except User.DoesNotExist:
        watchlist_share = None

    # gets historical statistics in different ranges for graph data
    # current_dt = datetime.datetime.now() # sshould be this when its the nba season at least, possibly always, but for testing will be artificial
    current_dt = datetime(2020, 3, 10)
    start_of_current_nba_season = datetime(2019, 8, 1)
    month_ago_dt = current_dt - timedelta(days=30)
    week_ago_dt = current_dt - timedelta(days=7)

    player_historical_stats_career = PlayerHistoricalStatistics.objects.filter(player_id=player_id, stat_name='game_score').order_by('stat_datetime_of_game')
    player_historical_stats_career_json = serializers.serialize('json', player_historical_stats_career, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))

    player_historical_stats_season = PlayerHistoricalStatistics.objects.filter(player_id=player_id, stat_datetime_of_game__gte=start_of_current_nba_season, stat_name='game_score').order_by('stat_datetime_of_game')
    player_historical_stats_season_json = serializers.serialize('json', player_historical_stats_season, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))

    player_historical_stats_month = PlayerHistoricalStatistics.objects.filter(player_id=player_id, stat_datetime_of_game__gte=month_ago_dt, stat_name='game_score').order_by('stat_datetime_of_game')
    player_historical_stats_month_json = serializers.serialize('json', player_historical_stats_month, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))

    player_historical_stats_week = PlayerHistoricalStatistics.objects.filter(player_id=player_id, stat_datetime_of_game__gte=week_ago_dt, stat_name='game_score').order_by('stat_datetime_of_game')
    player_historical_stats_week_json = serializers.serialize('json', player_historical_stats_week, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))

    player_historical_stats_day = PlayerHistoricalStatistics.objects.filter(player_id=player_id, stat_name='game_score').order_by('stat_datetime_of_game')
    player_historical_stats_day_json = serializers.serialize('json', player_historical_stats_day, fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))

    # this could just be gotten by player.price, idk why im doing this
    # current_price = player_historical_stats_season.reverse()[0].game_score


    teammates = Player.objects.filter(team=player.team).exclude(id=player.id).order_by('-current_price')[0:4]
    relevant_players = Player.objects.filter(position=player.position).exclude(id=player.id).order_by('-current_price')[0:4]


    context = {'user':user, 'player':player, 'selected_player_share':selected_player_share, 'is_watchlist_share':is_watchlist_share,
    'player_historical_stats_career_json':player_historical_stats_career_json, 'player_historical_stats_season_json':player_historical_stats_season_json,
    'player_historical_stats_month_json':player_historical_stats_month_json, 'player_historical_stats_week_json':player_historical_stats_week_json,
    'player_historical_stats_day_json':player_historical_stats_day_json, 'is_owned_share':is_owned_share,
    'teammates':teammates, 'relevant_players':relevant_players, 'player_current_stats_dict':player_current_stats}
    context_to_add = getHeaderVariables()
    context.update(context_to_add)
    return render(request, 'market/player_detail.html', context)
    # return HttpResponse(response % player_id)

def buy(request, player_id):
    player = get_object_or_404(Player, pk=player_id)
    user = request.user
    # user = get_object_or_404(User, pk=1)
    selected_player = Player.objects.get(pk=request.POST['player_id'])
    # TOTEST
    # must test this with no players owned or watchlisted, it blew up
    try:
        player_share = player.share_set.get(user=user) #player=request.POST['player_id']
    # what to do about the bought price, does it update now or stay as the old one
        player_share.number_of_shares += int(request.POST['number_of_shares'])
        player_share.save()
    except Share.DoesNotExist:
        selected_player_share = None
        player_share = Share()
        player_share.player = player
        player_share.user = user
        player_share.number_of_shares = int(request.POST['number_of_shares'])
        player_share.bought_price = float(request.POST['per_share_price']) # should be float, must change it in db first
        player_share.save()

    # INCORRECT this gets the object from user_id, but should be user_market_details_id
    user_market_detail = get_object_or_404(UserMarketDetails, user=user.id) # should be user.user_id, but db is messed up right now
    vs_user = get_object_or_404(VSUser, user=user)
    total_amount = int(player_share.number_of_shares) * float(player_share.bought_price)
    print(total_amount)
    vs_user.money_in_the_bank -= total_amount
    vs_user.save()
    # user_market_detail.money_in_account -= total_amount
    # user_market_detail.save()

    user_shares = Share.objects.filter(user_id=user.id)

    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.

    # makes it go to the player page, not home page, possibly switch
    # essentially goes to index function, so no need to any extra work in this function
    return HttpResponseRedirect(reverse('market:index'))


def sell(request, player_id):
    player = get_object_or_404(Player, pk=player_id)
    # user_id = request.COOKIES.get("user_id", None)
    # user_id = 1
    user = request.user
    # user = get_object_or_404(User, pk=1)
    # selected_player = Player.objects.get(pk=request.POST['player_id'])
    try:
        # doesn't work for player Quincy Adams at /market/2/buy/
        player_share = user.share_set.get(player_id=request.POST['player_id'])
        


        user_market_detail = get_object_or_404(UserMarketDetails, user=user.id) # should be user.user_id, but db is messed up right now
        # TODO change bought price, it should be the current market price
        vs_user = get_object_or_404(VSUser, user=user)
        total_amount = int(player_share.number_of_shares) * float(player_share.bought_price)
        print(total_amount)
        vs_user.money_in_the_bank += total_amount
        vs_user.save()
        # user_market_detail.money_in_account += total_amount
        # user_market_detail.save()

        if player_share.number_of_shares > int(request.POST["number_of_shares"]):
            player_share.number_of_shares -= int(request.POST["number_of_shares"])
            player_share.save()
        else:
        # make sure to delete the player share after you use it to get the selling info(price, etc.)
            player_share.delete()
    except (KeyError, Share.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'market/player_detail.html', {
            'player': player,
            'error_message': "You didn't select a choice.",
        })
    else:
        # selected_player.number_of_shares += 1
        # selected_player.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.

        # makes it go to the player page, not home page, possibly switch
        return HttpResponseRedirect(reverse('market:index')) #, args=(player.id,)


def addToWatchlist(request, player_id):
    user = request.user
    player_to_add = get_object_or_404(Player, id=player_id)
    watchlist_share = WatchlistDetail()
    watchlist_share.user = user
    watchlist_share.player = player_to_add
    watchlist_share.save()
    return HttpResponseRedirect(reverse('market:player_detail', args=(player_id,))) # could be player_id or player_to_add.player_id
    # return render_to_response(
    #     'market/index.html',
    #     {'form': 'form'}, 
    #     RequestContext(request)
    # )
    # return HttpResponseRedirect(reverse('market:index')) #, args=(player.id,)



def removeFromWatchlist(request, player_id):
    user = request.user
    share_to_delete = get_object_or_404(WatchlistDetail, user=user.id, player=player_id)
    share_to_delete.delete()
    return HttpResponseRedirect(reverse('market:player_detail', args=(player_id,))) # could be player_id or player_to_add.player_id



def search(request, search_query=None): # search_query
    # template_name = 'market/search.hstml'
    # context_object_name = 'latest_player_list'
    # request.user.id is the system user for admin and other stuff, don't use that use you're own user id stuff,
    # but have to find a way to get it from a cookie or something like that, but for now it's just a dummy variable
    # also note that the user_id doesn't match the user_id for usermarketdetails, pretty sure thats incorrect
    # user_id = 1
    # user = get_object_or_404(User, pk=user_id)
    # result = table.objects.filter(string__icontains='pattern')
    # user_shares = Share.objects.filter(user_id=user_id)
    # user_market_details = get_object_or_404(UserMarketDetails, pk=3)

    # response = "You're looking at the results of question %s."
    # player = Player.objects.get(pk=player_id)
    # user = User.objects.get(pk=1)
    # user_id = request.COOKIES.get("user_id", None)
    # user_id = 1
    user = request.user

    if not search_query:
        search_query = request.GET.get('search_query', '')
    try:
        # doesn't work for player Quincy Adams at /market/2/buy/
        search_results = Player.objects.filter(sport="nba", name__icontains=search_query).prefetch_related('playercurrentstatistics_set')#.exclude(sport="mlb") # filter can be before or after prefetch_related, still not sure if also works with select_related
        print(search_results)
        search_results_dict = {}
        

        # for player in search_results:
        #     # print(row.id)
        #     player_current_stats = player.player_current_stats.all()
        #     search_results_dict[player.id] = {}
        #     for row in player_current_stats:
        #         search_results_dict[player.id][row.stat_name] = row


        context = {'user':user, 'player_list':search_results, 'search_results_dict':search_results_dict, 'query':search_query}
        context_to_add = getHeaderVariables()
        context.update(context_to_add)
        return render(request, 'market/search.html', context)
    except (KeyError, Player.DoesNotExist):
        context = {
            'test': 'hello world',
        }
        context_to_add = getHeaderVariables()
        context.update(context_to_add)
        return render(request, 'market/search.html', context)
    # else:
    #     user_player_shares = Share.objects.get(player_id=player_id)
    #     context = {'player':player, 'player_share':user_player_shares}
    #     return render(request, 'market/player_detail.html', context)
def rules(request):
    context = {}
    context_to_add = getHeaderVariables()
    context.update(context_to_add)
    return render(request, 'market/rules.html', context)

def account(request):
    user = request.user
    #https://stackoverflow.com/questions/31237042/whats-the-difference-between-select-related-and-prefetch-related-in-django-orm
    shares = Share.objects.filter(user=user)#Player.objects.prefetch_related("share_set").filter(share__user=user)#
    shares_list = []
    for row in shares:
        shares_list.append({'number_of_shares':row.number_of_shares, 'name': row.player.name})
    shares_json = json.dumps(shares_list)
    #shares_json = serializers.serialize('json', [x.player for x in shares])#, fields=('number_of_shares', 'player__name'))# , fields=('number_of_shares', 'name')fields=('stat_datetime_of_game', 'stat_name', 'stat_value'))
    # print(shares_json[0])
    context = {"owned_shares":shares, "owned_shares_json":shares_json}
    context_to_add = getHeaderVariables()
    context.update(context_to_add)
    return render(request, 'market/account.html', context)


# see https://stackoverflow.com/questions/51155947/django-redirect-to-another-view-with-context/51156032
def contact_us(request):
    if request.method == 'POST':
        current_site = get_current_site(request)
        mail_subject = 'You have a question from user' + request.POST["email"]
        message = request.POST["content"]
        to_email = 'elan.paul1@gmail.com' # change for company email
        email = EmailMessage(
                    mail_subject, message, to=[to_email]
        )
        email.send()
        # context = {'public_service_announcements': ['Thank you for reacching out. You\'re inquiry has been received. You should expect a reply in 3-5 business days']}
        # workaround, because you can't pass context on a redirect
        request.session['public_service_announcements'] = ['Thank you for reaching out. You\'re inquiry has been received. You should expect a reply in 3-5 business days']
        # return index(request, context) # workaround, but doesn't change to correct url
        return redirect('market:index')
        
    else:
        context = {}
        context_to_add = getHeaderVariables()
        context.update(context_to_add)
        return render(request, 'market/contact_us.html', context)




# https://simpleisbetterthancomplex.com/tutorial/2016/08/29/how-to-work-with-ajax-request-with-django.html
def getScheduledGames(request):
    # username = request.GET.get('username', None)
    selected_sport = request.GET['sport']
    today = date.today()
    scheduled_games = list(Game.objects.filter(game_time__day=date.today().day + 9, sport=selected_sport).values())

    # try:
    #     # doesn't work for player Quincy Adams at /market/2/buy/
    #     scheduled_games = .objects.filter(sport__icontains=selected_sport).
    #     data = {'list_of_games':scheduled_games}
    #     return JsonResponse(data)
    # except (KeyError, Player.DoesNotExist):
    #     data = {'list_of_games':None}
    #     # return render(request, 'market/search.html', {
    #     #     'player': 'hello world',
    #     # })
    data = {'ajax_scheduled_games': scheduled_games, 'sport':selected_sport}
    return JsonResponse(data)



def getHeaderVariables():
    # get list of names for search box suggestions, may need a better solution for json-ing it, for example if I decide to include the team or sport next to the suggestion
    # it will have to be a dict then and the search will have to be redone
    # see here why i did it this way https://stackoverflow.com/questions/7650448/django-serialize-queryset-values-into-json
    list_of_player_names = Player.objects.filter(sport="nba")#.all()
    list_of_players_json = serializers.serialize('json', list_of_player_names, fields=('name', 'player_id'))

    # get games being played tonight
    default_sport = 'nba'
    today = date.today()
    scheduled_games = Game.objects.filter(game_time__day=date.today().day + 7, sport=default_sport)
    header_context = {'scheduled_games':scheduled_games, 'list_of_names':list_of_players_json, 'primary_design_color':'#cccccc', 'secondary_design_color':'#404040',
    'primary_hover_color': '#bdbdbd', 'secondary_hover_color': '#303030', 'positive_color': '#16A916', 'negative_color': '#D41C1C'}
    return header_context
















# user interaction sectio 
# got a some of the help from https://blog.hlab.tech/part-ii-how-to-sign-up-user-and-send-confirmation-email-in-django-2-1-and-python-3-6/
def signup(request):
    # still not sure if I want a custom login or user theirs, I used all their other auth stuff
    if 'user_username' not in request.POST:
        return render(request, 'market/signup.html', {'hello':'part a'})
    else:
        user = User.objects.create_user(username=request.POST["user_username"],password=request.POST["user_password"],email=request.POST["user_email"])
        user.save()
        login(request, user)
        current_site = get_current_site(request)
        mail_subject = 'Welcome to Venture Sports'
        message = render_to_string('market/account_confirmation_email.html', {
            'user': user,
            'domain': current_site.domain,
            'uid':urlsafe_base64_encode(force_bytes(user.pk)),
            'token':account_activation_token.make_token(user),
        })
        to_email = request.POST["user_email"]
        email = EmailMessage(
                    mail_subject, message, to=[to_email]
        )
        email.send()
        # return HttpResponse('Welcome to Venture Sports, Please confirm your email address to complete the registration.')
        request.session['public_service_announcements'] = ['Welcome to Venture Sports, Please confirm your email address to complete the registration.']
        return redirect('market:index')
    if request.method == 'GET':
        context = {}
        context_to_add = getHeaderVariables()
        context.update(context_to_add)
        # wierd that my custom login is in registration and this is in market, should that be?
        return render(request, 'market/signup.html', context)

# got a some of the help from https://blog.hlab.tech/part-ii-how-to-sign-up-user-and-send-confirmation-email-in-django-2-1-and-python-3-6/
def activate_account(request, uidb64, token):
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return HttpResponse('Your account has been activate successfully')
        request.session['public_service_announcements'] = ['Your account has been activated successfully!']
        return redirect('market:index')
    else:
        return HttpResponse('Activation link is invalid!')

def custom_login(request):
    # still not sure if I want a custom login or user theirs, I used all their other auth stuff
    if request.method == 'POST':
        if 'user_username' not in request.POST:
            return render(request, 'registration/login.html', {'hello':'part a'})
        else:
            username = request.POST['user_username']
            password = request.POST['user_password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                # Redirect to a success page.
                response = HttpResponse('blah')
                response.set_cookie('cookie_name', 'cookie_value')
                context = {'error':False, 'hello':'part b'}
                return redirect('market:index')
                # return HttpResponseRedirect(reverse('market:index'))
                # return render(request, 'market/index.html', context)
            else:
                # Return an 'invalid login' error message.
                context = {'error':True, 'hello':'part c'}
                return render(request, 'registration/login.html', context)

    if request.method == 'GET':
        context = {}
        context_to_add = getHeaderVariables()
        context.update(context_to_add)
        # wierd that my custom login is in registration, should that be?
        return render(request, 'registration/login.html', context)







def custom_logout(request):
    logout(request)
    context = {}
    return redirect('market:index')



























# Django Rest Framework API Section
# see here https://www.django-rest-framework.org/tutorial/quickstart/ for an overview
# see here any of the tutorial pages starting here https://www.django-rest-framework.org/tutorial/1-serialization/ for more details on the shorthand and other stuff

# @api_view(['GET'])
# def api_root(request, format=None):
#     return Response({
#         'users': reverse('user-list', request=request, format=format),
#         'players': reverse('player-list', request=request, format=format)
#     })



class UserViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]





# @csrf_exempt
# @api_view(['GET', 'POST'])
# @permission_classes((permissions.AllowAny,))
class PlayerViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `team` action.
    """
    queryset = Player.objects.all().order_by("id")
    serializer_class = PlayerSerializer
    # tutorial has also custom permission IsOwnerOrReadOnly that is built at https://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/ but I haven't decided
    # if it's useful/needed yet
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,]

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def team(self, request, *args, **kwargs):
        player = self.get_object()
        return Response(player.team)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class VSUserViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = VSUser.objects.all()
    serializer_class = VSUserSerializer

class PlayerHistoricalStatisticsViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = PlayerHistoricalStatistics.objects.all()
    serializer_class = PlayerHistoricalStatisticsSerializer

class PlayerCurrentStatisticsViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = PlayerCurrentStatistics.objects.all()
    serializer_class = PlayerCurrentStatisticsSerializer

# @csrf_exempt
# @api_view(['GET', 'POST'])
# @permission_classes((permissions.AllowAny,))
# class ShareList(generics.ListCreateAPIView):
#     """
#     List all players, or create a new player.
#     """
#     queryset = Share.objects.all()
#     serializer_class = ShareSerializer
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]
#     def perform_create(self, serializer):
#         serializer.save(owner=self.request.user)

# # @csrf_exempt
# # @api_view(['GET', 'POST'])
# # @permission_classes((permissions.AllowAny,))
# class ShareDetail(generics.RetrieveUpdateDestroyAPIView):
#     """
#     Retrieve, update or delete a player instance.
#     """
#     queryset = Share.objects.all()
#     serializer_class = ShareSerializer
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]

# class PlayerTeam(generics.GenericAPIView):
#     queryset = Share.objects.all()
#     renderer_classes = [renderers.StaticHTMLRenderer]

#     def get(self, request, *args, **kwargs):
#         player = self.get_object()
#         return Response(player.team)
class ShareViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Share.objects.all()
    serializer_class = ShareSerializer

    # filterset_fields = ('user', )

class WatchlistDetailViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = WatchlistDetail.objects.all()
    serializer_class = WatchlistDetailSerializer

class UserMarketDetailsViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = UserMarketDetails.objects.all()
    serializer_class = UserMarketDetailsSerializer

class TeamViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

class GameViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer

class MarketViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Market.objects.all()
    serializer_class = MarketSerializer
