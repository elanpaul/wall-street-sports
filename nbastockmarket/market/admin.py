from django.contrib import admin

# Register your models here.
from .models import Player, VSUser, PlayerHistoricalStatistics, PlayerCurrentStatistics, UserMarketDetails, OrderBook, Team, Game, Share, WatchlistDetail, Market

admin.site.register(Player)
admin.site.register(VSUser)
admin.site.register(PlayerHistoricalStatistics)
admin.site.register(PlayerCurrentStatistics)
admin.site.register(Share)
admin.site.register(UserMarketDetails)
admin.site.register(WatchlistDetail)
admin.site.register(OrderBook)
admin.site.register(Team)
admin.site.register(Game)
admin.site.register(Market)