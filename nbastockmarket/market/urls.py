from django.urls import path, re_path
from django.conf.urls import url

from . import views
from django.contrib.auth import views as auth_views


# not sure, if these should be here or project urls.py, the quickstart tutorial says here explicitly, but on the rest framework home page
    # it implies it should at the project urls.py
from django.urls import include, path
from rest_framework import routers
from market import views # even though on the tutorial it says project.application
from rest_framework.urlpatterns import format_suffix_patterns
from market.views import UserViewSet, GroupViewSet, PlayerViewSet, VSUserViewSet, PlayerHistoricalStatisticsViewSet, PlayerCurrentStatisticsViewSet, ShareViewSet, WatchlistDetailViewSet, UserMarketDetailsViewSet, TeamViewSet, GameViewSet, MarketViewSet
from rest_framework import renderers
from rest_framework.routers import DefaultRouter



app_name = 'market'
# handler404 = 'market.views.my_custom_page_not_found_view'
# handler500 = 'market.views.my_custom_error_view'
# handler403 = 'market.views.my_custom_permission_denied_view'
# handler400 = 'market.views.my_custom_bad_request_view'


# Rest Framework API
# not sure, if these should be here or project urls.py, the quickstart tutorial says here explicitly, but on the rest framework home page
# it implies it should at the project urls.py
# router = routers.DefaultRouter()
# router.register(r'users', UserViewSet)
# router.register(r'groups', views.GroupViewSet) # not created yet
# router.register(r'players', views.PlayerViewSet)
# router.register(r'vsusers', views.VSUserViewSet)
# router.register(r'playerhistoricalstatistics', views.PlayerHistoricalStatisticsViewSet)
# router.register(r'playercurrentstatistics', views.PlayerCurrentStatisticsViewSet)
# router.register(r'shares', views.ShareViewSet)
# router.register(r'watchlistdetails', views.WatchlistDetailViewSet)
# router.register(r'usermarketdetails', views.UserMarketDetailsViewSet)
# router.register(r'teams', views.TeamViewSet)
# router.register(r'games', views.GameViewSet)
# router.register(r'markets', views.MarketViewSet)



# player_list = PlayerViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })
# player_detail = PlayerViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })
# player_team = PlayerViewSet.as_view({
#     'get': 'team'
# }, renderer_classes=[renderers.StaticHTMLRenderer])
# user_list = UserViewSet.as_view({
#     'get': 'list'
# })
# user_detail = UserViewSet.as_view({
#     'get': 'retrieve'
# })


urlpatterns = [
    # ex /polls/
    path('', views.index, name='index'),
    # path('login/', views.login, name='login'),
    
    # ex /polls/5/
    path('<int:player_id>/', views.detail, name='player_detail'),
    # ex: /polls/5/results/
    # path('<int:pk>/results/', views.results, name='results'),
    # ex: /polls/5/buy/
    path('<int:player_id>/buy/', views.buy, name='buy'),
    path('<int:player_id>/sell/', views.sell, name='sell'),
    path('<int:player_id>/watchlist_add/', views.addToWatchlist, name='watchlist_add'),
    path('<int:player_id>/watchlist_remove/', views.removeFromWatchlist, name='watchlist_remove'),
    # using both patterns for search either: search/?search_query=ploni, must have $ otherwise it will include urls formatted like so: search/ploni,
    # which we don't want this to include until the next url pattern
    re_path(r'^search/$', views.search, name='search'),
    # or: search/ploni
    re_path(r'^search/(?P<search_query>\w+)', views.search, name='search'),
    re_path(r'player_stats', views.allPlayerStats, name='all_player_stats'),
    
    re_path(r'^ajax/get_scheduled_games/', views.getScheduledGames, name='get_scheduled_games'),

    re_path(r'^rules/$', views.rules, name='rules'),
    re_path(r'^account/$', views.account, name='account'),
    # re_path(r'^faq/$', views.faq, name='faq'),
    re_path(r'^contact-us/', views.contact_us, name='contact_us'),



    

    path('signup/', views.signup, name='signup'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate_account, name='activate'),
    # path('accounts/', include('django.contrib.auth.urls')),

    path('login/', views.custom_login, name='login'),
    # path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('logout/', views.custom_logout, name='logout'),




    








    # not sure, if these should be here or project urls.py, the quickstart tutorial says here explicitly, but on the rest framework home page
    # it implies it should at the project urls.py
    # this path blows things up
    # url('^api/', include(router.urls)),

    # i guess this can also be deleted
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # path('players/', player_list, name='player-list'),
    # path('players/<int:pk>/', player_detail, name='player-detail'),
    # path('players/<int:pk>/team/', player_team, name='player-team'),


    # path('users/', user_list, name='user-list'),
    # path('users/<int:pk>/', user_detail, name='user-detail'),

    # path('shares/', views.ShareList.as_view(), name='share-list'),
    # path('shares/<int:pk>/', views.ShareDetail.as_view(), name='share-detail'),
    # path('shares/<int:pk>/player/', views.SharePlayer.as_view(), name='share-player'),

    # path('', views.api_root),
]


# for rest framework .json, .xml, etc.
# urlpatterns = format_suffix_patterns(urlpatterns)