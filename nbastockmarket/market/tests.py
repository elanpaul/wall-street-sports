from django.test import TestCase


from .models import Player, VSUser, User, UserMarketDetails, PlayerHistoricalStatistics, PlayerCurrentStatistics, Team, Game, Share, WatchlistDetail

# Create your tests here.



class PlayerTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("setUpTestData: Run once to set up non-modified data for all class methods.")
        # this is maybe to like create test users or players or something like that, not create global variables
        pass

    def setUp(self):
        print("setUp: Run once for every test method to setup clean data.")
        pass

    def test_player_list_current_price_total(self):
        print("Method: test_false_is_false.")
        player_list = Player.objects.all()
        target_total_price = 200.0
        test_total_price = 0.0
        for player in player_list:
            print(player.current_price)
            test_total_price += float(player.current_price)
        self.assertEquals(test_total_price, target_total_price)

    def test_false_is_true(self):
        print("Method: test_false_is_true.")
        self.assertTrue(False)

    def test_one_plus_one_equals_two(self):
        print("Method: test_one_plus_one_equals_two.")
        self.assertEqual(1 + 1, 2)
