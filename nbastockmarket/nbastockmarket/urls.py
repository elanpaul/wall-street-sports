"""nbastockmarket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

# https://www.django-rest-framework.org/ said to add these
from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from market.views import UserViewSet, GroupViewSet, PlayerViewSet, VSUserViewSet, PlayerHistoricalStatisticsViewSet, PlayerCurrentStatisticsViewSet, ShareViewSet, WatchlistDetailViewSet, UserMarketDetailsViewSet, TeamViewSet, GameViewSet, MarketViewSet

from market import views # even though on the tutorial it says project.application

# from serializers import UserSerializer
# from market.views import UserViewSet, GroupViewSet

import debug_toolbar

# Serializers define the API representation.
# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ['url', 'username', 'email', 'is_staff']

# # ViewSets define the view behavior.
# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'users', UserViewSet)

# Rest Framework API
# not sure, if these should be here or project urls.py, the quickstart tutorial says here explicitly, but on the rest framework home page
# it implies it should at the project urls.py
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', views.GroupViewSet) # not created yet
router.register(r'players', views.PlayerViewSet)
router.register(r'vsusers', views.VSUserViewSet)
router.register(r'playerhistoricalstatistics', views.PlayerHistoricalStatisticsViewSet)
router.register(r'playercurrentstatistics', views.PlayerCurrentStatisticsViewSet)
router.register(r'shares', views.ShareViewSet)
router.register(r'watchlistdetails', views.WatchlistDetailViewSet)
router.register(r'usermarketdetails', views.UserMarketDetailsViewSet)
router.register(r'teams', views.TeamViewSet)
router.register(r'games', views.GameViewSet)
router.register(r'markets', views.MarketViewSet)

urlpatterns = [
    url(r'^__debug__/', include(debug_toolbar.urls)),

    path('', views.index, name='index'),

    path('admin/', admin.site.urls),
    path('market/', include('market.urls')),
    # this is from https://docs.djangoproject.com/en/3.0/topics/auth/default/#module-django.contrib.auth.views

    # path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'), #html_email_template_name, success_url='index.html')

    path('', include('django.contrib.auth.urls')),

    url('^api/', include(router.urls)),

    
    # https://www.django-rest-framework.org/ said to add these
    # url(r'^', include(router.urls)),

    # url(r'^api-auth/', include('rest_framework.urls')),

]
