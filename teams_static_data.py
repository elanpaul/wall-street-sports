import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()
# your imports, e.g. Django models
# from models import Location

from market.models import Team



teams = [
    {
    "sport":"nba",
    "full_name":"atlanta hawks",
    "abbreviation":"atl",
    "primary_color":"#E03A3E",
    "secondary_color":"#C1D32F"
    },
    {
    "sport":"nba",
    "full_name":"boston celtics",
    "abbreviation":"bos",
    "primary_color":"#007A33",
    "secondary_color":"#BA9653"
    },
    {
    "sport":"nba",
    "full_name":"brooklyn nets",
    "abbreviation":"bkn",
    "primary_color":"#000000",
    "secondary_color":"#FFFFFF"
    },
    {
    "sport":"nba",
    "full_name":"charlotte hornets",
    "abbreviation":"cha",
    "primary_color":"#1D1160",
    "secondary_color":"#00788C"
    },
    {
    "sport":"nba",
    "full_name":"chicago bulls",
    "abbreviation":"chi",
    "primary_color":"#CE1141",
    "secondary_color":"#000000"
    },
    {
    "sport":"nba",
    "full_name":"cleveland cavaliers",
    "abbreviation":"cle",
    "primary_color":"#860038",
    "secondary_color":"#041E42"
    },
    {
    "sport":"nba",
    "full_name":"dallas mavericks",
    "abbreviation":"dal",
    "primary_color":"#00538C",
    "secondary_color":"#002B5E"
    },
    {
    "sport":"nba",
    "full_name":"denver nuggets",
    "abbreviation":"den",
    "primary_color":"#0E2240",
    "secondary_color":"#FEC524"
    },
    {
    "sport":"nba",
    "full_name":"detroit pistons",
    "abbreviation":"det",
    "primary_color":"#C8102E",
    "secondary_color":"#1D42BA"
    },
    {
    "sport":"nba",
    "full_name":"golden state warriors",
    "abbreviation":"gsw",
    "primary_color":"#1D428A",
    "secondary_color":"#FFC72C"
    },
    {
    "sport":"nba",
    "full_name":"houston rockets",
    "abbreviation":"hou",
    "primary_color":"#CE1141",
    "secondary_color":"#000000"
    },
    {
    "sport":"nba",
    "full_name":"indiana pacers",
    "abbreviation":"ind",
    "primary_color":"#002D62",
    "secondary_color":"#FDBB30"
    },
    {
    "sport":"nba",
    "full_name":"los angeles clippers",
    "abbreviation":"lac",
    "primary_color":"#C8102E",
    "secondary_color":"#1D428A"
    },
    {
    "sport":"nba",
    "full_name":"los angeles lakers",
    "abbreviation":"lal",
    "primary_color":"#552583",
    "secondary_color":"#FDB927"
    },
    {
    "sport":"nba",
    "full_name":"memphis grizzlies",
    "abbreviation":"mem",
    "primary_color":"#5D76A9",
    "secondary_color":"#12173F"
    },
    {
    "sport":"nba",
    "full_name":"miami heat",
    "abbreviation":"mia",
    "primary_color":"#98002E",
    "secondary_color":"#F9A01B"
    },
    {
    "sport":"nba",
    "full_name":"milwaukee bucks",
    "abbreviation":"mil",
    "primary_color":"#00471B",
    "secondary_color":"#EEE1C6"
    },
    {
    "sport":"nba",
    "full_name":"minnesota timberwolves",
    "abbreviation":"min",
    "primary_color":"#0C2340",
    "secondary_color":"#236192"
    },
    {
    "sport":"nba",
    "full_name":"new orleans pelicans",
    "abbreviation":"nop",
    "primary_color":"#0C2340",
    "secondary_color":"#C8102E"
    },
    {
    "sport":"nba",
    "full_name":"new york knicks",
    "abbreviation":"nyk",
    "primary_color":"#006BB6",
    "secondary_color":"#F58426"
    },
    {
    "sport":"nba",
    "full_name":"oklahoma city thunder",
    "abbreviation":"okc",
    "primary_color":"#007AC1",
    "secondary_color":"#EF3B24"
    },
    {
    "sport":"nba",
    "full_name":"orlando magic",
    "abbreviation":"orl",
    "primary_color":"#0077C0",
    "secondary_color":"#C4CED4"
    },
    {
    "sport":"nba",
    "full_name":"philadelphia 76ers",
    "abbreviation":"phi",
    "primary_color":"#006BB6",
    "secondary_color":"#ED174C"
    },
    {
    "sport":"nba",
    "full_name":"phoenix suns",
    "abbreviation":"phx",
    "primary_color":"#1D1160",
    "secondary_color":"#E56020"
    },
    {
    "sport":"nba",
    "full_name":"portland trailblazers",
    "abbreviation":"por",
    "primary_color":"#E03A3E",
    "secondary_color":"#000000"
    },
    {
    "sport":"nba",
    "full_name":"sacramento kings",
    "abbreviation":"sac",
    "primary_color":"#5A2D81",
    "secondary_color":"#63727A"
    },
    {
    "sport":"nba",
    "full_name":"san antonio spurs",
    "abbreviation":"sas",
    "primary_color":"#C4CED4",
    "secondary_color":"#000000"
    },
    {
    "sport":"nba",
    "full_name":"toronto raptors",
    "abbreviation":"tor",
    "primary_color":"#CE1141",
    "secondary_color":"#000000"
    },
    {
    "sport":"nba",
    "full_name":"utah jazz",
    "abbreviation":"uta",
    "primary_color":"#002B5C",
    "secondary_color":"#00471B"
    },
    {
    "sport":"nba",
    "full_name":"washington wizards",
    "abbreviation":"was",
    "primary_color":"#002B5C",
    "secondary_color":"#E31837"
    },
    {
    "sport":"nfl",
    "full_name":"arizona cardinals",
    "abbreviation":"ari",
    "primary_color":"#97233F",
    "secondary_color":"#000000"
    },
    {
    "sport":"nfl",
    "full_name":"atlanta falcons",
    "abbreviation":"atl",
    "primary_color":"#A71930",
    "secondary_color":"#000000"
    },
    {
    "sport":"nfl",
    "full_name":"baltimore ravens",
    "abbreviation":"bal",
    "primary_color":"#241773",
    "secondary_color":"#000000"
    },
    {
    "sport":"nfl",
    "full_name":"buffalo bills",
    "abbreviation":"buf",
    "primary_color":"#00338D",
    "secondary_color":"#C60C30"
    },
    {
    "sport":"nfl",
    "full_name":"carolina panthers",
    "abbreviation":"car",
    "primary_color":"#0085CA",
    "secondary_color":"#101820"
    },
    {
    "sport":"nfl",
    "full_name":"chicago bears",
    "abbreviation":"chi",
    "primary_color":"#0B162A",
    "secondary_color":"#C83803"
    },
    {
    "sport":"nfl",
    "full_name":"cincinnati bengals",
    "abbreviation":"cin",
    "primary_color":"#FB4F14",
    "secondary_color":"#000000"
    },
    {
    "sport":"nfl",
    "full_name":"cleveland browns",
    "abbreviation":"cle",
    "primary_color":"#311D00",
    "secondary_color":"#FF3C00"
    },
    {
    "sport":"nfl",
    "full_name":"dallas cowboys",
    "abbreviation":"dal",
    "primary_color":"#003594",
    "secondary_color":"#041E42"
    },
    {
    "sport":"nfl",
    "full_name":"denver broncos",
    "abbreviation":"den",
    "primary_color":"#FB4F14",
    "secondary_color":"#002244"
    },
    {
    "sport":"nfl",
    "full_name":"detroit lions",
    "abbreviation":"det",
    "primary_color":"#0076B6",
    "secondary_color":"#B0B7BC"
    },
    {
    "sport":"nfl",
    "full_name":"green bay packers",
    "abbreviation":"gb",
    "primary_color":"#203731",
    "secondary_color":"#FFB612"
    },
    {
    "sport":"nfl",
    "full_name":"houston texans",
    "abbreviation":"hou",
    "primary_color":"#03202F",
    "secondary_color":"#A71930"
    },
    {
    "sport":"nfl",
    "full_name":"indianapolis colts",
    "abbreviation":"ind",
    "primary_color":"#002C5F",
    "secondary_color":"#A2AAAD"
    },
    {
    "sport":"nfl",
    "full_name":"jacksonville jaguars",
    "abbreviation":"jax",
    "primary_color":"#101820",
    "secondary_color":"#D7A22A"
    },
    {
    "sport":"nfl",
    "full_name":"kansas city chiefs",
    "abbreviation":"kc",
    "primary_color":"#E31837",
    "secondary_color":"#FFB81C"
    },
    {
    "sport":"nfl",
    "full_name":"los angeles chargers",
    "abbreviation":"lac",
    "primary_color":"#002A5E",
    "secondary_color":"#FFC20E"
    },
    {
    "sport":"nfl",
    "full_name":"los angeles rams",
    "abbreviation":"lar",
    "primary_color":"#003594",
    "secondary_color":"#FFA300"
    },
    # possibly the las vegas raiders go here
    {
    "sport":"nfl",
    "full_name":"miami dolphins",
    "abbreviation":"mia",
    "primary_color":"#008E97",
    "secondary_color":"#FC4C02"
    },
    {
    "sport":"nfl",
    "full_name":"minnesota vikings",
    "abbreviation":"min",
    "primary_color":"#4F2683",
    "secondary_color":"#FFC62F"
    },
    {
    "sport":"nfl",
    "full_name":"new england patriots",
    "abbreviation":"ne",
    "primary_color":"#002244",
    "secondary_color":"#C60C30"
    },
    {
    "sport":"nfl",
    "full_name":"new orleans saints",
    "abbreviation":"no",
    "primary_color":"#D3BC8D",
    "secondary_color":"#101820"
    },
    {
    "sport":"nfl",
    "full_name":"new york giants",
    "abbreviation":"nyg",
    "primary_color":"#0B2265",
    "secondary_color":"#A71930"
    },
    {
    "sport":"nfl",
    "full_name":"new york jets",
    "abbreviation":"nyj",
    "primary_color":"#125740",
    "secondary_color":"#000000"
    },
    {
    "sport":"nfl",
    "full_name":"oakland raiders",
    "abbreviation":"oak",
    "primary_color":"#000000",
    "secondary_color":"#A5ACAF"
    },
    {
    "sport":"nfl",
    "full_name":"philadelphia eagles",
    "abbreviation":"phi",
    "primary_color":"#004C54",
    "secondary_color":"#A5ACAF"
    },
    {
    "sport":"nfl",
    "full_name":"pittsburgh steelers",
    "abbreviation":"pit",
    "primary_color":"#FFB612",
    "secondary_color":"#101820"
    },
    {
    "sport":"nfl",
    "full_name":"san francisco 49ers",
    "abbreviation":"sf",
    "primary_color":"#AA0000",
    "secondary_color":"#B3995D"
    },
    {
    "sport":"nfl",
    "full_name":"seattle seahawks",
    "abbreviation":"sea",
    "primary_color":"#002244",
    "secondary_color":"#69BE28"
    },
    {
    "sport":"nfl",
    "full_name":"tampa bay buccaneers",
    "abbreviation":"tb",
    "primary_color":"#D50A0A",
    "secondary_color":"#FF7900"
    },
    {
    "sport":"nfl",
    "full_name":"tennessee titans",
    "abbreviation":"ten",
    "primary_color":"#0C2340",
    "secondary_color":"#4B92DB"
    },
    {
    "sport":"nfl",
    "full_name":"washington redskins",
    "abbreviation":"was",
    "primary_color":"#773141",
    "secondary_color":"#FFB612"
    },
    {
    "sport":"mlb",
    "full_name":"arizona diamondbacks",
    "abbreviation":"ari",
    "primary_color":"#A71930",
    "secondary_color":"#E3D4AD"
    },
    {
    "sport":"mlb",
    "full_name":"atlanta braves",
    "abbreviation":"atl",
    "primary_color":"#CE1141",
    "secondary_color":"#13274F"
    },
    {
    "sport":"mlb",
    "full_name":"baltimore orioles",
    "abbreviation":"bal",
    "primary_color":"#DF4601",
    "secondary_color":"#000000"
    },
    {
    "sport":"mlb",
    "full_name":"boston red sox",
    "abbreviation":"bos",
    "primary_color":"#BD3039",
    "secondary_color":"#0C2340"
    },
    {
    "sport":"mlb",
    "full_name":"chicago cubs",
    "abbreviation":"chc",
    "primary_color":"#0E3386",
    "secondary_color":"#CC3433"
    },
    {
    "sport":"mlb",
    "full_name":"chicago white sox",
    "abbreviation":"cws",
    "primary_color":"#27251F",
    "secondary_color":"#C4CED4"
    },
    {
    "sport":"mlb",
    "full_name":"cincinnati reds",
    "abbreviation":"cin",
    "primary_color":"#C6011F",
    "secondary_color":"#000000"
    },
    {
    "sport":"mlb",
    "full_name":"cleveland indians",
    "abbreviation":"cle",
    "primary_color":"#0C2340",
    "secondary_color":"#E31937"
    },
    {
    "sport":"mlb",
    "full_name":"colorado rockies",
    "abbreviation":"col",
    "primary_color":"#33006F",
    "secondary_color":"#C4CED4"
    },
    {
    "sport":"mlb",
    "full_name":"detroit tigers",
    "abbreviation":"det",
    "primary_color":"#0C2340",
    "secondary_color":"#FA4616"
    },
    {
    "sport":"mlb",
    "full_name":"houston astros",
    "abbreviation":"hou",
    "primary_color":"#002D62",
    "secondary_color":"#EB6E1F"
    },
    {
    "sport":"mlb",
    "full_name":"kansas city royals",
    "abbreviation":"kc",
    "primary_color":"#004687",
    "secondary_color":"#BD9B60"
    },
    {
    "sport":"mlb",
    "full_name":"los angeles angels",
    "abbreviation":"laa",
    "primary_color":"#003263",
    "secondary_color":"#BA0021"
    },
    {
    "sport":"mlb",
    "full_name":"los angeles dodgers",
    "abbreviation":"lad",
    "primary_color":"#005A9C",
    "secondary_color":"#EF3E42"
    },
    {
    "sport":"mlb",
    "full_name":"miami marlins",
    "abbreviation":"mia",
    "primary_color":"#00A3E0",
    "secondary_color":"#EF3340"
    },
    {
    "sport":"mlb",
    "full_name":"milwaukee brewers",
    "abbreviation":"mil",
    "primary_color":"#0A2351",
    "secondary_color":"#B6922E"
    },
    {
    "sport":"mlb",
    "full_name":"minnesota twins",
    "abbreviation":"min",
    "primary_color":"#002B5C",
    "secondary_color":"#D31145"
    },
    {
    "sport":"mlb",
    "full_name":"new york mets",
    "abbreviation":"nym",
    "primary_color":"#002D72",
    "secondary_color":"#FF5910"
    },
    {
    "sport":"mlb",
    "full_name":"new york yankees",
    "abbreviation":"nyy",
    "primary_color":"#003087",
    "secondary_color":"#E4002C"
    },
    {
    "sport":"mlb",
    "full_name":"oakland athletics",
    "abbreviation":"oak",
    "primary_color":"#003831",
    "secondary_color":"#EFB21E"
    },
    {
    "sport":"mlb",
    "full_name":"philadelphia phillies",
    "abbreviation":"phi",
    "primary_color":"#E81828",
    "secondary_color":"#002D72"
    },
    {
    "sport":"mlb",
    "full_name":"pittsburgh pirates",
    "abbreviation":"pit",
    "primary_color":"#27251F",
    "secondary_color":"#FFC72C"
    },
    {
    "sport":"mlb",
    "full_name":"st. louis cardinals",
    "abbreviation":"stl",
    "primary_color":"#C41E3A",
    "secondary_color":"#0C2340"
    },
    {
    "sport":"mlb",
    "full_name":"san diego padres",
    "abbreviation":"sd",
    "primary_color":"#2F241D",
    "secondary_color":"#FFC425"
    },
    {
    "sport":"mlb",
    "full_name":"san fransisco giants",
    "abbreviation":"sf",
    "primary_color":"#FD5A1E",
    "secondary_color":"#27251F"
    },
    {
    "sport":"mlb",
    "full_name":"seattle mariners",
    "abbreviation":"sea",
    "primary_color":"#0C2C56",
    "secondary_color":"#005C5C"
    },
    {
    "sport":"mlb",
    "full_name":"tampa bay rays",
    "abbreviation":"tb",
    "primary_color":"#092C5C",
    "secondary_color":"#8FBCE6"
    },
    {
    "sport":"mlb",
    "full_name":"texas rangers",
    "abbreviation":"tex",
    "primary_color":"#003278",
    "secondary_color":"#C0111F"
    },
    {
    "sport":"mlb",
    "full_name":"toronto blue jays",
    "abbreviation":"tor",
    "primary_color":"#134A8E",
    "secondary_color":"#1D2D5C"
    },
    {
    "sport":"mlb",
    "full_name":"washington nationals",
    "abbreviation":"wsh",
    "primary_color":"#AB0003",
    "secondary_color":"#14225A"
    },
]


id_counter = 1
for team in teams:
    if team["sport"] == "nba":
        continue
    new_team = Team()
    new_team.id = id_counter
    new_team.sport = team["sport"]
    new_team.full_name = team["full_name"]
    new_team.abbreviation = team["abbreviation"]
    new_team.primary_color = team["primary_color"]
    new_team.secondary_color = team["secondary_color"]
    if id_counter == 1:
        new_team.next_opponent_id = 1610612748
    else:
        new_team.next_opponent_id = id_counter - 1
    new_team.save()
    id_counter += 1