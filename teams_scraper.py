import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint

team_colors_array = [['#E03A3E', '#C1D32F'], ['#007A33', '#BA9653'], ['#000000', '#FFFFFF'], ['#1D1160', '#00788C'], ['#CE1141', '#000000'], ['#860038', '#041E42'],
['#00538C', '#002B5E'], ['#0E2240', '#FEC524'], ['#C8102E', '#1D42BA'], ['#1D428A', '#FFC72C'], ['#CE1141', '#000000'], ['#002D62', '#FDBB30'],
['#C8102E', '#1D428A'], ['#552583', '#FDB927'], ['#5D76A9', '#12173F'], ['#98002E', '#F9A01B'], ['#00471B', '#EEE1C6'], ['#0C2340', '#236192'],
['#0C2340', '#C8102E'], ['#006BB6', '#F58426'], ['#007AC1', '#EF3B24'], ['#0077C0', '#C4CED4'], ['#006BB6', '#ED174C'], ['#1D1160', '#E56020'],
['#E03A3E', '#000000'], ['#5A2D81', '#63727A'], ['#C4CED4', '#000000'], ['#CE1141', '#000000'], ['#002B5C', '#00471B'], ['#002B5C', '#E31837']]


retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


url = "https://data.nba.net/10s/prod/v1/2019/teams.json"
request = http.get(url)
result = request.json()
teams = result["league"]["standard"]

for team in teams:
    if team["isNBAFranchise"] == True:
        team_id = int(team["teamId"])
        name = team["fullName"]
        abbreviation = team["tricode"]
        to_add_team = Team()
        to_add_team.id = team_id
        to_add_team.sport = 'NBA'
        to_add_team.full_name = name
        to_add_team.abbreviation = abbreviation
        to_add_team.next_opponent_id = team_id # can this also be to_add_team.next_opponent
        to_add_team.save()



