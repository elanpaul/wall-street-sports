import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team, PlayerCurrentStatistics, PlayerHistoricalStatistics
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer, PlayerCurrentStatisticsSerializer, PlayerHistoricalStatisticsSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import csv
from datetime import date
import datetime
import json
import pprint

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


def createPlayerHistoricalStatisticsRow(id, stat_name, stat_value, stat_type, player):
    player_statistics_row = PlayerHistoricalStatistics()
    player_statistics_row.stat_id = id
    player_statistics_row.stat_name = stat_name
    player_statistics_row.stat_value = stat_value
    player_statistics_row.stat_type = stat_type
    player_statistics_row.stat_datetime_of_game = datetime.now()
    player_statistics_row.stat_created_at = datetime.now()
    player_statistics_row.player = player
    # player_statistics_row.save()


def getOrDefault(value):
    if value == '':
        return 0.0
    else:
        return float(value)

def getNBAPlayerHistoricalStatistics():
    # base_start_url = "https://stats.nba.com/search/player-game/#?Season=2019-20&SeasonType=Regular%20Season&sort=GAME_DATE&PlayerID="
    # base_end_url = "&dir=1"
    base_start_url = "https://stats.nba.com/player/"
    base_end_url = "/boxscores/?Season=2019-20&SeasonType=Regular%20Season&PerMode=PerGame&sort=Game_ID&dir=1"

    players = Player.objects.filter(sport="NBA")
    id_counter = 1
    for player in players:
        player_id = player.id
        url = base_start_url + str(player_id) + base_end_url
        print(url)
        request = http.get(url)
        result = request.text
        soup = BeautifulSoup(result)
        nba_stat_table_div = soup.find('div', {'class':'nba_stat_table'})


        # must take into account pages per player
        if not nba_stat_table_div:
        #     request = http.get(url)
        #     result = request.text
        #     soup = BeautifulSoup(result)
        #     soup = BeautifulSoup(result)
        #     nba_stat_table_div = soup.find('div', {'class':'nba_stat_table'})
        #     if not nba_stat_table_div:
            print("couldn't find player id: " + str(player_id))
            continue



        rows = nba_stat_table_div.findAll('tr')

        game_counter = 1

        for row in rows:
            
            cells = row.findAll('td')
            matchup = cells[0]
            external_format_date = matchup.split[0].strip()
            matchup_teams = matchup.split[1].strip()
            game_id = 0
            # game_date = cells[2].text.strip()

            # split_date = game_date.split('/')
            game_id = int(str(player_id) + str(game_counter) + '2020')
            # datetime.datetime.strptime(game_date, '%Y/%m/%d') # %H:%M:%S.%f

            team_abbreviation = matchup_teams.split(' ')[0].strip() # this should be a foreign key
            team = Team.objects.filter(sport="NBA", abbreviation=team_abbreviation)
            if team:
                team = team[0]
            else:
                print("couldnt find team")
                print(team_abbreviation)

            home_or_away = ''
            if '@' in matchup_teams:
                home_or_away = '@'
            opponent = matchup_teams.split(' ')[2].strip()
            opponent_abbreviation = matchup_teams.split(' ')[0].strip() # this should be a foreign key
            opponent = Team.objects.filter(sport="NBA", abbreviation=opponent_abbreviation)
            if opponent:
                opponent = opponent[0]
            else:
                print("couldnt find opponent")
                print(opponent_abbreviation)
            started = ''
            minutes_played = cells[2].text.strip()
            field_goals_made = int(cells[4].text.strip())
            field_goals_attempted = int(cells[5].text.strip())
            three_point_field_goals_made = int(cells[7].text.strip())
            three_point_field_goals_attempted = int(cells[8].text.strip())
            free_throws_made = int(cells[10].text.strip())
            free_throws_attempted = int(cells[11].text.strip())
            offensive_rebounds = int(cells[13].text.strip())
            defensive_rebounds = int(cells[14].text.strip())
            total_rebounds = int(cells[15].text.strip())
            assists = int(cells[16].text.strip())
            steals = int(cells[17].text.strip())
            blocks = int(cells[18].text.strip())
            turnovers = int(cells[19].text.strip())
            personal_fouls = int(cells[20].text.strip())
            game_score = 0.0
            key_player = player


            createPlayerHistoricalStatisticsRow(id_counter, "nba_game_id", game_id, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+1, "nba_game_number", game_counter, "integer", player)
            # is implemented as string right now in models, but should be foreign key
            createPlayerHistoricalStatisticsRow(id_counter+2, "nba_team", team, "foreign_key", player)
            createPlayerHistoricalStatisticsRow(id_counter+3, "nba_home_or_away", home_or_away, "string", player)
            # implemented as string, should this also be a foreign key or just keep it as string
            createPlayerHistoricalStatisticsRow(id_counter+4, "nba_opponent", opponent_abbreviation, "string", player)
            # idk how to get if started
            createPlayerHistoricalStatisticsRow(id_counter+5, "nba_started", 0, "integer", player)
            # should this be string
            createPlayerHistoricalStatisticsRow(id_counter+6, "nba_minutes_played", minutes_played, "string", player)

            createPlayerHistoricalStatisticsRow(id_counter+7, "nba_field_goals_made", field_goals_made, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+8, "nba_field_goals_attempted", field_goals_attempted, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+9, "nba_three_point_field_goals_made", three_point_field_goals_made, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+10, "nba_three_point_field_goals_attempted", three_point_field_goals_made, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+11, "nba_free_throws_made", free_throws_made, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+12, "nba_free_throws_attempted", free_throws_attempted, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+13, "nba_offensive_rebounds", offensive_rebounds, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+14, "nba_defensive_rebounds", defensive_rebounds, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+15, "nba_total_rebounds", total_rebounds, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+16, "nba_assists", assists, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+17, "nba_steals", steals, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+18, "nba_blocks", blocks, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+19, "nba_turnovers", turnovers, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+20, "nba_personal_fouls", player_fouls, "integer", player)
            # should it be points
            createPlayerHistoricalStatisticsRow(id_counter+21, "nba_total_points", total_points, "integer", player)
            createPlayerHistoricalStatisticsRow(id_counter+22, "nba_game_score", 0.0, "float", player)

            id_counter+=23
            # player_historical_stats_to_add = PlayerHistoricalStatistics()

            # player_historical_stats_to_add.game_id = game_id
            # # shouldn't be this if we're doing more than one season, but for now it's good
            # player_historical_stats_to_add.game_number = game_counter
            # player_historical_stats_to_add.game_date = game_date
            # player_historical_stats_to_add.team = team
            # player_historical_stats_to_add.home_or_away = home_or_away
            # player_historical_stats_to_add.opponent = opponent
            # player_historical_stats_to_add.started = 0
            # player_historical_stats_to_add.minutes_played = minutes_played
            # player_historical_stats_to_add.field_goals_made = int(field_goals_made)
            # player_historical_stats_to_add.field_goals_attempted = int(field_goals_attempted)
            # player_historical_stats_to_add.three_point_field_goals_made = int(three_point_field_goals_made)
            # player_historical_stats_to_add.three_point_field_goals_attempted = int(three_point_field_goals_attempted)
            # player_historical_stats_to_add.free_throws_made = int(free_throws_made)
            # player_historical_stats_to_add.free_throws_attempted = int(free_throws_attempted)
            # player_historical_stats_to_add.offensive_rebounds = int(offensive_rebounds)
            # player_historical_stats_to_add.defensive_rebounds = int(defensive_rebounds)
            # player_historical_stats_to_add.total_rebounds = int(total_rebounds)
            # player_historical_stats_to_add.assists = int(assists)
            # player_historical_stats_to_add.steals = int(steals)
            # player_historical_stats_to_add.blocks = int(blocks)
            # player_historical_stats_to_add.turnovers = int(turnovers)
            # player_historical_stats_to_add.personal_fouls = int(personal_fouls)
            # player_historical_stats_to_add.total_points = int(total_points)
            # player_historical_stats_to_add.game_score = 0.0
            # player_historical_stats_to_add.player = key_player
            # player_historical_stats_to_add.save()

            game_counter += 1




getNBAPlayerHistoricalStatistics()