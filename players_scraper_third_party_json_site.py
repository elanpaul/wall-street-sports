import os, sys
sys.path.append("C:\\Programs_And_Projects\\Websites\\SportsStockMarket\\nbastockmarket")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nbastockmarket.settings")

import django
django.setup()

import requests
from market.models import User, Player, Share, Team
from market.serializers import UserSerializer, PlayerSerializer, ShareSerializer, TeamSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import pprint

player_colors_array = [['#E03A3E', '#C1D32F'], ['#007A33', '#BA9653'], ['#000000', '#FFFFFF'], ['#1D1160', '#00788C'], ['#CE1141', '#000000'], ['#860038', '#041E42'],
['#00538C', '#002B5E'], ['#0E2240', '#FEC524'], ['#C8102E', '#1D42BA'], ['#1D428A', '#FFC72C'], ['#CE1141', '#000000'], ['#002D62', '#FDBB30'],
['#C8102E', '#1D428A'], ['#552583', '#FDB927'], ['#5D76A9', '#12173F'], ['#98002E', '#F9A01B'], ['#00471B', '#EEE1C6'], ['#0C2340', '#236192'],
['#0C2340', '#C8102E'], ['#006BB6', '#F58426'], ['#007AC1', '#EF3B24'], ['#0077C0', '#C4CED4'], ['#006BB6', '#ED174C'], ['#1D1160', '#E56020'],
['#E03A3E', '#000000'], ['#5A2D81', '#63727A'], ['#C4CED4', '#000000'], ['#CE1141', '#000000'], ['#002B5C', '#00471B'], ['#002B5C', '#E31837']]


retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "OPTIONS"]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


url = "https://data.nba.net/10s/prod/v1/2019/players.json"
request = http.get(url)
result = request.json()
players = result["league"]["standard"]

player_counter = 0
for player in players:
    if player["isActive"] == True:
        player_counter += 1
        player_id = int(player["personId"])
        team_id = int(player["teamId"])
        name = player["firstName"] + " " + player["lastName"]
        position = player["pos"]
        number = player["jersey"]
        if number == '':
            number = 0
        else:
            number = int(number)
        height = player["heightFeet"] + " ft. " + player["heightInches"] + " in."
        weight = player["weightPounds"] + " lbs."
        start_year = player["nbaDebutYear"]
        to_add_player = Player()
        to_add_player.id = player_counter
        to_add_player.sport = 'NBA'
        to_add_player.name = name
        to_add_player.position = position
        to_add_player.number = number
        to_add_player.height = height
        to_add_player.weight = weight
        to_add_player.team_id = team_id
        to_add_player.owner_id = 1
        to_add_player.save()



